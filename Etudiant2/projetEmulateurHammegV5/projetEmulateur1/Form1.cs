﻿using System;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

namespace projetEmulateur1
{
    public partial class imgOutpOn : Form
    {
        //chDemandé est initialisee ici pour que la variable soit utilisable partout dans la classe
        int chDemandé = 1;
        private string[] etatChannel = { "0", "0", "0", "0" };
        private string[] voltageMAX = { "0", "0", "0", "0" };
        private string[] intensitéMAX = {"0", "0", "0", "0" };

        public imgOutpOn()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)//pendant que l'app se charge
        {
            string[] ports = SerialPort.GetPortNames();
            cboPort.Items.AddRange(ports);
            if (cboPort.Items.Count > 0)
            {
                cboPort.SelectedIndex = 0;
            }
            btnClose.Enabled = false;
            Ch1Gris.Visible = true;
            Ch2Gris.Visible = true;
            Ch3Gris.Visible = true;
            Ch4Gris.Visible = true;
            Ch1Vert.Visible = false;
            Ch2Vert.Visible = false;
            Ch3Vert.Visible = false;
            Ch4Vert.Visible = false;
            Ch1Bleu.Visible = false;
            Ch2Bleu.Visible = false;
            Ch3Bleu.Visible = false;
            Ch4Bleu.Visible = false;
            OutpGris.Visible = true;
            OutpBlanc.Visible = false;

            Rmt.Visible = true;
        }



        public void btnOpen_Click(object sender, EventArgs e)
        {
            btnOpen.Enabled = false;
            btnClose.Enabled = true;
          
            try
            {
                serialPort1.PortName = cboPort.Text;
                serialPort1.Open();
                timer1.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnOpen.Enabled = true;
            btnClose.Enabled = false;
            try
            {
                serialPort1.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        string sBuffer = "";
        public void MonTimer()
            //Longueur Timer modifée de 1000 ms a 50 ms
        {
            try
            {
                string lsReçu = serialPort1.ReadExisting();
                lsReçu = lsReçu.Replace("\n", "\r");
                lsReçu = lsReçu.Replace("\r\r", "\r");
                sBuffer += lsReçu;
                if (sBuffer == null || sBuffer.Length > 0)
                {
                    string t;
                    t = "";
                }
                if (sBuffer.Contains("\r"))
                {
                    lsReçu = sBuffer.Split("\r"[0])[0] + "\r";
                    sBuffer = sBuffer.Substring(lsReçu.Length);
                }
                else
                {
                    return;
                }

                if (lsReçu.Length != 0)
                {
                    lsReçu = lsReçu.Substring(0, lsReçu.Length - 1);//enlever le retour chariot (/r)
                   
                }
                if (lsReçu == "*IDN?")
                {
                    serialPort1.WriteLine("HAMEG,HMP4040");
                    return;
                }
                if (lsReçu == "OUTP:SEL?")
                {
                    serialPort1.WriteLine(this.etatChannel[this.chDemandé - 1].ToString());
                }
                if (lsReçu.Contains("APPLY"))
                {
                    lsReçu = lsReçu.Replace("APPLY ","" );
                    voltageMAX[this.chDemandé -1] = lsReçu.Split(',')[0];
                    intensitéMAX[this.chDemandé -1] = lsReçu.Split(',')[1];
                }
                if (lsReçu == "VOLT?")
                {
                    serialPort1.WriteLine(this.voltageMAX[this.chDemandé - 1].ToString());
                }
                if (lsReçu == "CURR?")
                {
                    serialPort1.WriteLine(this.intensitéMAX[this.chDemandé - 1].ToString());
                }
                if (lsReçu == "OUTP:GEN?")
                {
                    serialPort1.WriteLine("1");
                }
                if (lsReçu == "MEAS:VOLT?")
                {
                    // Calculer voltage compte tenu resistance fictive (non pret)
                    serialPort1.WriteLine(this.voltageMAX[this.chDemandé - 1].ToString());
                }
                  
                if (lsReçu == "MEAS:CURR?")
                {
                    //meme cas que dans voltage a faire avec intensité
                    serialPort1.WriteLine(this.intensitéMAX[this.chDemandé - 1].ToString());
                  
                }
                

                //  Si chaine de charactère strictement superieur a 8 et que de la position 0 a 8 on a "INST OUT" alors...
                if (lsReçu.Length > 8 && lsReçu.Substring(0, 8).ToUpper() == "INST OUT")//Choix du channel
                {
                    //Split = decouper la chaine de charactere en 2 morceaux et conserver le 2 eme, valeur numerique
                    string lsChannel = lsReçu.ToUpper().Split('T')[2];
                    /*convertion du chiffre contenu dans lsChannel qui est en string, en int dans la variable chDemandé.
                        Le contenue de chDemandé sera disponible dans toute la classe*/
                    int channel;
                    int.TryParse(lsChannel, out channel);
                    if (channel < 1 || channel  > 4)
                    {
                        //Valeur invalide ne rien faire
                    }
                    else
                    {
                        this.chDemandé = channel;
                    }

                    // Vérifier que entre 1 et 5
                    int valeurStockée = 0; // Initialiser la variable à une valeur de départ

                    if (chDemandé == 1)
                    {

                        


                        valeurStockée = 1; // Stocker la valeur de chDemandé dans la variable
                    }
                    else if (chDemandé == 2)
                    {
                        


                        valeurStockée = 2; // Stocker la valeur de chDemandé dans la variable
                    }
                    else if (chDemandé == 3)
                    {
                       



                        valeurStockée = 3; // Stocker la valeur de chDemandé dans la variable
                    }
                    else if (chDemandé == 4)
                    {
                        



                        if (lsReçu.Length == 11 && lsReçu.Substring(0, 11).ToUpper() == "OUTP:SEL ON")//Activer le bouton output pour allumer le courant
                        {
                            OutpBlanc.Visible = true;
                            OutpGris.Visible = false;
                        }
                        if (lsReçu.Length == 12 && lsReçu.Substring(0, 12).ToUpper() == "OUTP:SEL OFF")//Desactiver le bouton output pour allumer le courant
                        {
                            OutpBlanc.Visible = false;
                            OutpGris.Visible = true;
                        }

                        //Si chaine de charactère = VOLTAGE ou voltage prendre la valeur entre le 8 eme char et 10 char

                        if (lsReçu.Length > 7 && lsReçu.Substring(0, 7).ToUpper() == "VOLTAGE")
                        {
                            //Split = decouper la chaine de charactere en 2 morceaux et conserver le 2 eme, valeur numerique
                            string lsVoltage = lsReçu.Split(' ')[1];
                            if (chDemandé == 1)
                            {
                                this.txtVoltCh1.Text = lsVoltage;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = true;
                                Ch1Gris.Visible = false;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = false;
                                Ch2Gris.Visible = true;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = false;
                                Ch3Gris.Visible = true;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = false;
                                Ch4Gris.Visible = true;
                            }
                            if (chDemandé == 2)
                            {
                                this.txtVoltCh2.Text = lsVoltage;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = false;
                                Ch1Gris.Visible = true;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = true;
                                Ch2Gris.Visible = false;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = false;
                                Ch3Gris.Visible = true;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = false;
                                Ch4Gris.Visible = true;
                            }
                            if (chDemandé == 3)
                            {
                                this.txtVoltCh3.Text = lsVoltage;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = false;
                                Ch1Gris.Visible = true;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = false;
                                Ch2Gris.Visible = true;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = true;
                                Ch3Gris.Visible = false;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = false;
                                Ch4Gris.Visible = true;
                            }
                            if (chDemandé == 4)
                            {
                                this.txtVoltCh4.Text = lsVoltage;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = false;
                                Ch1Gris.Visible = true;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = false;
                                Ch2Gris.Visible = true;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = false;
                                Ch3Gris.Visible = true;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = true;
                                Ch4Gris.Visible = false;
                            }
                        }

                        //Si chaine de charactère = CURRENT ou current prendre la valeur entre le 8 eme char et 10 char
                        // on retire les 8 premiers char avec "substring" on met lenght -2 car la touche entrée est considée comment un char
                        if (lsReçu.Length > 7 && lsReçu.Substring(0, 7).ToUpper() == "CURRENT")
                        {
                            //Split = decoupe la chaine de charactere en 2 morceaux et conserve le 2 eme, valeur numerique
                            string lsCurrent = lsReçu.Split(' ')[1];
                            if (chDemandé == 1)
                            {
                                this.txtVoltCh1.Text = lsCurrent;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = true;
                                Ch1Gris.Visible = false;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = false;
                                Ch2Gris.Visible = true;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = false;
                                Ch3Gris.Visible = true;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = false;
                                Ch4Gris.Visible = true;
                            }
                            if (chDemandé == 2)
                            {
                                this.txtVoltCh2.Text = lsCurrent;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = false;
                                Ch1Gris.Visible = true;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = true;
                                Ch2Gris.Visible = false;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = false;
                                Ch3Gris.Visible = true;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = false;
                                Ch4Gris.Visible = true;
                            }
                            if (chDemandé == 3)
                            {
                                this.txtAmpCh3.Text = lsCurrent;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = false;
                                Ch1Gris.Visible = true;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = false;
                                Ch2Gris.Visible = true;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = true;
                                Ch3Gris.Visible = false;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = false;
                                Ch4Gris.Visible = true;
                            }
                            if (chDemandé == 4)
                            {
                                this.txtAmpCh4.Text = lsCurrent;

                                Ch1Vert.Visible = false;
                                Ch1Bleu.Visible = false;
                                Ch1Gris.Visible = true;

                                Ch2Vert.Visible = false;
                                Ch2Bleu.Visible = false;
                                Ch2Gris.Visible = true;

                                Ch3Vert.Visible = false;
                                Ch3Bleu.Visible = false;
                                Ch3Gris.Visible = true;

                                Ch4Vert.Visible = false;
                                Ch4Bleu.Visible = true;
                                Ch4Gris.Visible = false;
                            }
                        }

                        //Envoie des valeurs demandé par putty

                        if (lsReçu.Length == 5 && lsReçu.Substring(0, 5).ToUpper() == "VOLT?")
                        {
                            serialPort1.WriteLine(txtVoltCh1.Text + Environment.NewLine);
                        }

                        if (lsReçu.Length == 5 && lsReçu.Substring(0, 5).ToUpper() == "CURR?")
                        {
                            serialPort1.WriteLine(txtAmpCh1.Text + Environment.NewLine);
                        }
                        }

                }


                this.txtVoltCh1.Text = this.voltageMAX[0];
                this.txtVoltCh2.Text = this.voltageMAX[1];
                this.txtVoltCh3.Text = this.voltageMAX[2];
                this.txtVoltCh4.Text = this.voltageMAX[3];
                this.txtAmpCh1.Text = this.intensitéMAX[0];
                this.txtAmpCh2.Text = this.intensitéMAX[1];
                this.txtAmpCh3.Text = this.intensitéMAX[2];
                this.txtAmpCh4.Text = this.intensitéMAX[3];
                listBox1.Items.Add(lsReçu);
                // Scroll to bottom
                listBox1.TopIndex = listBox1.Items.Count - 1;
                // Refresh
                listBox1.Refresh();

            }// fin try 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        public void txtVoltCh1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCh1_Click(object sender, EventArgs e)
        {

        }

        private void btnCh2_Click(object sender, EventArgs e)
        {

        }

        private void txtVoltCh2_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void txtAmpCh1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAmpCh2_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MonTimer();
        }

        private void txtVoltCh3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtVoltCh4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAmpCh3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAmpCh4_TextChanged(object sender, EventArgs e)
        {

        }

        private void Ch1Gris_Click(object sender, EventArgs e)
        {
            Ch1Gris.Visible = true;
            Ch1Gris.Visible = false;
        }

        private void Ch2Gris_Click(object sender, EventArgs e)
        {
            Ch2Gris.Visible = true;
            Ch2Gris.Visible = false;
        }

        private void Ch3Gris_Click(object sender, EventArgs e)
        {
            Ch3Gris.Visible = true;
            Ch3Gris.Visible = false;
        }

        private void Ch4Gris_Click(object sender, EventArgs e)
        {
            Ch4Gris.Visible = true;
            Ch4Gris.Visible = false;
        }

        private void Ch1Vert_Click(object sender, EventArgs e)
        {
            Ch1Vert.Visible = true;
            Ch1Vert.Visible = false;
        }

        private void Ch2Vert_Click(object sender, EventArgs e)
        {
            Ch2Vert.Visible = true;
            Ch2Vert.Visible = false;
        }

        private void Ch3Vert_Click(object sender, EventArgs e)
        {
            Ch3Vert.Visible = true;
            Ch3Vert.Visible = false;
        }

        private void Ch4Vert_Click(object sender, EventArgs e)
        {
            Ch4Vert.Visible = true;
            Ch4Vert.Visible = false;
        }

        private void Ch1Bleu_Click(object sender, EventArgs e)
        {
            Ch1Bleu.Visible = true;
            Ch1Bleu.Visible = false;
        }

        private void Ch2Bleu_Click(object sender, EventArgs e)
        {
            Ch2Bleu.Visible = true;
            Ch2Bleu.Visible = false;
        }

        private void Ch3Bleu_Click(object sender, EventArgs e)
        {
            Ch3Bleu.Visible = true;
            Ch3Bleu.Visible = false;
        }

        private void Ch4Bleu_Click(object sender, EventArgs e)
        {
            Ch4Bleu.Visible = true;
            Ch4Bleu.Visible = false;
        }

        private void Rmt_Click(object sender, EventArgs e)
        {
            Rmt.Visible = true;
            Rmt.Visible = false;
        }

        private void OutpGris_Click(object sender, EventArgs e)
        {
            OutpGris.Visible = true;
            OutpGris.Visible = false;
        }

        private void OutpBlanc_Click(object sender, EventArgs e)
        {
            OutpBlanc.Visible = true;
            OutpBlanc.Visible = false;
        }

        private void cboPort_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}

