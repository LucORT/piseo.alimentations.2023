from asyncio import CancelledError
from lib2to3.pgen2.token import AMPER
import pyvisa as visa
import time

class Keithley:
    resource = None
    inst = None
    def __init__(self, ressource):
        print("Constructeur")
        self.ressource = ressource
        rm = visa.ResourceManager()
        rm.list_resources()
        self.inst = rm.open_resource(ressource)
        print(self.inst)

    def __del__(self):
        print("Destructeur")    
        if self.inst != None:       #LG
            self.inst.close()

    def Alim(self):
        alim = self.inst.query("\n*idn?")
        return(alim)
    def ChangerVolt(self, Volt):
        changeVolt = print(self.inst.write("VOLT " + Volt))
        return(changeVolt)
    def ChangerAmp(self, Amp):
        changeAmp = print(self.inst.write("CURRENT " + Amp))
        return(changeAmp)
    def Write(self):
        Ecriture = input("Entrez votre commande: ")
        print(self.inst.write(Ecriture))
        return()
    def Query(self):
        Demande = input("Entrez votre demande: ")
        print(self.inst.query(Demande))
        return()
    def Clignoter(self):
        while True:
            print(self.inst.write("current 0.1"))
            time.sleep(0.1)
            print(self.inst.write("current 0"))
            time.sleep(0.1)
    def Mesures(self):
        Mesure1 = int(input("Quelle unité voulez-vous mesurer ? \n \
            1. Les volts \n \
            2. Les ampères \n"))
        if Mesure1 == 1:
            print("Volt réel: ") 
            print(self.inst.query("fetc:volt?")) 
            print("Volt objectif: ") 
            print(self.inst.query('volt?'))
        if Mesure1 == 2:
            print("Amp réel: ") 
            print(self.inst.query("fetc:curr?")) 
            print("Amp objectif: ") 
            print(self.inst.query('curr?'))
    def Output(self):
        out = self.inst.query("output?")
        out = int(out)
        if out == 0:
            print(self.inst.write("output on"))
            return('allumer')
        if out == 1:
            print(self.inst.write("output off"))
            return("eteindre")

    def DemanderVoltsObjectif(self):
        return self.inst.query('volt?')
    def DemanderVoltsRéel(self):
        return self.inst.query("fetc:volt?")
