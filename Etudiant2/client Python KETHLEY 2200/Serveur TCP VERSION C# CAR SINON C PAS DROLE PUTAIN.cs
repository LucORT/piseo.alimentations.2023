﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Keithley2600Simulator
{
    internal class SynchronousSocketListener
    {
        public static void Main(string[] args)
        {
            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 5025);

                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.

                Console.WriteLine("En attente de connexion...");

                while (true)
                {
                    // Program is suspended while waiting for an incoming connection.
                    Socket handler = listener.Accept();
                    Console.WriteLine($"Connexion acceptée de {handler.RemoteEndPoint}");
                    bytesRec = handler.Receive(bytes);
                    // An incoming connection needs to be processed.
                    //Thread t = new Thread(() => ProcessConnexion(handler));
                    //t.Start();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(1);
            }
        }

        private static void ProcessConnexion(Socket handler)
        {
            try
            {
                byte[] bytes = new Byte[1024];
                int bytesRec = 0;
                string data = "";

                // check if more data is available
                while (handler.Available > 0 || !data.Contains("endscript"))
                {
                    bytesRec = handler.Receive(bytes);
                    Console.WriteLine(bytesRec);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    Console.WriteLine(data);
                }

                // Show the data on the console.
                Console.WriteLine("Text received : {0}", data);

                string sRep = "Capacity, 6.990100e+04" + Environment.NewLine + "NumReading, 2.000000e+00" + Environment.NewLine
                              + "Appendmode, 1.000000e+00" + Environment.NewLine + "TimestampResolution, 1.000000e-06" + Environment.NewLine
                              + "BaseTimeStamp, 1.571479e+09" + Environment.NewLine + Environment.NewLine + "Reading" + Environment.NewLine
                              + "4.000210e+01" + Environment.NewLine + "-4.146099e-04" + Environment.NewLine + Environment.NewLine
                              + "Donnees des pulses" + Environment.NewLine
                              + "Mesure Fonction, Mesure Range, Status, TimeStamp, Source Values, Source Fonction, Source Range, Source OutPut States," +
                              Environment.NewLine
                              + "Voltage, 4.000000e+01, 8.400000e+01, 0.000000e+00, 1.000000e-01, Current, 1.000000e-01, On" + Environment.NewLine
                              + "Current, 1.000000e+00, 2.800000e+01, 2.092897e+00, 0.000000e+00, Voltage, 1.000000e-01, Off" + Environment.NewLine;

                // Echo the data back to the client.
                byte[] msg = Encoding.ASCII.GetBytes(sRep);
                handler.Send(msg);
                
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
