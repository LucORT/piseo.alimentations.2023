from ClassKeithley2200 import Keithley
import sys
print(sys.version)
# Voir https://www.ni.com/docs/fr-FR/bundle/ni-visa/page/ni-visa/visaresourcesyntaxandexamples.html
# USB[board]::manufacturer ID::model code::serial number[::USB interface number][::INSTR]
# USB0      -> USB[board]
# 0x05E6    -> manufacturer ID
# 0x2200    -> model code
# 1369387   -> serial number
#           -> [::USB interface number]
# INSTR     -> [::INSTR]

# pip install pyvisa
# pip install pyvisa-py
# pip install pyusb
# pip install zeroconf

import pyvisa as visa
print ("0 ------------------------------------------")
rm = visa.ResourceManager()
print ("1 ------------------------------------------")

print ("2 ------------------------------------------")
maKeithley = None
if 0:
    # Accéder au périphérique en TCP 
    # voir le serveur ad-hoc dans /home/luc/partage_win10/Luc/Lycée ORT/TSII/2 TS/Projets/Piseo Alimentations/ServeurTCPPython/ServeurTCPPython.py pour lequel cette ligne sert de test
    # 1112 est le port d'écoute, il s'écrit après l'IP, avec un séparateur virgule
    # si on ne fournit pas l'info, VISA va demander le N du port de l'appareil sur le port 111 de ce même appareil
#    inst = rm.open_resource('TCPIP::127.0.0.1,1112::INSTR')
    maKeithley = Keithley('TCPIP::127.0.0.1,1112::INSTR') #Preciser pour les IHM de bien accepter l'ethernet
else:
    print ("2b ------------------------------------------")
    # # Accéder au périphérique en USB
    # # LG : une des méthodes clés de la reconnaissance d'un périphérique USB Visa est /home/luc/.local/lib/python3.8/site-packages/pyvisa_py/protocols/usbtmc.py->is_usbtmc. Elle sert à déterminer si le device est un USB Test and Measurement Class (USBTMC) instrument
    rm.list_resources()     # LG : à quoi sert cette ligne ?
    # inst = rm.open_resource('USB0::0x05E6::0x2200::1369387::INSTR')
    # print ("3 ------------------------------------------")
    maKeithley = Keithley('USB0::0x05E6::0x2200::1369387::INSTR')
    
# Recherches LG :
# "Introduction to Programming USB Devices in VISA" : https://www.ni.com/docs/fr-FR/bundle/ni-visa/page/ni-visa/introprogrammingusbdevicesni-visa.html
# "Configuring NI-VISA to recognize a RAW USB Device" : https://www.ni.com/docs/fr-FR/bundle/ni-visa/page/ni-visa/configuringni-visarecognizerawusbdevice.html
# "VISA tester tool" : https://www.rohde-schwarz.com/fr/driver-pages/commande-a-distance/3-visa-and-tools_231388.html
# Bibliothèque Python de simulation de périphérique VISA : https://pypi.org/project/PyVISA-sim/
# Retrouver les informations à partir des données USB : https://the-sz.com/products/usbid/
    # ex : vendor Id = 0x13B1 -> Linksys; 0x4b8 => Seiko Epson
# Codes des langues USB : http://www.baiheee.com/Documents/090518/090518112619/USB_LANGIDs.pdf (0x0409 -> English (United States))
# listes les périphériques USB (Linux) : https://linuxhint.com/list-usb-devices-linux/
# RIEN A VOIR # Emulation USB en python (prjet USB/IP) : https://github.com/smulikHakipod/USB-Emulation, https://breaking-the-system.blogspot.com/2014/08/emulating-usb-devices-in-python-with-no.html, https://usbip.sourceforge.net/, https://github.com/cezanne/usbip-win, https://www.howtoforge.com/how-to-set-up-a-usb-over-ip-server-and-client-with-ubuntu-10.04
# Emulation USB et C++ pour Win10 : https://learn.microsoft.com/en-us/windows-hardware/drivers/usbcon/writing-a-ude-client-driver
# LibUSBDotNet : https://sourceforge.net/projects/libusbdotnet/files/LibUsbDotNet/LibUsbDotNet%20v2.2.8/
# Ecrire un pilote Windows : https://learn.microsoft.com/fr-fr/windows-hardware/drivers/?redirectedfrom=MSDN
# USB gadget mode : https://www.kernel.org/doc/html/v5.0/driver-api/usb/gadget.html, https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget
def Menu():
    while True:
        #Menu textuel pour choisir la prochaine action
        try:
            Menu = input('Que voulez vous faire ?\n '
                + '0. Afficher nom de l\'alimentation\n '
                + '1. Allumer/Eteindre sortie\n '
                + '2. Changer les Volts\n '
                + '3. Changer les Ampères\n '
                + '4. Ecriture manuelle\n '
                + '5. Demande manuelle\n '
                + '6. Prendre mesure\n '
                + '7. Quitter\n')
        except:
            print("EOF")
            Menu = "-1"

        Menu = int(Menu)
        
        #La requête est choisis en fonction de la valeur entrée
        if Menu == 0:
            print(maKeithley.Alim()) 
        if Menu == 1:
            print(maKeithley.Output())
        if Menu == 2:
            Volt = input("Entrez valeur des Volts (30.1 max): ")
            print(maKeithley.ChangerVolt(Volt))
        if Menu == 3:
            Amp = input("Entrez valeur des Ampères (5 max): ")
            print(maKeithley.ChangerAmp(Amp))
        if Menu == 4:
            print(maKeithley.Write())
        if Menu == 5:
            print(maKeithley.Query())
        if Menu == 6:
            print(maKeithley.Mesures())
        if Menu == 7:
            break
        if Menu == 100:
            print(maKeithley.Clignoter())

if 0:
    Menu()
else:
    # Pour tests sans menu
#    print("Alim() => " + maKeithley.Alim())
    print("ChangerVolt(10) => " + (maKeithley.ChangerVolt("10") or "None"))
    print("maKeithley.DemanderVoltsObjectif() => " + maKeithley.DemanderVoltsObjectif())
    print("maKeithley.DemanderVoltsRéel() => " + maKeithley.DemanderVoltsRéel())

maKeithley = None
toto = 0
