﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace KEITHLEY_SERV
{
    internal class Serveur
    {
        float VoltageMax;
        float IntensitéMax;

        public void Lance(System.Windows.Forms.ListBox laListe)
        {
            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 5025);

                // Start listening for connections.

                laListe.Invoke(new Action(() => laListe.Items.Add("En Attente de Connexion"))) ;

                listener.Bind(localEndPoint);
                listener.Listen(10);

                while (true)
                {
                    // Program is suspended while waiting for an incoming connection.
                    Socket handler = listener.Accept();
                    laListe.Invoke(new Action(() => laListe.Items.Add($"Connexion acceptée de {handler.RemoteEndPoint}")));

                    byte[] bytes = new Byte[1024];
                    int bytesRec = 0;
                    bytesRec = handler.Receive(bytes);

                    String Commande =Encoding.UTF8.GetString(bytes);
                    laListe.Invoke(new Action(() => laListe.Items.Add("Reçu : " + Commande)));
                    String Reponse = this.TraiteCommande(Commande);
                    laListe.Invoke(new Action(() => laListe.Items.Add("Renvoyé : " + Reponse)));
                    Reponse = "5";
                    laListe.Invoke(new Action(() => laListe.Items.Add("Repondu : " + Reponse)));

                    // An incoming connection needs to be processed.
                    //Thread t = new Thread(() => ProcessConnexion(handler));
                    //t.Start();
                    byte[] bytessend = Encoding.UTF8.GetBytes(Reponse);
                    handler.Send(bytessend);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(1);
            }
        }

   public string TraiteCommande(string Commande)
        {
            String Reponse = "";
            if (Commande == "VOLT?")
            {
                Reponse = VoltageMax.ToString();
            }
            if (Commande == "CURR?")
            {
                Reponse = IntensitéMax.ToString();
            }
            if (Commande == "*IDN?")
            {
                Reponse = "0";
            }
            if (Commande == "MEAS:VOLT?")
            {
                Reponse = VoltageMax.ToString();
            }
            if (Commande == "MEAS:CURR?")
            {
                Reponse = IntensitéMax.ToString();
            }
            if (Commande.Contains("APPLY"))
            {
                Commande = Commande.Replace("APPLY ", "");
                String Volt = Commande.Split(',') [0];
                String Intensité = Commande.Split(',')[1];
                VoltageMax = Convert.ToSingle (Volt);
                IntensitéMax = Convert.ToSingle (Intensité);
            }
            if (Commande == "OUTP:GEN?")
            {
                Reponse = "OUTP:GEN ON";
            }



                return Reponse; 
        }
    }

}
