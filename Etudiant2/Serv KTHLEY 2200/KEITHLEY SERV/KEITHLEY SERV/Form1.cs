namespace KEITHLEY_SERV
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {

            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(this.lanceServeurTCP));
            t.Start();
        }

        private void lanceServeurTCP()
        {
            Serveur MonServeur = new Serveur();
            MonServeur.Lance(this.listBox1);
        }
    }
}