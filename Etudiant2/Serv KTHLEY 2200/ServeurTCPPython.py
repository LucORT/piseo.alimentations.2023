# Serveur VISA pour le contrôle d'instruments virtuels
# spécifications protocole VISA : https://www.ivifoundation.org/docs/vpp43_2016-02-26.pdf
# Je n'ai pas trouvé de description exacte du protocole TCP Visa, mais pyvisa_py.protocols.rpc contient tout le code nécéssaire pour le comprendre, y compris le code des clients et surtout celui des serveurs
# Pour l'encodage et le décodage des trames TCP, pyvisa_py.protocols.rpc se base sur struct.pack/struct.unpack, et sur xdrlib ("Encode and decode XDR data", https://docs.python.org/3/library/xdrlib.html)
# Le dialogue se déroule à ce que j'ai compris en 2 temps
    # 1 : le client demande au serveur TCP, SUR LE PORT 111, que est le port sur lequel celui-ci écoutera les demandes ultérieures
    # 2 : le client utilise ce port pour les demandes suivantes
    # NB :  le port 111 est bloqué par le système et il faudra exécuter le .py en root. 
    # En attendant, c'est le port 1111 qui a été utilisé, et une modification en dur a été faite TEMPORAIREMENT dans /home/luc/.local/lib/python3.8/site-packages/pyvisa_py/protocols/rpc.py (ligne 803)
# Pour exploiter ce projet, il faut
    # exécuter de programme (ce qui met le serveur en attente)
    # exécuter Keithley2200.py (qui est le client qui interroge ce serveur, situé dans /home/luc/partage_win10/Luc/Lycée ORT/TSII/2 TS/Projets/Piseo Alimentations/Codes Hameg récupérés (session 2022 et annotés par LG)/CodeKeithley/ sur poste LG)
# LG 20230207

# FAIT au 07/03/2023
# Le serveur écoute le client et sait répondre à la première partie du dialogue : la demande du port d'écoute

# FAIT au 23/03/2023
# Trouvé un moyen de se passer du Serveur1 qui écoute sur le port 111 : 
    # la ressource doit s'écrire 'TCPIP::<AdresseIP>,<Port>::INSTR'
    # , ici pour une écoute sur le port 1112 de localhost, ça donne 'TCPIP::127.0.0.1,1112::INSTR'
#   -> ce serveur est celui qui écoute sur le port 1112 et qui rend les services Visa
# Le serveur de dialogue écoute le client et sait répondre à une commande SCPI

# TODO
# résoudre l'erreur qui apparait dans la console chez le client à la fin du traitement (destructeur ?), et qui montre que la déconnexion n'est pas bien gérée par le serveur
# modifier ce code pour que la partie socket soit prise en charge par un client C#, et que seul la partie traitement (méthodes handle_XX) soit errectué en Python
    # pour lappel de code Python depuis C#, voir https://stackoverflow.com/questions/579272/instantiating-a-python-class-in-c-sharp

# ---------------------------------------------------
# imports utiles pour la fonction "aLaMain"
import socket
import struct
from pyvisa_py.protocols.rpc import Packer
from pyvisa_py.protocols.rpc import PortMapperUnpacker
from pyvisa_py.protocols.rpc import PortMapperVersion

# ---------------------------------------------------
# imports utiles pour la fonction "avecPyvisa"
from pyvisa_py.protocols.rpc import TCPServer
from pyvisa_py.protocols.rpc import AcceptStatus

# ---------------------------------------------------
# Fonction utilisant le serveur codé dans 
def avecPyvisa():

    if 0:
        # Initialiser le serveur qui permet de connaitre le port sur lequel le serveur de dialogue de l'instrument écoute
        print("On devrait utiliser ici le port 111")
        print("Pour que ce serveur soit appellé sur le port 1111, il faut modifier la ligne 803 de /home/luc/.local/lib/python3.8/site-packages/pyvisa_py/protocols/rpc.py")

        # 100000 est la constante PMAP_PROG, c'est la valeur à utiliser pour le programme serveur dédié à fournir le port d'écoute
        loServer = serveurPersonnalise('', 100000, 2, 1111)     # 1111 est le port, pour être standard ça devrait $etre 111, mais ce port est bloqué par le système et il faudra exécuter le .py en root
    else:
        # Initialiser le serveur de dialogue de l'instrument : on a choisi arbitrairement le port 1112
        # DEVICE_CORE_PROG  = 0x0607AF, c'est la constante pour le prg serveur d'un appareil$
        loServer = serveurPersonnalise('', 0x0607AF, 1, 1112)

    loServer.loop()
    print("On sort du loop")

# ---------------------------------------------------
# surcharge de TCPServer de la bibliothèque pyvisa_py (fichier rpc.py)
class serveurPersonnalise(TCPServer):
    
    isListening = True      # propriété ajoutée pour gérer la fin de la communication avec un client

    def __init__(self, host, prog, vers, port):
        print("Initialisation du serveur")
        return TCPServer.__init__(self, host, prog, vers, port)

    # surcharge de TCPServer, pour affichage dans la console
    def connect(self):
        print("Acceptation d'une connexion entrante")
        return TCPServer.connect(self)
        
   # surcharge de TCPServer, pour affichage dans la console
    def loop(self):
       print("Démarrage de la boucle d'acceptation des clients")
       return TCPServer.loop(self)
        
    # # surcharge de TCPServer, pour affichage dans la console
    # def session(self, connection):
    #     print("session")
    #     return TCPServer.session(self, connection)
    # remplacement de la méthode natine de TCPServer
    # Cette surcharge permet d'ajouter une sortie de boucle infinie lorsque le client fait une demande de déconnexion (recue par la procédure handle_23)
    # C'est étrange que le cas ne soit pris en charge par les classes Visa d'origine, mais c'est sans doutes que je n'ai pas tout compris
    # Une piste pourrait être la donnée "abort port" reçue par la méthode "handle_10"
    def session(self, connection):
        print("Démarrage de session de communication avec un nouveau client")
        sock, (host, port) = connection

        while self.isListening:     # code d'origine :       while 1:
            try:
                from pyvisa_py.protocols.rpc import _recvrecord     # Ajout lié à la surcharge
                call = _recvrecord(sock, None)
            except EOFError:
                break
            except socket.error:
    #            logger = logging.LoggerAdapter(logger, {"backend": "py"})
    #            logger.exception("socket error: %r", sys.exc_info()[0])
                break
            reply = self.handle(call)
            if reply is not None:
                from pyvisa_py.protocols.rpc import _sendrecord     # Ajout lié à la surcharge
                _sendrecord(sock, reply)

# Code ajouté début
            if not self.isListening:
                # On a demandé de fermer la session : terminer maintenant la boucle avec notre client
                break
        
        # Repasser en mode écoute
        self.isListening = True
        print("Fin de session de communication ")
# Code ajouté fin

    # surcharge de TCPServer, pour affichage dans la console
    def handle(self, call):
        print("Prise en charge d'une demande du client")
        return TCPServer.handle(self, call)
    
    # méthode appellée par self.handle, ajoutée car absente de la classe de base
    # le principe de TCPServer semble être de prévoir l'appel à des fonctions (une par type de demande), qui doivent être créées dans la classe héritière
    # 3 : get_port
    # procédure appellée par le client si on ne lui a pas fourni le port sur lequel se fera la conversation
    # Cette procédure se contente de renvoyer le port sur lequel se fera la conversation
    # (en principe, cette prise en charge est effectuée par une instance spécifique du serveur Visa, qui écoute sur le port 111 -on peut l'appeller "Serveur1"-
    # , un autre serveur Visa -qu'on peut appeller "Serveur2" doit écouter sur le port que renvoie Serveur1, il est en charge de tout le reste des échanges)
    def handle_3(self):
        print("handle_3 : prise en charge d'une demande du port d'écoute du serveur \"Serveur2\"")

        # Il n'y a rien d'important à récupérer dans les informations envoyées par le client

        # Répondre au client VISA
        self.packer.pack_uint(AcceptStatus.success)     # Statut : succès
        self.packer.pack_uint(1112)                     # Valeur de retour : N° du port d'écoute de Serveur2
    
    # méthode appellée par self.handle, ajoutée car absente de la classe de base
    # le principe de TCPServer semble être de prévoir l'appel à des fonctions (une par type de demande), qui doivent être créées dans la classe héritière
    # 10 : get_link
    # procédure appellée par le client au travers de la méthode Vxi11Unpacker.unpack_create_link_resp (vxi11.py)
    def handle_10(self):
        print("handle_10 : établissement du lien avec le client")

        # Il n'y a rien d'important à récupérer dans les informations envoyées par le client

        # Répondre au client VISA
        self.packer.pack_uint(AcceptStatus.success)     # Statut : succès
        self.packer.pack_uint(0)                        # Valeur de retour : code d'erreur
        self.packer.pack_uint(1111)                     # Valeur de retour : link
        self.packer.pack_uint(1112)                     # Valeur de retour : abort port, je n'ai pas compris le rôle de cette donnée, mais elle doit jouer un rôle dans la déconnexion
        self.packer.pack_uint(9999)                     # Valeur de retour : max_recv_size
    
    # méthode appellée par self.handle, ajoutée car absente de la classe de base
    # 11 : DEVICE_WRITE, envoyer une demande à l'instrument (?)
    # a priori, l'appel de cette prodédure par le client Visa est toujours suivie par l'appel de handle_12, qui renvoie la réponse de l'instrument
    # procédure appellée par le client au travers de la méthode Vxi11Unpacker.unpack_device_write_resp (vxi11.py)
    # elle est appellée par le client dans le cadre de 2 méthodes de l'instance de ressource Visa : "write" et "query"
    def handle_11(self):
        print("handle_11 : réception d'une demande du client")
        
        # Récupérer les informations envoyées par le client Visa, en particulier la dernière : la commande SCPI à exécuter par l'instrument
        # le pack des éléments du message a été fait par Vxi11Packer.pack_device_write_parms (vxi11.py)
        # On reprend les éléments dans l'ordre, même si on ne sait pas à quoi ils servent
        liLink = self.unpacker.unpack_int()
        liIOTimeout = self.unpacker.unpack_uint()
        liLockTimeout = self.unpacker.unpack_uint()
        liFlags = self.unpacker.unpack_uint()
        lbCommande = self.unpacker.unpack_opaque()      # La commande Visa, en bytes

        # Convertir la commande en string, majuscules et éliminer les sauts de ligne éventuels
        lsCommande = lbCommande.decode("utf-8").upper().replace('\n', '').replace('\r', '')
        print("handle_11 : la commande SCPI est : " + lsCommande)
        
        # Exécuter la commande SCPI et récupérer la réponse pour exploitation par self.handle_12 qui sera appellée juste après
        # Il n'est pas nécessaire faire en sorte que cette réponse mémorisée soit différente selon le client demandeur
        #, car de toute façons ce serveur ne peut écouter qu'un client à la fois (voir méthode "Session", dont la boucle de conversation est infinie)
        self.sLastResponse = self.executeSCPI(lsCommande)

        # Répondre au client VISA
        self.packer.pack_uint(AcceptStatus.success)     # Statut : succès
        liSize = len(lbCommande)
        self.packer.pack_uint(0)                        # error (???)
        self.packer.pack_uint(liSize)                   # Size de la commande reçue
    
    # méthode spécifique pour l'exécution d'une commande SCPI
    # cette méthode devra demander au serveur C# de faire le travail
    # ici, on se contente pour l'instant de faire un traitement de test
    import socket

    def executeSCPI(self, psCommande):
     HOST = '127.0.0.1'
     PORT = 5025

     client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
     client.connect((HOST, PORT))
     print ('Connexion vers' + HOST + ':' + str(PORT) + ' reussie.')
     client.send(psCommande.encode("utf-8"))
     reponse = client.recv(200)
     reponse = reponse.decode("utf-8")
     return reponse

     #TODO CLIENT SOCKET EN PYTHON (SE RENSEGNER)

    # méthode appellée par self.handle, ajoutée car absente de la classe de base
    # 12 : DEVICE_READ, lire la réponse de l'instrument ?
    # a priori, l'appel de cette prodédure par le client Visa est toujours précédée par l'appel de handle_11, qui contient la commande en langage SCPI faite à l'instrument
    # procédure appellée par le client au travers de la méthode Vxi11Unpacker.unpack_device_read_resp (vxi11.py)
    # elle n'est appellée par le client que dans le cadre de la méthode "query" de l'instance de ressource Visa
    def handle_12(self):
        print("handle_12 : demande de la réponse à la commande envoyée précédemment et traitée par handle_11")

        # Répondre au client VISA
        # le pack des éléments du message a été fait par Vxi11Packer.pack_device_read_parms (vxi11.py)
        # On reprend les éléments dans l'ordre, même si on ne sait pas à quoi ils servent
        liLink = self.unpacker.unpack_int()
        liRequestSize = self.unpacker.unpack_uint()
        liIOTimeout = self.unpacker.unpack_uint()
        liLockTimeout = self.unpacker.unpack_uint()
        liFlags = self.unpacker.unpack_uint()
        liTermChar = self.unpacker.unpack_uint()

        self.packer.pack_uint(AcceptStatus.success)     # Statut : succès
        
        # Récupérer la dernière réponse (déterminée par self.executeSCPI, appellée par self.handle_11)
        lbRep = self.sLastResponse
        print("handle_12 : la réponse SCPI est : " + lbRep.decode("utf-8"))

        # Répondre au client VISA
        self.packer.pack_uint(0)                        # error (0 semble indiquer que tout est OK ???)
        self.packer.pack_uint(6)                        # reason (la valeur 6 = vxi11.RX_END | vxi11.RX_CHR fait terminer la boucle d'attente de la fin de la réponse par le client???)
        self.packer.pack_opaque(lbRep)                  # Valeur de retour

    # méthode appellée par self.handle, ajoutée car absente de la classe de base
    # 23 : abort, fermer la session à la demande du client
    def handle_23(self):
        print("handle_23 : demande de déconnexion")

        # Libération du serveur pour qu'il accepte un autre client
        self.isListening = False
        
        # Répondre au client VISA
        # Dans ce contexte, je ne sais même pas s'il attend une réponse, mais en lui envoyant "success", cela fonctionne
        self.packer.pack_uint(AcceptStatus.success)     # Statut : succès
        
    # ajout qui ne semble finclament pas nécessaire
    def close(self):
        print("close : fermeture de la socket de conversation")
        self.sock.shutdown(socket.SHUT_RDWR)  # selon https://stackoverflow.com/questions/23733719/python-socket-close-does-not-work-until-the-script-exit
        self.sock.close()


# Lancement du seveur qui utilise la classe TCPServer de la bibliothèque pyvisa_py.protocols.rpc
avecPyvisa()



# ---------------------------------------------------
# Fonction où tout est fait à la main et utilisant struct.pack/struct.unpack, et sur xdrlib
def aLaMain(): 

    ADRESSE = '127.0.0.1'

    #Devrait être 111, maos ce port est interdit sans droits de root
    PORT = 1111

    serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serveur.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)   # selon https://stackoverflow.com/questions/41208720/python-sockets-not-really-closing
    serveur.bind((ADRESSE, PORT))
    serveur.listen(1)
    print("Serveur en attente")
    client, adresseClient = serveur.accept()
    print ('Connexion de ', adresseClient)

    try:
        donnees = client.recv(1024)
        if not donnees:
            print ('Erreur de reception.')
        else:
            print ('Reception brute de:' + str(donnees))
            loPortUnpacker = PortMapperUnpacker("")
            loPortUnpacker.reset(donnees)
    #        xid, verf = loUnpacker.unpack_replyheader()
    #        laDonnées = loPortUnpacker.unpack_mapping()
    #        loPortUnpacker.reset("")        # Pourquoi reste-t-il des données dans le buffer ???

            # Extraire l'information sur la taille de la demande (ou du fragment de demande)
            liTailleDemande = struct.unpack(">I", donnees[:4])
            donnees = donnees[4:]

            laDonnées = loPortUnpacker.unpack_callheader()
            loPortUnpacker.done()
            xid=laDonnées[1]
            codeDemande = laDonnées[0]
            print ('Reception tableau de:' + str(laDonnées) + " xid = " + str(xid))

    #        reponse = donnees.upper()
            # Construction de la réponse

            # Données de l'en-tête
            loPacker = Packer()
            loPacker.pack_replyheader(xid                   # id de la demande
                                            , [                 # Tableau [<flavor>, <stuff>] pour self.pack_auth 
                                                1               # flavor (doit être int)
                                                , b"stuff"      # stuff (doit être byte)
                                                ]               
                                                )

            # Encodage de l'en-tête réponse
            reponse = loPacker.get_buffer()

            if codeDemande == PortMapperVersion.get_port:
                # Réponse attendue : N° de port
                # loPacker.pack_uint(1111)
                reponse +=  struct.pack(">L", 1111)
            else:
                raise AssertionError("Demande non prévue : " + str(codeDemande))

            # La réponse commence par un entier sur 4 bytes 4 bytes pour la longueur de la réponse, en big endian
                # Inclut 0x80000000 -> fin de la réponse (dernier fragment)
                # Doit inclure 0x7FFFFFFF si ce message n'est pas fragmenté 
            reponse = struct.pack(">I", len(reponse)) + reponse 

            print ('Envoi de :' + str(reponse))

            n = client.send(reponse)
            if (n != len(reponse)):
                print ('Erreur envoi.')
            else:
                print ('Envoi ok.')

    except Exception as e :
        print("Erreur : " + str(type(e)))
    finally:
        print ('Fermeture de la connexion avec le client.')
        client.close()
        print ('Arret du serveur.')
        serveur.shutdown(socket.SHUT_RDWR)  # selon https://stackoverflow.com/questions/23733719/python-socket-close-does-not-work-until-the-script-exit
        serveur.close()
