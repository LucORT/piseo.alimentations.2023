import sys
import os
FILE_DIR = os.path.abspath(__file__) #on recupere le dossier dans lequel le fichier est lancé
SCRIPT_DIR = os.path.dirname(FILE_DIR) #on recupere le dossier racine du fichier
SCRIPT_DIR = os.path.dirname(SCRIPT_DIR) #on remonte d'un dossier
SCRIPT_DIR = os.path.dirname(SCRIPT_DIR) #on remonte d'un dossier
sys.path.append(SCRIPT_DIR) #on ajoute le dossier alim-p a la liste d'ou l'on trouve les modules
#
from tkinter import *  # imporatation de tkinter
from tkinter import ttk  # imporatation des combobox
import json  # imporatation des json
from Etudiant4.alimPiseo.alim.classeHMP4040 import HMP4040
#from hameg.classeHMP4040 import HMP4040  # importation des classes
import csv  # imporatation des csv
from datetime import datetime  # imporatation de la date
import serial.tools.list_ports as port_list #imporatation de la liste des ports
from tkinter import filedialog  # boite de dialogue
import pandas as pd  # gestion des tableau
import platform #permet de connaitre la platforme
#
class FHameg4040:
    #
    def __init__(self):
        self.system = platform.system()  #variable qui permet de vérifier le system
        #
        if self.system == "Windows": 
            chemin = 'Etudiant 6/tkinter'
        else:
            FILE_DIR = os.path.abspath(__file__) #on recupere le dossier dans lequel le fichier est lancé
            SCRIPT_DIR = os.path.dirname(FILE_DIR) #on recupere le dossier racine du fichier
            chemin = SCRIPT_DIR
        #
        self.FichierCSV = chemin+'/fichier.csv'
        #
        self.Ohameg = NONE
        #
        # ------------------Fenetre------------------#
        self.FHameg = Tk()
        self.FHameg.geometry('1270x700')
        self.FHameg.title('Hameg HMP4040')
        self.FHameg['bg'] = 'white'
        self.FHameg.resizable(height=True, width=True)
        if self.system == "Windows":
            self.FHameg.iconbitmap("Etudiant 6/tkinter/img/eclair.ico")

        #
        # ------------------BOITE------------------#
        self.boiteEtape = LabelFrame(self.FHameg, bg='white', bd='2', height=50, width=800)
        self.boiteEtape.place(x='450', y='15')
        #
        self.boiteEntry = LabelFrame(self.FHameg, bg='white', bd='2', height=525,width=400, text="Valeurs maximales", font=("Verdana", 25))
        self.boiteEntry.place(x=10, y=90)
        #
        self.boiteAffichage = LabelFrame(self.FHameg, bg='white', bd='2', height=360, width=800, text="Valeurs réelles", font=("Verdana", 25))
        self.boiteAffichage.place(x=450, y=90)
        #
        # ------------------LABEL------------------#
        self.btns = []  # liste des boutons
        self.Entre1 = []
        self.Entre2 = []
        self.ListeVA = []
        self.ListeNB = []
        Y = 20  # position y des element des la boucle
        for element in range(4):
            #
            self.VA = Label(self.boiteEntry, text="V :\nA :",font=("Verdana", 25), bg="white", state=DISABLED)
            self.VA.place(x='25', y=Y)
            self.ListeVA.append(self.VA)
            #
            self.NB = Label(self.boiteEntry, text=(":", element+1), font=("Verdana", 25), bg="white", state=DISABLED)
            self.NB.place(x='300', y=Y+15)
            self.ListeNB.append(self.NB)
            #
            # ------------------ENTREE USER------------------#
            variableV = IntVar()
            # validate="key" permet de vérifier le caractère entré dans le champ
            # validatecommand permet de définir la fonction de vérification / le '%P' est la variable pour récupérer le caractère
            self.entree1 = Entry(self.boiteEntry, textvariable=variableV, bd='5', validate="key", validatecommand=(self.FHameg.register(self.valider_nombre), '%P'), state=DISABLED)
            self.entree1.place(x=100, y=Y+17)
            self.Entre1.append(self.entree1)
            #
            self.variableA = IntVar()
            self.entree2 = Entry(self.boiteEntry, textvariable=self.variableA, bd='5', validate="key", validatecommand=(self.FHameg.register(self.valider_nombre), '%P'), state=DISABLED)
            self.entree2.place(x=100, y=Y+55)
            self.Entre2.append(self.entree2)
            #
            # ------------------BOUTON------------------#
            self.btnON = Button(self.boiteEntry, text="OFF", bg="red", fg="white",cursor='hand2', command=lambda x=element: self.ActivationChannel(x), state=DISABLED)
            self.btnON.place(x=250, y=Y+30)
            self.btns.append(self.btnON)
            #
            Y = Y+120
            #
        #
        # ------------------ENTREE USER------------------#
        self.variableT = IntVar()
        self.PeriodeEnrg = Entry(self.FHameg, textvariable=self.variableT, bd='5', validate="key", validatecommand=(self.FHameg.register(self.valider_nombre), '%P'), state=DISABLED)
        # valeur par défaut du entry de la fréquence a 1
        self.PeriodeEnrg.insert(1, 1)
        self.PeriodeEnrg.place(x=880, y=459)
        #
        # ------------------LABEL------------------#
        self.Port = Label(self.FHameg, text="Port :",font=("Verdana", 25), bg="white")
        self.Port.place(x='20', y=15)
        #
        self.Etape = Label(self.boiteEtape, text="Étape 1 : Sélectionnez un port.", font=("Verdana", 25), bg="white")
        self.Etape.place(x=0, y=0)
        #
        self.FEchantillon = Label(self.FHameg, text="Période d'échantillonnage :", font=("Verdana", 20), bg="white", state=DISABLED)
        self.FEchantillon.place(x=450, y=450)
        #
        self.sec = Label(self.FHameg, text="s.",font=("Verdana", 20), bg="white", state=DISABLED)
        self.sec.place(x=1010, y=450)
        #
        Y = 35  # position y des element des la boucle pour les labels V/A et Ch
        self.ListeVA1 = []
        self.ListeVA2 = []
        self.ListeCh12 = []
        self.ListeCh34 = []
        for element in range(2):
            #
            self.Ch12 = Label(self.boiteAffichage, text=("Ch", element+1, ":"), font=("Verdana", 25), bg="white", state=DISABLED)
            self.Ch12.place(x=10, y=Y)
            self.ListeCh12.append(self.Ch12)
            #
            self.Ch34 = Label(self.boiteAffichage, text=("Ch", element+3, ":"), font=("Verdana", 25), bg="white", state=DISABLED)
            self.Ch34.place(x=420, y=Y)
            self.ListeCh34.append(self.Ch34)
            #
            self.VA1 = Label(self.boiteAffichage, text=("V :0\nA :0"), font=("Verdana", 25), bg="white", state=DISABLED)
            self.VA1.place(x=120, y=Y-20)
            self.ListeVA1.append(self.VA1)

            self.VA2 = Label(self.boiteAffichage, text=("V :0\nA :0"), font=("Verdana", 25), bg="white", state=DISABLED)
            self.VA2.place(x=530, y=Y-20)
            self.ListeVA2.append(self.VA2)
            #
            Y = Y+150
            #
        #
        # ------------------BOUTON------------------#
        self.retour = Button(self.FHameg, text="<- Retour", bg="gray", fg="white", cursor='hand2',font=("Verdana", 25), command=lambda: self.ouvrir_fenetre('ChoixAlim'))
        self.retour.place(x=450, y=500)
        #
        self.Breset = Button(self.FHameg, text="Reset", bg="gray", fg="white",cursor='hand2', font=("Verdana", 25), command=self.reset, state=DISABLED)
        self.Breset.place(x=660, y=500)
        #
        self.bEnvoyer = Button(self.FHameg, text="Envoyer\nvaleurs", bg="gray", fg="white",cursor='hand2', font=("Verdana", 15), command=self.envoyer, state=DISABLED)
        self.bEnvoyer.place(x=800, y=500)
        #
        self.bAllumer = Button(self.FHameg, text="Allumer", bg="green", fg="white",cursor='hand2', font=("Verdana", 25), command=self.allumerf, state=DISABLED)
        self.bAllumer.place(x=920, y=500)
        #
        self.Exit = Button(self.FHameg, text="Quitter", bg="gray", fg="white",cursor='hand2', font=("Verdana", 25), command=self.closeFenetre)
        self.Exit.place(x=1100, y=500)
        #
        # applique la commande closeFenetre a la croix windows
        self.FHameg.protocol('WM_DELETE_WINDOW', self.closeFenetre)
        #
        # ------------------LISTE------------------#
        self.PortChoisi = ttk.Combobox(
            self.FHameg, values=self.MajListePorts(), cursor='hand2')
        self.PortChoisi.place(x=150, y=30)
        self.PortChoisi.bind('<<ComboboxSelected>>', self.listePort_change)
        #
        # ------------------MENU------------------#
        self.mon_menu = Menu(self.FHameg)  # création du menu
        #
        # Sous onglet Fichier
        # création du sous onglet fichier
        self.fichier = Menu(self.mon_menu, tearoff=0)
        # ajout du sous onglet Enregistrer configuration
        self.fichier.add_command(
            label="Enregistrer configuration", command=self.enregistrerConfig)
        # ajout du sous onglet Ouvrir configuration
        self.fichier.add_command(
            label="Ouvrir configuration", command=self.recupererConfig)
        self.fichier.add_command(
            label="Enregistrer la session", command=self.EnregistrerSession)
        #
        # Le principal onglet
        # ajout d'un onglet Fichier a mon_menu + lié au sous onglet fichier
        self.mon_menu.add_cascade(label="Fichier", menu=self.fichier)
        #
        self.FHameg.config(menu=self.mon_menu)  # affiche le menu
        #
        # ------------------FIN FENETRE------------------#
        # lance la fonction actualiser_Hameg au bout d'une seconde
        self.FHameg.after(1000, self.actualiser_Hameg)
        self.FHameg.mainloop()
    #
    # ------------------FONCTIONS------------------#
    #
    def envoyer(self):
        for i in range(4):
            self.Ohameg.ModifierValeurs(i+1, self.Entre1[i].get(), self.Entre2[i].get())

    def EnregistrerSession(self):
        file_name = f"session_hameg4040_{datetime.now().strftime('%Y-%m-%d')}"
        cheminEnregistrement = filedialog.asksaveasfilename(defaultextension='.xlsx', filetypes=[
                                                            ("Excel files", "*.xlsx")], initialfile=file_name)  # fabrique chemin
        # lire fichier.csv
        df_val = pd.read_csv(self.FichierCSV, delimiter=";")
        # transforme fichier.csv en fichier.xlsx
        df_val.to_excel(cheminEnregistrement, index=False)
    #
    #

    def ouvrir_fenetre(self, name):
        if name == 'ChoixAlim':
            self.FHameg.destroy()
            from ChoixAlim import FChoixAlim
            FChoixAlim()
    #
    #

    def ActivationChannel(self, btn_id):
        #on active le channel en question
        if self.btns[btn_id].cget("bg")=="red":
            self.Ohameg.ActiverUnChannel(btn_id+1)
        else:
            self.Ohameg.DesactiverUnChannel(btn_id+1)
        #
        if self.bAllumer.cget("bg") == "green":  # si l'alim est éteinte
            #on modifie le bouton
            self.btns[btn_id].config(text="OFF" if self.btns[btn_id].cget("text") == "ON" else "ON")
            self.btns[btn_id].config(bg="red" if self.btns[btn_id].cget("text") == "OFF" else "blue")
            self.btns[btn_id].config(activebackground="red" if self.btns[btn_id].cget("text") == "OFF" else "blue")
            self.btns[btn_id].config(activeforeground="white" if self.btns[btn_id].cget("text") == "OFF" else "white")
        else:  # si l'alim est allumer 
            #on modifie le bouton
            if self.btns[btn_id].cget("bg") == "red":
                self.btns[btn_id].config(bg="green")
                self.btns[btn_id].config(text="ON")
            else:
                self.btns[btn_id].config(bg="red")
                self.btns[btn_id].config(text="OFF")
            #on active les channels
            if self.Ohameg != NONE:  # vérifie la conexion
                if self.btns[btn_id].cget("bg") == ("red"):
                    self.Ohameg.DesactiverChannel(btn_id+1)
                else:
                    self.Ohameg.ActiverChannel(btn_id+1)

    #
    #
    def allumerf(self):  # fonction du bouton allumer
        #
        self.bAllumer.config(text="Éteindre" if self.bAllumer.cget("text") == "Allumer" else "Allumer")  # changer text bouton allumer
        self.bAllumer.config(bg="red" if self.bAllumer.cget("bg") == "green" else "green")  # changer couleur allumer
        #
        for element in self.btns:
            if self.bAllumer.cget("bg") == "red":  # si l'alim est allumer
                #
                element.config(bg="green" if element.cget("bg") == "blue" else "red") # si le chanal est activer il passe vert
            else:  # si l'alim est éteinte
                if element.cget("bg") == "green": # si le chanal est activer il passe bleu
                    element.config(bg="blue")
                else:
                    element.config(bg="red")
        #
        if self.bAllumer.cget("bg") == "red":  # si l'alim est allumer
            #on active les channels
            if self.Ohameg != NONE:  # vérifie la conexion
                if self.btns[0].cget("bg") == ("red"): #si le channel n'est pas active on le désactive
                    self.Ohameg.DesactiverUnChannel(1)
                else: #sinon on l'active
                    self.Ohameg.ActiverUnChannel(1)
                    #
                if self.btns[1].cget("bg") == ("red"):
                    self.Ohameg.DesactiverUnChannel(2)
                else:
                    self.Ohameg.ActiverUnChannel(2)
                #
                if self.btns[2].cget("bg") == ("red"):
                        self.Ohameg.DesactiverUnChannel(3)
                else:
                    self.Ohameg.ActiverUnChannel(3)
                #
                if self.btns[3].cget("bg") == ("red"):
                    self.Ohameg.DesactiverUnChannel(4)
                else:
                    self.Ohameg.ActiverUnChannel(4)
                #

        #
        if self.bAllumer.cget("bg") == "red": # si l'alim est allume on écrit dans le csv
            # démarre l'enregistrement des données dans le csv
            with open(self.FichierCSV, mode='w', newline='') as file:  # ouvre le csv en mode ecriture
                ecrire = csv.writer(file, delimiter=';') # création de lobjet d'ecriture et remplace les "," par " "
                ecrire.writerow(['Date', 'Ch1_V', 'Ch1_A', 'Ch2_V','Ch2_A', 'Ch3_V', 'Ch3_A', 'Ch4_V', 'Ch4_A'])
            self.enregistrerCSV()
            #
        if self.Ohameg != NONE:  # vérifie la conexion
            if self.bAllumer.cget("text") == "Éteindre": #si l'alim est allumer on active la sortie general
                self.Ohameg.ActiverCourant()
            else: #si l'alim est éteinte on désactive la sortie general
                self.Ohameg.DesactiverCourant()
    #
    #
    def reset(self):  # vide les entry
        for element in self.Entre1:
            # On vide la text box et on insère la valeur 0
            element.delete(0, END)
            element.insert(0, 0)
        for element in self.Entre2:
            # On vide la text box et on insère la valeur 0
            element.delete(0, END)
            element.insert(0, 0)
        self.PeriodeEnrg.delete(0, END)
    #
    #

    # emepche d'entrer autre chose que des nombre dans le entry
    def valider_nombre(self, text):
        if len(text) == 0 or text.isdigit():
            return True
        elif text.count('.') <= 1 and (text.replace('.', '', 1).isdigit()):
            return True
        else:
            return False
    #
    #

    def enregistrerConfig(self):  # enregistre la config dans le json
        data = {
            "Config": {
                "FEnrg": {"Nb": self.PeriodeEnrg.get()},
                "CH1": {"V1": self.Entre1[0].get(), "A1": self.Entre2[0].get()},
                "CH2": {"V2": self.Entre1[1].get(), "A2": self.Entre2[1].get()},
                "CH3": {"V3": self.Entre1[2].get(), "A3": self.Entre2[2].get()},
                "CH4": {"V4": self.Entre1[3].get(), "A4": self.Entre2[3].get()}
            }
        }
        with open("Etudiant 6/tkinter/json/Hameg4040.json", "w") as x:
            json.dump(data, x)
    #
    #
    def recupererConfig(self):  # recupere l'encienne config
        self.Entre1[0].config(text="test")
        with open('Etudiant 6/tkinter/json/Hameg4040.json') as mon_fichier:
            data = json.load(mon_fichier)
            for i in range(4):
                self.Entre1[0].delete(0, END)
                self.Entre1[0].insert(0, data['Config']['CH1']['V1'])
                self.Entre1[1].delete(0, END)
                self.Entre1[1].insert(0, data['Config']['CH2']['V2'])
                self.Entre1[2].delete(0, END)
                self.Entre1[2].insert(0, data['Config']['CH3']['V3'])
                self.Entre1[3].delete(0, END)
                self.Entre1[3].insert(0, data['Config']['CH4']['V4'])
                self.Entre2[0].delete(0, END)
                self.Entre2[0].insert(0, data['Config']['CH1']['A1'])
                self.Entre2[1].delete(0, END)
                self.Entre2[1].insert(0, data['Config']['CH2']['A2'])
                self.Entre2[2].delete(0, END)
                self.Entre2[2].insert(0, data['Config']['CH3']['A3'])
                self.Entre2[3].delete(0, END)
                self.Entre2[3].insert(0, data['Config']['CH4']['A4'])
                self.PeriodeEnrg.delete(0, END)
                self.PeriodeEnrg.insert(0, data['Config']['FEnrg']['Nb'])
    #
    #
    def actualiser_Hameg(self):  # actualise
        #
        if self.Ohameg != NONE:  # vérifie la conexion
            for n in range(2):
                if self.btns[n].cget("bg") == "green": #si les channel 1 et 2 sont activer
                    self.ListeVA1[n].config(text=("V :"+self.Ohameg.LireTensionReelle(n+1)+"\nA :"+self.Ohameg.LireIntensiteReelle(n+1))) #on recupere les valeurs et les affiches
                else: #sinon on met les valeur a 0
                    self.ListeVA1[n].config(text=("V :0\nA :0"))
                #
                if self.btns[n+2].cget("bg") == "green": #si les channel 3 et 4 sont activer
                    self.ListeVA2[n].config(text=("V :"+self.Ohameg.LireTensionReelle(n+3)+"\nA :"+self.Ohameg.LireIntensiteReelle(n+3))) #on recupere les valeurs et les affiches
                else: #sinon on met les valeur a 0
                    self.ListeVA2[n].config(text=("V :0\nA :0"))
        #
        # actualise les étapes
        if self.PortChoisi.get() != "":
            self.Etape.config(text="Étape 2 : Entrer les valeurs.")
            # Vérifie que les champs sont remplie
            if any(self.Entre1[i].get() not in {"", "0", "0."} and self.Entre2[i].get() not in {"", "0", "0."} for i in range(4)):
                self.Etape.config(
                    text="Étape 3 : Activer les Ch(s) désiré(s).")
                # vérifie si les bouton son activer
                if self.btns[0].cget("bg") != "red" or self.btns[1].cget("bg") != "red" or self.btns[2].cget("bg") != "red" or self.btns[3].cget("bg") != "red":
                    self.Etape.config(
                        text="Étape 4 : Entré le fréquence d'enregistrement")
                    # vérifie qu'une fréquence est renseigner
                    if self.PeriodeEnrg.get() != "" and self.PeriodeEnrg.get() != "0" and self.PeriodeEnrg.get() != "0.":
                        self.Etape.config(text="Étape 5 : Appuyer sur allumer")
                        if self.bAllumer.cget("bg") == "red":
                            self.Etape.config(
                                text="Vous pouvez enregistrer la session.")
                    # retour au étapes précedente
                    else:
                        self.Etape.config(
                            text="Étape 4 : Entré le fréquence d'enregistrement")
                else:
                    self.Etape.config(
                        text="Étape 3 : Activer les Ch(s) désiré(s).")
            else:
                self.Etape.config(text="Étape 2 : Entrer les valeurs.")
        else:
            self.Etape.config(text="Étape 1 : Sélectionnez un port.")
        #
        # actualise les ports
        self.PortChoisi.config(values=self.MajListePorts())
        #
        self.FHameg.after(1000, self.actualiser_Hameg)
    #
    #

    def listePort_change(self, event):  # fonction appele au moment du choix du port
        self.Ohameg = NONE #on le repasse a none pour en créer un nouveau
        print(self.PortChoisi.get())
        # création objet pour les classes
        self.Ohameg = HMP4040()
        if self.system == "Linux":      
            self.Ohameg.cPort = self.PortChoisi.get()
        else:
            self.Ohameg.cPort = self.PortChoisi.get()
        self.TestPort()
    #
    #
    def TestPort(self):

        try:
            PortOK = self.Ohameg.TestConnexion()
        except Exception as e:
            PortOK = False

        if not PortOK:
            self.Ohameg = NONE #comme le port n'est pas bon il repasse en NONE
            #
            self.PeriodeEnrg.config(state=DISABLED)
            self.FEchantillon.config(state=DISABLED)
            self.sec.config(state=DISABLED)
            self.Breset.config(state=DISABLED)
            self.bEnvoyer.config(state=DISABLED) 
            self.bAllumer.config(state=DISABLED)
            for i in range(2):
                self.ListeVA1[i].config(state=DISABLED)
                self.ListeVA2[i].config(state=DISABLED)
                self.ListeCh12[i].config(state=DISABLED)
                self.ListeCh34[i].config(state=DISABLED)
            for i in range(4):
                self.btns[i].config(state=DISABLED)
                self.Entre1[i].config(state=DISABLED)
                self.Entre2[i].config(state=DISABLED)
                self.ListeNB[i].config(state=DISABLED)
                self.ListeVA[i].config(state=DISABLED)
            #
            top = Toplevel(self.FHameg)
            top.geometry("450x70")
            top.title("Erreur")
            Label(top, text="Le port choisi ne corespond pas \na une Hameg 4040", font=('Verdana')).pack()
        else:
            self.PeriodeEnrg.config(state=NORMAL)
            self.FEchantillon.config(state=NORMAL)
            self.sec.config(state=NORMAL)
            self.Breset.config(state=NORMAL)
            self.bEnvoyer.config(state=NORMAL) 
            self.bAllumer.config(state=NORMAL)
            for i in range(2):
                self.ListeVA1[i].config(state=NORMAL)
                self.ListeVA2[i].config(state=NORMAL)
                self.ListeCh12[i].config(state=NORMAL)
                self.ListeCh34[i].config(state=NORMAL)
            for i in range(4):
                self.btns[i].config(state=NORMAL)
                self.Entre1[i].config(state=NORMAL)
                self.Entre2[i].config(state=NORMAL)
                self.ListeNB[i].config(state=NORMAL)
                self.ListeVA[i].config(state=NORMAL)


            
        return PortOK
    #
    #
    def enregistrerCSV(self):  # enregistrer dans le csv
        #
        # si l'alim est toujours allume la fonction enregistrerCSV se relance
        if self.bAllumer.cget("bg") == "red":
            # vérife que la fréquence est bien renseigner
            if self.PeriodeEnrg.get() != "0" and self.PeriodeEnrg.get() != "":
                with open(self.FichierCSV, mode='a', newline='') as file:
                    ecrire = csv.writer(file, delimiter=';')
                    date = datetime.now()
                    date = date.strftime("%d/%m/%Y %Hh%Mm%Ss")
                    ecrire.writerow([date
                                     , self.Ohameg.LireTensionReelle(1)
                                     , self.Ohameg.LireIntensiteReelle(1)
                                     , self.Ohameg.LireTensionReelle(2)
                                     , self.Ohameg.LireIntensiteReelle(2)
                                     , self.Ohameg.LireTensionReelle(3)
                                     , self.Ohameg.LireIntensiteReelle(3)
                                     , self.Ohameg.LireTensionReelle(4)
                                     , self.Ohameg.LireIntensiteReelle(4)])  # Écrire les données dans le fichier CSV
                    #
                # recupere la fréquence qui est en seconde et la passe en ms
                F = float(self.PeriodeEnrg.get())*1000
                # relance la fonction enregistrerCSV a la fréquence demander
                self.FHameg.after(int(F), self.enregistrerCSV)
            else:
                # relance la fonction enregistrerCSV
                self.FHameg.after(1000, self.enregistrerCSV)
    #
    #

    def closeFenetre(self):  # arrete le programme
        if self.Ohameg != NONE:
            self.Ohameg.ToutDesactiverChannel()
            self.Ohameg.DesactiverCourant()
        exit()
    #
    #

    def MajListePorts(self):
        self.liste = list(port_list.comports())
        self.listePorts = []
        for p in self.liste:
            self.listePorts.append(p.name)
        return self.listePorts
    #
   #
  #
 #
#
FHameg4040()
