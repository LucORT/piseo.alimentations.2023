import sys
import os
FILE_DIR = os.path.abspath(__file__) #on recupere le dossier dans lequel le fichier est lancé
SCRIPT_DIR = os.path.dirname(FILE_DIR) #on recupere le dossier racine du fichier
SCRIPT_DIR = os.path.dirname(SCRIPT_DIR) #on remonte d'un dossier
SCRIPT_DIR = os.path.dirname(SCRIPT_DIR) #on remonte d'un dossier
sys.path.append(SCRIPT_DIR) #on ajoute le dossier alim-p a la liste d'ou l'on trouve les modules
from tkinter import *  # imporatation de tkinter
from tkinter import ttk  # imporatation des combobox
import tkinter as tk  # imporatation des radiobutton
import json  # imporatation des json
import csv  # imporatation des csv
from datetime import datetime  # imporatation de la date
from tkinter import filedialog  # boite de dialogue
import pandas as pd  # gestion des tableau
import time #timer
from Etudiant3.Python.CKeithley2602A import CKeithley2602A
import platform #permet de connaitre la platforme
#


class FKeithley2602A:
    #

    def __init__(self):
        self.system = platform.system()  #variable qui permet de vérifier le system
        self.o = CKeithley2602A()
        #self.o.IpAddress = "127.0.0.1"
        self.o.IpAddress = "192.168.0.155"

        #
        if self.system == "Windows": 
            chemin = 'Etudiant 6/tkinter'
            self.o.CheminFichier = "Etudiant3\\Python\\Pulse 25ms_350mA_Trig_spectro APO.tsp"
        else:
            FILE_DIR = os.path.abspath(__file__) #on recupere le dossier dans lequel le fichier est lancé
            SCRIPT_DIR = os.path.dirname(FILE_DIR) #on recupere le dossier racine du fichier
            chemin = SCRIPT_DIR
            SCRIPT_DIR = os.path.dirname(SCRIPT_DIR)
            SCRIPT_DIR = os.path.dirname(SCRIPT_DIR)
            cheminCKeithley= SCRIPT_DIR
            self.o.CheminFichier = cheminCKeithley + "/Etudiant3/Python/Pulse 25ms_350mA_Trig_spectro APO.tsp"
            
        #
        self.FichierCSV = chemin+'/fichier.csv'

        
        #
        # ------------------Fenetre------------------#
        self.FKeithleyA = Tk()
        self.FKeithleyA.geometry('1270x700')
        self.FKeithleyA.title('Keithley 2602A')
        self.FKeithleyA['bg'] = 'white'
        self.FKeithleyA.resizable(height=True, width=True)
        if self.system == "Windows": 
            self.FKeithleyA.iconbitmap("Etudiant 6/tkinter/img/eclair.ico")
        #
        # ------------------BOITE------------------#
        boiteEtape = LabelFrame(self.FKeithleyA, bg='white', bd='2', height=50, width=800)
        boiteEtape.place(x=450, y=15)
        #
        boiteContinu = LabelFrame(self.FKeithleyA, bg='white', bd='2',height=200, width=400, text="Continu", font=("Verdana", 25))
        boiteContinu.place(x=850, y=70)
        #
        boiteValeurs = LabelFrame(self.FKeithleyA, bg='white', bd='2', height=210,width=400, text="Valeurs appliquées", font=("Verdana", 25))
        boiteValeurs.place(x=850, y=270)
        #
        boitePulse = LabelFrame(self.FKeithleyA, bg='white', bd='2',height=410, width=390, text="Pulsé", font=("Verdana", 25))
        boitePulse.place(x=450, y=70)
        #
        boiteChoixMode = LabelFrame(self.FKeithleyA, bg='white', bd='2',
                                    height=250, width=390, text="Choisir le mode", font=("Verdana", 25))
        boiteChoixMode.place(x=20, y=70)
        #
        # ------------------LABEL------------------#
        #
        self.VAmax = Label(boiteContinu, text="Vmax :\nAmax :", font=(
            "Verdana", 25), bg="white", state=DISABLED)
        self.VAmax.place(x=25, y=-8)
        #
        self.VA = Label(boiteContinu, text="V :\nA :", font=(
            "Verdana", 25), bg="white", state=DISABLED)
        self.VA.place(x=25, y=70)
        #
        self.NB = Label(boiteContinu, text=(": 1"), font=(
            "Verdana", 25), bg="white", state=DISABLED)
        self.NB.place(x=340, y=87)
        #
        # Port = Label(self.FKeithleyA, text="Port :",
        #              font=("Verdana", 25), bg="white")
        # Port.place(x=20, y=15)
        #
        self.Etape = Label(boiteEtape, text="Étape 1 : Sélectionnez un port.", font=(
            "Verdana", 25), bg="white")
        self.Etape.place(x=0, y=0)
        #
        self.TempsON = Label(boitePulse, text="Durée du ON :", font=(
            "Verdana", 20), bg="white", state=DISABLED)
        self.TempsON.place(x=0, y=0)
        #
        self.TempsOFF = Label(boitePulse, text="Durée du OFF :", font=(
            "Verdana", 20), bg="white", state=DISABLED)
        self.TempsOFF.place(x=0, y=60)
        #
        self.NbPulse = Label(boitePulse, text="Nb de pulse :", font=(
            "Verdana", 20), bg="white", state=DISABLED)
        self.NbPulse.place(x=0, y=120)
        #
        self.ValeursMax = Label(boitePulse, text="Valeurs max:\nV :\nA :", font=(
            "Verdana", 20), bg="white", state=DISABLED)
        self.ValeursMax.place(x=0, y=180)
        #
        self.Valeurs = Label(boitePulse, text="Valeur souhaitée:\nA :", font=(
            "Verdana", 20), bg="white", state=DISABLED)
        self.Valeurs.place(x=0, y=280)
        #
        self.FEchantillon = Label(self.FKeithleyA, text="Période d'échantillonnage :", font=(
            "Verdana", 20), bg="white", state=DISABLED)
        self.FEchantillon.place(x=10, y=320)
        #
        self.sec = Label(self.FKeithleyA, text="s.",
                         font=("Verdana", 20), bg="white", state=DISABLED)
        self.sec.place(x=280, y=350)
        #
        Ch1 = Label(boiteValeurs, text=("Ch1 :"),
                    font=("Verdana", 25), bg="white")
        Ch1.place(x=10, y=45)
        #
        self.VAReel = Label(boiteValeurs, text=(
            "V :0\nA :0"), font=("Verdana", 25), bg="white")
        self.VAReel.place(x=120, y=25)
        #
        # ------------------ENTREE USER------------------#
        variableVmax = IntVar()
        self.entreeVmax = Entry(boiteContinu, textvariable=variableVmax, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.entreeVmax.place(x=160, y=7)
        #
        variableAmax = IntVar()
        self.entreeAmax = Entry(boiteContinu, textvariable=variableAmax, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.entreeAmax.place(x=160, y=45)
        #
        variableV = IntVar()
        self.entreeV = Entry(boiteContinu, textvariable=variableV, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.entreeV.place(x=160, y=87)
        #
        variableA = IntVar()
        self.entreeA = Entry(boiteContinu, textvariable=variableA, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.entreeA.place(x=160, y=125)
        #
        VariableDuréeON = IntVar()
        self.DuréeOn = Entry(boitePulse, textvariable=VariableDuréeON, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.DuréeOn.place(x=250, y=10)
        #
        VariableDuréeOFF = IntVar()
        self.DuréeOff = Entry(boitePulse, textvariable=VariableDuréeOFF, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.DuréeOff.place(x=250, y=70)
        #
        VariableNbPulse = IntVar()
        self.NombrePulse = Entry(boitePulse, textvariable=VariableNbPulse, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.NombrePulse.place(x=250, y=130)
        #
        VariableVPulseMax = IntVar()
        self.VPulseMax = Entry(boitePulse, textvariable=VariableVPulseMax, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.VPulseMax.place(x=250, y=215)
        #
        VariableAPulseMax = IntVar()
        self.APulseMax = Entry(boitePulse, textvariable=VariableAPulseMax, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.APulseMax.place(x=250, y=255)
        #
        VariableAPulse = IntVar()
        self.Apulse = Entry(boitePulse, textvariable=VariableAPulse, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        self.Apulse.place(x=145, y=320)
        #
        self.variableT = IntVar()
        self.FrequenceEnrg = Entry(self.FKeithleyA, textvariable=self.variableT, bd='5', validate="key", validatecommand=(self.FKeithleyA.register(self.valider_nombre), '%P'), state=DISABLED)
        # valeur par défaut du entry de la fréquence a 1
        self.FrequenceEnrg.insert(1, 1)
        self.FrequenceEnrg.place(x=150, y=360)
        #
        # ------------------BOUTON------------------#
        self.btnON = Button(boiteContinu, text="ON", bg="green", fg="white",cursor='hand2', command=lambda: self.allumer("ON"), state=DISABLED)
        self.btnON.place(x=300, y=103)
        #
        self.btnDemarrer = Button(boitePulse, text="Démarrer", bg="green", fg="white", cursor='hand2', font=("Verdana", 10), command=lambda: self.allumer("Pulse"), state=DISABLED)
        self.btnDemarrer.place(x=300, y=315)
        #
        retour = Button(self.FKeithleyA, text="<- Retour", bg="gray", fg="white", cursor='hand2',
                        font=("Verdana", 25), command=lambda: self.ouvrir_fenetre('ChoixAlim'))
        retour.place(x=450, y=500)
        #
        self.bEnvoyer = Button(self.FKeithleyA, text="Envoyer\nvaleurs", bg="gray",
                               fg="white", cursor='hand2', font=("Verdana", 15), command=self.envoyer)
        self.bEnvoyer.place(x=730, y=500)
        #
        self.Breset = Button(self.FKeithleyA, text="Reset", bg="gray", fg="white", cursor='hand2', font=(
            "Verdana", 25), command=self.reset, state=DISABLED)
        self.Breset.place(x=860, y=500)
        #
        Quitter = Button(self.FKeithleyA, text="Quitter", bg="gray", fg="white",
                         cursor='hand2', font=("Verdana", 25), command=self.closeFenetre)
        Quitter.place(x=1100, y=500)
        #
        self.M = IntVar()
        tk.Radiobutton(boiteChoixMode, text="       Pulsé", cursor='hand2', font=(
            "Verdana", 25), variable=self.M, value=1, command=self.disabled).place(x=35, y=25)
        tk.Radiobutton(boiteChoixMode, text="    Continu", cursor='hand2', font=(
            "Verdana", 25), variable=self.M, value=2, command=self.disabled).place(x=35, y=110)
        #
        # applique la commande closeFenetre a la croix windows
        self.FKeithleyA.protocol('WM_DELETE_WINDOW', self.closeFenetre)
        #
        # ------------------LISTE------------------#
        # listePort = ["COM 1", "COM 2", "COM 3", "COM 4"]
        # self.listeCombo = ttk.Combobox(
        #     self.FKeithleyA, values=listePort, cursor='hand2')
        # self.listeCombo.place(x='150', y=30)
        #
        # ------------------MENU------------------#
        mon_menu = Menu(self.FKeithleyA)
        # Sous onglet Fichier
        fichier = Menu(mon_menu, tearoff=0)  # création du sous onglet fichier
        # ajout du sous onglet Enregistrer configuration
        fichier.add_command(label="Enregistrer configuration",
                            command=self.enregistrerConfig)
        # ajout du sous onglet Ouvrir configuration
        fichier.add_command(label="Ouvrir configuration",
                            command=self.recupererConfig)
        fichier.add_command(label="Enregistrer la session",
                            command=self.EnregistrerSession)
        #
        mon_menu.add_cascade(label="Fichier", menu=fichier)
        #
        self.FKeithleyA.config(menu=mon_menu)  # affiche le menu
        #
        # ------------------IMG------------------#
        pulse = PhotoImage(file=chemin+'/img/pulse.png')
        labelPulse = Label(boiteChoixMode, image=pulse, height=70, width=100)
        labelPulse.place(x=265, y=15)
        #
        continu = PhotoImage(file=chemin+'/img/continu.png')
        labelPulse = Label(boiteChoixMode, image=continu, height=70, width=100)
        labelPulse.place(x=265, y=100)
        #
        KeithleyAImg = PhotoImage(file=chemin+'/img/Keithley2602A.png')
        labelKeithleyAe = Label(self.FKeithleyA, image=KeithleyAImg)
        labelKeithleyAe.place(x=70, y=400)
        # ------------------FIN FENETRE------------------#
        # lance la fonction actualiser_KeithleyA au bout d'une seconde
        self.FKeithleyA.after(1000, self.actualiser_KeithleyA)
        self.FKeithleyA.mainloop()
        #
    # ------------------FONCTIONS------------------#
    #
    def envoyer(self):
        self.o.fileRead()
        # self.o.SetLimitI(self.APulseMax.get())
        # self.o.SetLimitV(self.VPulseMax.get())
        # self.o.SetLevelI(self.Apulse.get())
        # self.o.Setdelay(self.DuréeOn.get())
        self.o.Send()
    #
    def EnregistrerSession(self):
        file_name = f"session_keithley2602A_{datetime.now().strftime('%Y-%m-%d')}"
        cheminEnregistrement = filedialog.asksaveasfilename(defaultextension='.xlsx', filetypes=[
                                                            ("Excel files", "*.xlsx")], initialfile=file_name)  # fabrique chemin
        # lire fichier.csv
        df_val = pd.read_csv(self.FichierCSV, delimiter=";")
        # transforme fichier.csv en fichier.xlsx
        df_val.to_excel(cheminEnregistrement, index=False)
    #

    def ouvrir_fenetre(self, name):
        if name == 'ChoixAlim':
            self.FKeithleyA.destroy()
            from ChoixAlim import FChoixAlim
            FChoixAlim()
    #
    def allumer(self, btn):
        if btn == "ON":
            self.btnON.config(text="OFF" if self.btnON.cget("text") == "ON" else "ON")
            self.btnON.config(bg="red" if self.btnON.cget("text") == "OFF" else "green")
            self.btnON.config(activebackground="red" if self.btnON.cget("text") == "OFF" else "green")
            self.btnON.config(activeforeground="white" if self.btnON.cget("text") == "OFF" else "white")
            #
            if self.btnON.cget("bg") == "red": # si l'alim est allume on écrit dans le csv
                #
                if self.FrequenceEnrg.get() != 0:  # vérife que la fréquence est bien renseigner
                    # démarre l'enregistrement des données dans le csv
                    # ouvre le csv en mode ecriture
                    with open(self.FichierCSV, mode='w', newline='') as file:
                        # création de lobjet d'ecriture
                        ecrire = csv.writer(file, delimiter=';')
                        ecrire.writerow(['Date', 'Ch1_V', 'Ch1_A'])
                    self.enregistrerCSV()
        elif btn == "Pulse":
            self.btnDemarrer.config(text="Démarrer" if self.btnDemarrer.cget("text") == "Arrêter" else "Arrêter")
            self.btnDemarrer.config(bg="green" if self.btnDemarrer.cget("text") == "Démarrer" else "red")
            self.btnDemarrer.config(activebackground="green" if self.btnDemarrer.cget("text") == "Démarrer" else "red")
            #
            if self.btnDemarrer.cget("bg") == "red": # si l'alim est allume 
                #
                #Envoie pulse
                for i in range(int(self.NombrePulse.get())):
                    self.envoyer()
                    time.sleep(float(self.DuréeOff.get()))
            #
                if self.FrequenceEnrg.get() != 0:  # vérife que la fréquence est bien renseigner
                    # démarre l'enregistrement des données dans le csv
                    with open(self.FichierCSV, mode='w', newline='') as file: # ouvre le csv en mode ecriture
                        # création de lobjet d'ecriture
                        ecrire = csv.writer(file, delimiter=';')
                        ecrire.writerow(['Date', 'Periode_T', 'Duree_ON', 'Nb_Pulse', 'Ch1_V', 'Ch1_A'])
                    self.enregistrerCSV()
    #
    #
    def reset(self):  # vide les entry
        # On vide la text box et on insère la valeur 0
        self.VPulseMax.delete(0, END)
        self.VPulseMax.insert(0, 0)
        #
        self.APulseMax.delete(0, END)
        self.APulseMax.insert(0, 0)   
        #    
        self.entreeVmax.delete(0, END)
        self.entreeVmax.insert(0, 0)
        #
        self.entreeAmax.delete(0, END)
        self.entreeAmax.insert(0, 0)
        # 
        self.entreeV.delete(0, END)
        self.entreeV.insert(0, 0)
        # On vide la text box et on insère la valeur 0
        self.entreeA.delete(0, END)
        self.entreeA.insert(0, 0)
        #
        self.DuréeOff.delete(0, END)
        self.DuréeOff.insert(0, 0)
        #
        self.DuréeOn.delete(0, END)
        self.DuréeOn.insert(0, 0)
        #
        self.NombrePulse.delete(0, END)
        self.NombrePulse.insert(0, 0)
        #
        self.Apulse.delete(0, END)
        self.Apulse.insert(0, 0)
        #
        self.btnDemarrer.config(text="Démarrer")
        self.btnDemarrer.config(bg="green")
        #
        self.btnON.config(text="ON")
        self.btnON.config(bg="green")
        #
       #
      #
     #
    #

    # emepche d'entrer autre chose que des nombre dans le entry
    def valider_nombre(self, text):
        if len(text) == 0 or text.isdigit():
            return True
        elif text.count('.') <= 1 and (text.replace('.', '', 1).isdigit()):
            return True
        else:
            return False
    #
    #

    def enregistrerConfig(self):  # enregistre la config dans le json
        if self.M.get() == 1:
            data = {
                "Valeurs": {
                    "CH1": {"Doff": self.DuréeOff.get(), "Don": self.DuréeOn.get(), "NP": self.NombrePulse.get(), "AP": self.Apulse.get(),"VPmax":self.VPulseMax.get(),"APmax":self.APulseMax.get()}
                }
            }
            with open("Etudiant 6/tkinter/json/PulseKeithley2602A.json", "w") as x:
                json.dump(data, x)
        else:
            data = {
                "Valeurs": {
                    "CH1": {"V1": self.entreeV.get(), "A1": self.entreeA.get(), "PEnrg": self.FrequenceEnrg.get(), "Vmax":self.entreeVmax.get(), "Amax":self.entreeAmax.get()}
                }
            }
            with open("Etudiant 6/tkinter/json/ContinuKeithley2602A.json", "w") as x:
                json.dump(data, x)
    #
    #

    def recupererConfig(self):  # recupere l'encienne config
        if self.M.get() == 1:  # si mode est plusé
            with open('Etudiant 6/tkinter/json/PulseKeithley2602A.json') as mon_fichier:
                data = json.load(mon_fichier)
                self.DuréeOff.delete(0, END)
                self.DuréeOff.insert(0, data['Valeurs']['CH1']['Doff'])
                self.DuréeOn.delete(0, END)
                self.DuréeOn.insert(0, data['Valeurs']['CH1']['Don'])
                self.NombrePulse.delete(0, END)
                self.NombrePulse.insert(0, data['Valeurs']['CH1']['NP'])
                self.VPulseMax.delete(0, END)
                self.VPulseMax.insert(0, data['Valeurs']['CH1']['VPmax'])
                self.APulseMax.delete(0, END)
                self.APulseMax.insert(0, data['Valeurs']['CH1']['APmax'])
                self.Apulse.delete(0, END)
                self.Apulse.insert(0, data['Valeurs']['CH1']['AP'])
        else:
            with open('Etudiant 6/tkinter/json/ContinuKeithley2602A.json') as mon_fichier:
                data = json.load(mon_fichier)
                self.entreeV.delete(0, END)
                self.entreeV.insert(0, data['Valeurs']['CH1']['V1'])
                self.entreeA.delete(0, END)
                self.entreeA.insert(0, data['Valeurs']['CH1']['A1'])
                self.FrequenceEnrg.delete(0, END)
                self.FrequenceEnrg.insert(0, data['Valeurs']['CH1']['PEnrg'])
                self.entreeVmax.delete(0, END)
                self.entreeVmax.insert(0, data['Valeurs']['CH1']['Vmax'])
                self.entreeAmax.delete(0, END)
                self.entreeAmax.insert(0, data['Valeurs']['CH1']['Amax'])

    #
    #
    def actualiser_KeithleyA(self):  # actualise
        # actualise les valeurs réelles
        if self.btnON.cget("bg") == "red":
            self.VAReel.config(
                text=("V :"+self.entreeV.get()+"\nA :"+self.entreeA.get()))
        elif self.btnDemarrer.cget("bg") == "red":
            self.VAReel.config(text=("V :"+self.o.GetLimitV()+"\nA :"+self.o.GetLevelI()))
        else:
            self.VAReel.config(text=("V :0\nA :0"))
        # actualise les étapes
        if self.listeCombo.get() != "":
            self.Etape.config(text="Étape 2 : Sélectionner le mode.")
            if self.M.get() == 1 or self.M.get() == 2:  # si mode choisi
                self.Etape.config(text="Étape 3 : Saisir les valeurs.")
                # Vérifie que les champs sont remplie
                if self.DuréeOff.get() != "" and self.DuréeOn.get() != "" and self.NombrePulse.get() != "" and self.Apulse.get() != "" and self.DuréeOff.get() != "0" and self.DuréeOff.get() != "0." and self.DuréeOn.get() != "0" and self.DuréeOn.get() != "0." and self.NombrePulse.get() != "0" and self.NombrePulse.get() != "0." and self.Apulse.get() != "0" and self.Apulse.get() != "0." or self.entreeV.get() != "" and self.entreeA.get() != "" and self.entreeV.get() != "0" and self.entreeV.get() != "0." and self.entreeA.get() != "0" and self.entreeA.get() != "0.":
                    if self.M.get() == 2:
                        self.Etape.config(text="Étape 4 : Saisir la période d'échantillonage")
                        if self.FrequenceEnrg.get() != "" and self.FrequenceEnrg.get() != "0" and self.FrequenceEnrg.get() != "0.":  # si la frequence est renseigner
                            self.Etape.config(text="Étape 5 : Activez la sortie.")
                            if self.btnDemarrer.cget("bg") == "red" or self.btnON.cget("bg") == "red":
                                self.Etape.config(text="Vous pouvez enregistrez la session.")
                    else: 
                        self.Etape.config(text="Étape 4 : Activez la sortie.")
                        if self.btnDemarrer.cget("bg") == "red" or self.btnON.cget("bg") == "red":
                            self.Etape.config(text="Vous pouvez enregistrez la session.")
        #
        else:
            self.Etape.config(text="Étape 1 : Sélectionner le port.")
        #
        # relance la fonction d'actualisation pour que sa actualise a l'infini
        self.FKeithleyA.after(1000, self.actualiser_KeithleyA)
        #

    def disabled(self):  # fonction pour grisé
        #
        self.reset()
        #
        self.FrequenceEnrg.config(state=NORMAL)
        self.FEchantillon.config(state=NORMAL)
        self.Breset.config(state=NORMAL)
        self.sec.config(state=NORMAL)
        #
        if self.M.get() == 1:
            self.VAmax.config(state=DISABLED)
            self.entreeVmax.config(state=DISABLED)
            self.entreeAmax.config(state=DISABLED)
            self.entreeV.config(state=DISABLED)
            self.entreeA.config(state=DISABLED)
            self.btnON.config(state=DISABLED)
            self.VA.config(state=DISABLED)
            self.NB.config(state=DISABLED)
            self.FEchantillon.config(state=DISABLED)
            self.FrequenceEnrg.config(state=DISABLED)
            self.sec.config(state=DISABLED)
            self.DuréeOff.config(state=NORMAL)
            self.DuréeOn.config(state=NORMAL)
            self.NombrePulse.config(state=NORMAL)
            self.Apulse.config(state=NORMAL)
            self.TempsOFF.config(state=NORMAL)
            self.TempsON.config(state=NORMAL)
            self.NbPulse.config(state=NORMAL)
            self.Valeurs.config(state=NORMAL)
            self.btnDemarrer.config(state=NORMAL)
            self.ValeursMax.config(state=NORMAL)
            self.VPulseMax.config(state=NORMAL)
            self.APulseMax.config(state=NORMAL)
        #
        elif self.M.get() == 2:
            self.VAmax.config(state=NORMAL)
            self.entreeVmax.config(state=NORMAL)
            self.entreeAmax.config(state=NORMAL)
            self.entreeV.config(state=NORMAL)
            self.entreeA.config(state=NORMAL)
            self.btnON.config(state=NORMAL)
            self.VA.config(state=NORMAL)
            self.NB.config(state=NORMAL)
            self.DuréeOff.config(state=DISABLED)
            self.DuréeOn.config(state=DISABLED)
            self.NombrePulse.config(state=DISABLED)
            self.Apulse.config(state=DISABLED)
            self.TempsOFF.config(state=DISABLED)
            self.TempsON.config(state=DISABLED)
            self.NbPulse.config(state=DISABLED)
            self.Valeurs.config(state=DISABLED)
            self.btnDemarrer.config(state=DISABLED)
            self.ValeursMax.config(state=DISABLED)
            self.VPulseMax.config(state=DISABLED)
            self.APulseMax.config(state=DISABLED)
    #

    def enregistrerCSV(self):  # enregistrer dans le csv
        #
        # si l'alim est toujours allume la fonction enregistrerCSV se relance
        if self.btnON.cget("bg") == "red":
            # vérife que la fréquence est bien renseigner
            if self.FrequenceEnrg.get() != "0" and self.FrequenceEnrg.get() != "":
                with open(self.FichierCSV, mode='a', newline='') as file:  # ouvre le csv
                    # création de l'objet pour ecrire
                    ecrire = csv.writer(file, delimiter=';')
                    date = datetime.now()  # récuperation de la date
                    # configuration de la date
                    date = date.strftime("%d/%m/%Y %Hh%Mm%Ss")
                    # Écrire les données dans le fichier CSV
                    ecrire.writerow(
                        [date, self.entreeA.get(), self.entreeV.get()])
                #
                # recupere la fréquence qui est en seconde et la passe en ms
                F = float(self.FrequenceEnrg.get())*1000
                # relance la fonction enregistrerCSV a la fréquence demander
                self.FKeithleyA.after(int(F), self.enregistrerCSV)
            else:
                # relance la fonction enregistrerCSV
                self.FKeithleyA.after(1000, self.enregistrerCSV)
        #
        # si l'alim est toujours allume et en mode pulsé
        elif self.btnDemarrer.cget("bg") == "red":
            # vérife que la fréquence est bien renseigner
            if self.FrequenceEnrg.get() != "0" and self.FrequenceEnrg.get() != "":
                with open(self.FichierCSV, mode='a', newline='') as file:  # ouvre le csv
                    # création de l'objet pour ecrire
                    ecrire = csv.writer(file, delimiter=';')
                    date = datetime.now()  # récuperation de la date
                    # configuration de la date
                    date = date.strftime("%d/%m/%Y %Hh%Mm%Ss")
                    ecrire.writerow([date, self.DuréeOff.get(), self.DuréeOn.get(), self.NombrePulse.get(
                    ), self.Apulse.get()])  # Écrire les données dans le fichier CSV
                #
                # recupere la fréquence qui est en seconde et la passe en ms
                F = int(self.FrequenceEnrg.get())*1000
                # relance la fonction enregistrerCSV a la fréquence demander
                self.FKeithleyA.after(F, self.enregistrerCSV)
            else:
                # relance la fonction enregistrerCSV
                self.FKeithleyA.after(1000, self.enregistrerCSV)
    #
    def closeFenetre(self):  # arrete le programme
        exit()
   #
  #
 #
#
FKeithley2602A()
