from tkinter import *  # imporatation de tkinter
from tkinter import ttk  # imporatation des combobox
import json  # imporatation des json
import csv  # imporatation des csv
import tkinter as tk  # imporatation des radiobutton
from datetime import datetime  # imporatation de la date
from tkinter import filedialog  # boite de dialogue
import pandas as pd  # gestion des tableau
import platform #permet de connaitre la platforme
import os
#


class FKeithley2200:
    #

    def __init__(self):
        self.system = platform.system()  #variable qui permet de vérifier le system
        #
        if self.system == "Windows": 
            chemin = 'Etudiant 6/tkinter'
        else:
            FILE_DIR = os.path.abspath(__file__) #on recupere le dossier dans lequel le fichier est lancé
            SCRIPT_DIR = os.path.dirname(FILE_DIR) #on recupere le dossier racine du fichier
            chemin = SCRIPT_DIR
        #
        self.FichierCSV = chemin+'/fichier.csv'
        # ------------------Fenetre------------------#
        self.FKeithley = Tk()
        self.FKeithley.geometry('1270x700')
        self.FKeithley.title('Keithley 2200')
        self.FKeithley['bg'] = 'white'
        self.FKeithley.resizable(height=True, width=True)
        if self.system == "Windows": 
            self.FKeithley.iconbitmap("Etudiant 6/tkinter/img/eclair.ico")
        #
        # ------------------BOITE------------------#
        self.boiteEtape = LabelFrame(
            self.FKeithley, bg='white', bd='2', height=50, width=800)
        self.boiteEtape.place(x=450, y=15)
        #
        self.boiteEntry = LabelFrame(self.FKeithley, bg='white', bd='2', height=200,
                                     width=400, text="Valeurs souhaitées", font=("Verdana", 25))
        self.boiteEntry.place(x=450, y=110)
        #
        self.FHameg = LabelFrame(self.FKeithley, bg='white', bd='2', height=200,
                                 width=300, text="Valeurs réelles", font=("Verdana", 25))
        self.FHameg.place(x=950, y=110)
        #
        # ------------------ENTREE USER------------------#
        variableV = IntVar()
        #
        self.entreeV = Entry(self.boiteEntry, textvariable=variableV, bd='5', validate="key", validatecommand=(
            self.FKeithley.register(self.valider_nombre), '%P'))
        self.entreeV.place(x=100, y=37)
        #
        variableA = IntVar()
        self.entreeA = Entry(self.boiteEntry, textvariable=variableA, bd='5', validate="key", validatecommand=(
            self.FKeithley.register(self.valider_nombre), '%P'))
        self.entreeA.place(x=100, y=75)
        #
        # choix de la fréquence d'enregistrement des données
        variableT = IntVar()
        self.FrequenceEnrg = Entry(self.FKeithley, textvariable=variableT, bd='5', validate="key", validatecommand=(
            self.FKeithley.register(self.valider_nombre), '%P'))
        # valeur par défaut du entry de la fréquence a 1
        self.FrequenceEnrg.insert(1, 1)
        self.FrequenceEnrg.place(x=1060, y=455)
        #
        # ------------------LABEL------------------#
        Port = Label(self.FKeithley, text="Port :",
                     font=("Verdana", 25), bg="white")
        Port.place(x='20', y=15)
        #
        self.Etape = Label(self.boiteEtape, text="Étape 1 : Sélectionnez un port.", font=(
            "Verdana", 25), bg="white")
        self.Etape.place(x=0, y=0)
        #
        Y = 35  # position y des element des la boucle pour les labels V/A et Ch
        #
        Ch1 = Label(self.FHameg, text=("Ch", 1, ":"),
                    font=("Verdana", 25), bg="white")
        Ch1.place(x=10, y=Y)
        #
        self.VA = Label(self.FHameg, text=("V :0\nA :0"),
                        font=("Verdana", 25), bg="white")
        self.VA.place(x=120, y=Y-20)
        #
        VA = Label(self.boiteEntry, text="V :\nA :",
                   font=("Verdana", 25), bg="white")
        VA.place(x=25, y=20)
        #
        NB = Label(self.boiteEntry, text=(":", 1),
                   font=("Verdana", 25), bg="white")
        NB.place(x=300, y=35)
        #
        FEchantillon = Label(self.FKeithley, text=(
            "Période d'échantillonnage : "), font=("Verdana", 25), bg="white")
        FEchantillon.place(x=520, y=440)
        #
        self.sec = Label(self.FKeithley, text="s.",
                         font=("Verdana", 20), bg="white")
        self.sec.place(x=1190, y=445)
        #
        # ------------------BOUTON------------------#
        #
        self.btnON = Button(self.boiteEntry, text="ON", bg="green",
                            fg="white", cursor='hand2', command=lambda: self.allumer())
        self.btnON.place(x=250, y=50)
        #
        self.retour = Button(self.FKeithley, text="<- Retour", bg="gray", fg="white", cursor='hand2',
                             font=("Verdana", 25), command=lambda: self.ouvrir_fenetre('ChoixAlim'))
        self.retour.place(x=450, y=500)
        #
        self.bEnvoyer = Button(self.FKeithley, text="Envoyer\nvaleurs", bg="gray",
                               fg="white", cursor='hand2', font=("Verdana", 15), command=self.envoyer)
        self.bEnvoyer.place(x=730, y=500)
        #
        Breset = Button(self.FKeithley, text="Reset", bg="gray", fg="white",
                        cursor='hand2', font=("Verdana", 25), command=self.reset)
        Breset.place(x=900, y=500)
        #
        self.Exit = Button(self.FKeithley, text="Quitter", bg="gray", fg="white",
                           cursor='hand2', font=("Verdana", 25), command=self.closeFenetre)
        self.Exit.place(x=1100, y=500)
        #
        # applique la commande closeFenetre a la croix windows
        self.FKeithley.protocol('WM_DELETE_WINDOW', self.closeFenetre)
        #
        # ------------------LISTE------------------#
        listePort = ["COM 1", "COM 2", "COM 3", "COM 4"]
        self.listeCombo = ttk.Combobox(
            self.FKeithley, values=listePort, cursor='hand2')
        self.listeCombo.place(x='150', y=30)
        #
        # ------------------MENU------------------#
        mon_menu = Menu(self.FKeithley)
        # Sous onglet Fichier
        fichier = Menu(mon_menu, tearoff=0)  # création du sous onglet fichier
        # ajout du sous onglet Enregistrer configuration
        fichier.add_command(label="Enregistrer configuration",
                            command=self.enregistrerConfig)
        # ajout du sous onglet Ouvrir configuration
        fichier.add_command(label="Ouvrir configuration",
                            command=self.recupererConfig)
        fichier.add_command(label="Enregistrer la session",
                            command=self.EnregistrerSession)
        #
        mon_menu.add_cascade(label="Fichier", menu=fichier)
        #
        self.FKeithley.config(menu=mon_menu)  # affiche le menu
        #
        # ------------------IMG------------------#
        #
        Keithley2200 = PhotoImage(file= chemin + '/img/Keithley2200.png')
        labelKeithley = Label(self.FKeithley, image=Keithley2200)
        labelKeithley.place(x=30, y=100)
        #
        # ------------------FIN FENETRE------------------#
        # lance la fonction actualiser_Keithley au bout d'une seconde
        self.FKeithley.after(1000, self.actualiser_Keithley)
        self.FKeithley.mainloop()
        #
    # ------------------FONCTIONS------------------#
    #

    def envoyer(self):
        print("TODO")
    #

    def EnregistrerSession(self):
        file_name = f"session_keithley2200_{datetime.now().strftime('%Y-%m-%d')}"
        cheminEnregistrement = filedialog.asksaveasfilename(defaultextension='.xlsx', filetypes=[
                                                            ("Excel files", "*.xlsx")], initialfile=file_name)  # fabrique chemin
        # lire fichier.csv
        df_val = pd.read_csv(self.FichierCSV, delimiter=";")
        # transforme fichier.csv en fichier.xlsx
        df_val.to_excel(cheminEnregistrement, index=False)
    #

    def ouvrir_fenetre(self, name):
        if name == 'ChoixAlim':
            self.FKeithley.destroy()
            from ChoixAlim import FChoixAlim
            FChoixAlim()
    #
    #

    def allumer(self):
        if self.btnON.cget("bg") == "green":  # si l'alim est éteinte
            self.btnON.config(text="OFF", bg="red")
        else:
            self.btnON.config(text="ON", bg="green")
        #
        if self.btnON.cget("bg") == "red":  # si l'alim est allume on écrit dans le csv
            #
            # démarre l'enregistrement des données dans le csv
            with open(self.FichierCSV, mode='w', newline='') as file:  # ouvre le csv en mode ecriture
                # création de lobjet d'ecriture
                ecrire = csv.writer(file, delimiter=';')
                ecrire.writerow(['Date', 'Ch1_V', 'Ch1_A'])
            self.enregistrerCSV()
    #
    #

    def reset(self):  # vide les entry
        # On vide la text box et on insère la valeur 0
        self.entreeV.delete(0, END)
        self.entreeV.insert(0, 0)
        # On vide la text box et on insère la valeur 0
        self.entreeA.delete(0, END)
        self.entreeA.insert(0, 0)
        #
        self.FrequenceEnrg.delete(0, END)
    #
    #

    # emepche d'entrer autre chose que des nombre dans le entry
    def valider_nombre(self, text):
        if len(text) == 0 or text.isdigit():
            return True
        elif text.count('.') <= 1 and (text.replace('.', '', 1).isdigit()):
            return True
        else:
            return False
    #
    #

    def enregistrerConfig(self):  # enregistre la config dans le json
        data = {
            "Config": {
                "FEnrg": {"Nb": self.FrequenceEnrg.get()},
                "CH1": {"V1": self.entreeV.get(), "A1": self.entreeA.get()}
            }
        }
        with open("Etudiant 6/tkinter/json/Keithley2200.json", "w") as x:
            json.dump(data, x)
    #
    #

    def recupererConfig(self):  # recupere l'encienne config
        self.entreeV.config(text="test")
        with open('Etudiant 6/tkinter/json/Keithley2200.json') as mon_fichier:
            data = json.load(mon_fichier)
            for i in range(4):
                self.entreeV.delete(0, END)
                self.entreeV.insert(0, data['Config']['CH1']['V1'])
                self.entreeA.delete(0, END)
                self.entreeA.insert(0, data['Config']['CH1']['A1'])
                self.FrequenceEnrg.delete(0, END)
                self.FrequenceEnrg.insert(0, data['Config']['FEnrg']['Nb'])
    #
    #

    def actualiser_Keithley(self):  # actualise
        #
        if self.btnON.cget("bg") == "red":
            self.VA.config(text=("V :"+self.entreeV.get() +
                           "\nA :"+self.entreeA.get()))
        else:
            self.VA.config(text=("V :0\nA :0"))
        #
        # actualise les étapes
        if self.listeCombo.get() != "":
            self.Etape.config(text="Étape 2 : Saisir les valeurs.")
            # Vérifie que les champs sont remplie
            if any(self.entreeV.get() not in {"", "0", "0."} and self.entreeA.get() not in {"", "0", "0."}):
                self.Etape.config(
                    text="Étape 3 : Saisir la période d'échantillonage")
                # vérifie si les bouton son activer
                if self.FrequenceEnrg.get() != "" and self.FrequenceEnrg.get() != "0" and self.FrequenceEnrg.get() != "0.":
                    self.Etape.config(text="Étape 4 : Activer la sortie.")
                    # vérifie qu'une fréquence est renseigner
                    if self.btnON.cget("bg") != "green":
                        self.Etape.config(
                            text="Vous pouvez enregistrer la session.")
                    # retour au étapes précedente
                    else:
                        self.Etape.config(text="Étape 4 : Activer la sortie.")
                else:
                    self.Etape.config(
                        text="Étape 3 : Entré le fréquence d'enregistrement")
            else:
                self.Etape.config(text="Étape 2 : Entrer les valeurs.")
        else:
            self.Etape.config(text="Étape 1 : Sélectionner le port.")
        #
        self.FKeithley.after(1000, self.actualiser_Keithley)
    #
    #

    def enregistrerCSV(self):  # enregistrer dans le csv
        #
        # si l'alim est toujours allume la fonction enregistrerCSV se relance
        if self.btnON.cget("bg") == "red":
            # vérife que la fréquence est bien renseigner
            if self.FrequenceEnrg.get() != "0" and self.FrequenceEnrg.get() != "" and self.FrequenceEnrg.get() != "0.":
                with open(self.FichierCSV, mode='a', newline='') as file:  # ouvre le csv
                    # création de l'objet pour ecrire
                    ecrire = csv.writer(file, delimiter=';')
                    date = datetime.now()  # récuperation de la date
                    # configuration de la date
                    date = date.strftime("%d/%m/%Y %Hh%Mm%Ss")
                    # Écrire les données dans le fichier CSV
                    ecrire.writerow(
                        [date, self.entreeA.get(), self.entreeV.get()])
                #
                # recupere la fréquence qui est en seconde et la passe en ms
                F = float(self.FrequenceEnrg.get())*1000
                # relance la fonction enregistrerCSV a la fréquence demander
                self.FKeithley.after(int(F), self.enregistrerCSV)
            else:
                # relance la fonction enregistrerCSV
                self.FKeithley.after(1000, self.enregistrerCSV)
        #
    #

    def closeFenetre(self):  # arrete le programme
        exit()


   #
  #
 #
#
FKeithley2200()
