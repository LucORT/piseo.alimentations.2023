import os
import sys
from tkinter import *  # imporatation de tkinter
import platform #permet de connaitre la platforme
sys.path.append("/home/raspberry/PROJET/piseo.alimentations.2023")
#
#


class FChoixAlim:
    def __init__(self):
        self.system = platform.system() #variable qui permet de vérifier le system
        #
        # ------------------FENETRE------------------#
        self.FChoix = Tk()
        self.FChoix.geometry('1270x700')
        self.FChoix.title("Choix de l'alimentation")
        self.FChoix['bg'] = 'white'
        if self.system == "Windows": 
            self.FChoix.iconbitmap("Etudiant 6/tkinter/img/ampoule.ico")

        #
        # ------------------BOUTON------------------#
        BKeithleyA = Button(self.FChoix, text="Keithley 2602A", bg="gray", fg="white", cursor='hand2', font=("Verdana", 16), command=lambda: self.ouvrir_fenetre('KeithleyA'))
        BKeithleyA.place(x=100, y=315)
        #
        BHameg40 = Button(self.FChoix, text="Hameg4040", bg="gray", fg="white", cursor='hand2', font=("Verdana", 16), command=lambda: self.ouvrir_fenetre('HamegHMP4040'))
        BHameg40.place(x=555, y=435)
        #
        BKeithley = Button(self.FChoix, text="Keithley 2200", bg="gray", fg="white", cursor='hand2', font=("Verdana", 16), command=lambda: self.ouvrir_fenetre('Keithley'))
        BKeithley.place(x=1000, y=550)
        #
        # applique la commande closeFenetre a la croix windows
        self.FChoix.protocol('WM_DELETE_WINDOW', self.closeFenetre)
        # ------------------LABEL------------------#
        Port = Label(self.FChoix, text="Choix de l'alimentation :",font=("Verdana", 25), bg="white")
        Port.pack()
        #
        # ------------------IMG------------------#
        #
        if self.system == "Windows": 
            chemin = 'Etudiant 6/tkinter/img/'
        else:
            FILE_DIR = os.path.abspath(__file__) #on recupere le dossier dans lequel le fichier est lancé
            SCRIPT_DIR = os.path.dirname(FILE_DIR) #on recupere le dossier racine du fichier
            chemin = SCRIPT_DIR  + '/img/'
        #
        Keithley2602A = PhotoImage(file=chemin + 'Keithley2602A.png')
        labelKeithleyA = Button(self.FChoix,image=Keithley2602A,command=lambda: self.ouvrir_fenetre('KeithleyA'))
        labelKeithleyA.place(x=50, y=100)
        #
        HamegHMP4040 = PhotoImage(file=chemin + 'HamegHMP4040.png')
        labelHameg = Button(self.FChoix,image=HamegHMP4040,command=lambda: self.ouvrir_fenetre('HamegHMP4040'))
        labelHameg.place(x=425, y=200)
        #
        Keithley2200 = PhotoImage(file=chemin + 'Keithley2200.png')
        labelKeithley = Button(self.FChoix, image=Keithley2200,command=lambda: self.ouvrir_fenetre('Keithley'))
        labelKeithley.place(x=900, y=300)
        
        # ------------------FIN FENETRE------------------#
        self.FChoix.mainloop()
        #
    # ------------------FONCTIONS------------------#

    def ouvrir_fenetre(self, name):
        if name == 'HamegHMP4040':
            self.FChoix.destroy()
            from Hameg4040 import FHameg4040
            FHameg4040()
        elif name == 'Keithley':
            self.FChoix.destroy()
            from Keithley2200 import FKeithley2200
            FKeithley2200()
        elif name == 'KeithleyA':
            self.FChoix.destroy()
            from Keithley2602A import FKeithley2602A
            FKeithley2602A()

    def closeFenetre(self):  # arrete le programme
        exit()

   #
  #
 #
#
#
#
FChoixAlim()  # lance fonction choixalim
