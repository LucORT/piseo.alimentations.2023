import sys
import os
import socket
from typing import Container
import serial
from serial import serialwin32
import serial.tools.list_ports as port_list
from serial.win32 import ONE5STOPBITS
import time
#from DLLPython.TestImport import Test
encoding = 'utf-8'
decoding = 'utf-8'
#from TestImport import Import
from classeHMP4040 import HMP4040
hameg = HMP4040("COM5")
#tableVariation = hameg.TableVariation(0.001, 100)
tableIHM = [{"channel":1,"liste":[{"intensite":80,"duree":5},{"intensite":50,"duree":3},{"intensite":30,"duree":7}]},
            {"channel":2,"liste":[{"intensite":80,"duree":5},{"intensite":50,"duree":3},{"intensite":30,"duree":7}]}
]


tablejson=[{"channel":1,"liste":[{"intensite":80,"duree":5},{"intensite":50,"duree":3},{"intensite":30,"duree":7}]},
    {"channel":2,"liste":[{"intensite":80,"duree":5},{"intensite":50,"duree":3},{"intensite":30,"duree":7}]},
    {"channel":3,"liste":[{"intensite":80,"duree":5},{"intensite":50,"duree":3},{"intensite":30,"duree":7}]},
    {"channel":4,"liste":[{"intensite":80,"duree":5},{"intensite":50,"duree":3},{"intensite":30,"duree":7}]}
]
#hameg.TestConnexion()
#HMP4040.getHamegPorts(hameg)
#hameg.ActiverSortie()
#hameg.DesactiverSortie()
#hameg.DesactiverToutesSorties()
#hameg.ModifierValeurs(2,30  &a, 0.001)
#hameg.Channels[0].Activer()
#hameg.Channels[2].Desactiver()
#hameg.ToutActiverChannel()
#hameg.ToutDesactiverChannel()
#hameg.Channels[0].LireTensionMax()
#hameg.Channels[0].LireIntensiteMax()
#hameg.Channels[0].LireTensionReelle()
#hameg.Channels[0].LireIntensiteReelle()
#hameg.Reset()
#hameg.Variation(2, 32, 0.001, 250, 0.5)
#hameg.VariationAPI(1, tableVariation)

#hameg.VariationIHM(tableIHM)


