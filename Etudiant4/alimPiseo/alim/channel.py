#import sys
#import os
#import socket
import serial
import time
#from typing import Container
#from serial import serialwin32
#import serial.tools.list_ports as port_list
#from serial.win32 import ONE5STOPBITS
#import time

encoding = 'utf-8'
decoding = 'utf-8'

# Classe Channel
class channel:
    global oHMP4040
    global iNumChannel

    
    # Constructeur de la classe
    # poHMP4040 -> Variable à rentrer par l'utilisateur (numéro du port)
    # piNumChannel -> 
    def __init__(self, poHMP4040, piNumChannel):
        self.oHMP4040 = poHMP4040
        self.iNumChannel = piNumChannel
        #print ('Construction réussie du channel numéro '+str(piNumChannel))
        
    def Activer(self):
        try:
            self.oHMP4040.defOpen()
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("OUTP:SEL ON")
            self.oHMP4040.defClose()
        except:
            print('Activation du channel visé impossible')
        #Active le channel visé 

    def Desactiver(self):
        try:
            self.oHMP4040.defOpen()
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("OUTP:SEL OFF")
            self.oHMP4040.defClose()
        except:
            print('Désactivation du channel visé impossible')
    #Desactive et retourne le channel visé

    def LireTensionMax(self):
        try:
            self.oHMP4040.defOpen()    
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("VOLT?")
            self.oHMP4040.lireLigne()
            self.oHMP4040.defClose()
        except:
            print('Lecture de la Tension max impossible')
            self.oHMP4040.defClose()
        #Modifie les valeurs de la tension max 
        # et de l'intensité max du channel visé
    #Retourne la valeur max de la tension du channel visé

    def LireIntensiteMax(self):
        print('')
        try:    
            self.oHMP4040.defOpen()
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("CURR?")
            self.oHMP4040.lireLigne()
            self.oHMP4040.defClose()
        except:
            print('Lecture de l''intensite max impossible')
            self.oHMP4040.defClose()
        #Retourne la valeur max de l'intensité du channel visé

    def LireTensionReelle(self):
        try:    
            self.oHMP4040.defOpen()
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("MEAS:VOLT?")
            return self.oHMP4040.lireLigne()
        except:
            print('Lecture de la Tension reelle impossible')
            self.oHMP4040.defClose()
        #Retourne la tension en sortie du channel visé

    def LireIntensiteReelle(self):
        print('')
        try:    
            self.oHMP4040.defOpen()
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("MEAS:CURR?")
            return self.oHMP4040.lireLigne()
        except:
            print('Lecture de l''intensité reelle impossible')
            self.oHMP4040.defClose()
        #Retourne l'intensité en sortie du channel visé

    def ModifierValeurs(self, TensionMax, IntensiteMax ):
        try:    
            self.oHMP4040.defOpen()
            self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
            self.oHMP4040.ecrire("APPLY "+str(TensionMax)+','+str(IntensiteMax))
        except:
            print('1 Modification de la tension max et de l''intensité max impossible')
            self.oHMP4040.defClose()
        #Modifie les valeurs de la tension max 
        # et de l'intensité max du channel visé

    def Variation(self, TensionMax, pasIntensite, nbboucle, tempsPas):
        try:
            self.oHMP4040.defOpen()
            for i in range (0, nbboucle):
                self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
                self.oHMP4040.ecrire("APPLY "+str(TensionMax)+','+str( i * pasIntensite))
                time.sleep(tempsPas)
            self.oHMP4040.ecrire("APPLY "+str(TensionMax)+','+str(0))     
            self.oHMP4040.defClose()
        except:
            print('Variation impossible')

    #def EtatChannel(self):
        #print('')
        #Retourne l'état du channel visé(Facultatif)


    def VariationAPI(self, table):
        try:
            self.oHMP4040.defOpen()
            for i in range (0, len(table)):
                self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel))
                self.oHMP4040.ecrire("APPLY "+str(table[i]["Tension"])+','+str(table[i]["Intensite"]))
                time.sleep(table[i]["Duree"])
            self.oHMP4040.ecrire("APPLY"+str(32)+','+str(0))
            self.oHMP4040.defClose()
        except:
            print('Variation avec tableau impossible')
        
        #Fonction permettant de faire varier l'intensite d'une lampe sur une duree 
    
    def VariationIHM(self, table):
        #print(table[0]['intensite'])
        try:
            for i in range(0, len(table)+1):
                #print(table['liste'][0]['intensite'])
                intensiteMax = (250/100)*table['liste'][i]['intensite']
                nbEtape = table['liste'][i]['duree']/0.2
                pasIntensite = intensiteMax/nbEtape
                print('Intensité max: '+str(intensiteMax)+', Nombre étape: '+str(nbEtape)+', pas intensité: '+str(pasIntensite))
                for i in range(1, int(nbEtape+1)):
                    self.oHMP4040.ecrire("INST OUT"+str(self.iNumChannel)) 
                    self.oHMP4040.ecrire("APPLY "+str(32)+', '+str((pasIntensite*i)/1000))
                    time.sleep(0.2)
                self.oHMP4040.ecrire("APPLY "+str(32)+','+str(0))
        except:
            print('erreur avec le programme de variation IHM')
    
