from django.shortcuts import render
from django.http import JsonResponse

# Keithley2200/piloter/<int:portCom>/<int:intensity>/<int:tension>/
def piloter(request, portCom, intensity, tension):  # Unité intensité et tension => mA & mV
    if (portCom == 1, intensity > 0, intensity <= 10000, tension > 0, tension <= 32000):
        status = "ok"
    else:
        status = "ko"
    data = {"status": status,
            "portCom": portCom,
            "intensity": intensity,
            "tension": tension
            }
    return JsonResponse(data)


#Keithley2200/lire/<int:portCom>/
def lire(request, portCom):
    if portCom == 1:
        status = "ok"
        intensite = 5000
        tension = 16000
    else:
        status = "ko"
        intensite = "ko"
        tension = "ko"
    data = {"status": status,
            "portCom": portCom,
            "intensity": intensite,
            "tension": tension
            }
    return JsonResponse(data)

# Keithley2200/activerSortieAlim/<int:portCom>/
def activerSortieAlim(request, portCom):
    if portCom == 1:
        status = "ok"
    else:
        status = "ko"
    data = {"status": status}

    return JsonResponse(data)

#Keithley2200/definirIntensite/<int:portCom>/<int:intensity>/
def definirIntensite(request, portCom, intensity):
    if (portCom == 1, intensity > 0, intensity <= 10000):
        status = "ok"
    else:
        status = "ko"

    data = {"status": status}

    return JsonResponse(data)


#Keithley2200/definirTension/<int:portCom>/<int:tension>/
def definirTension(request, portCom, tension):
    if (portCom == 1, tension > 0, tension <= 32000):
        status = "ok"
    else:
        status = "ko"

    data = {"status": status}

    return JsonResponse(data)


#Keithley2200/testerPortCom/<int:portCom>/<int:tension>/
def testerPortCom(request, portCom):
    if portCom == 1:
        status = "ok"
    else:
        status = "ko"
    data = {"status": status}

    return JsonResponse(data)
    # Create your views here.

#Keithley2200/activerSortieAlim/<int:portCom>/
def activerSortieAlim(request, portCom):
    if portCom == 1:
        status = "ok"
    else:
        status = "ko"

    data = {"status": status}

    return JsonResponse(data)
