# API Alimentation piseo manuel

Liste des urls de tests fonctionnels
--- 
Les urls se strucurent sous ce format lorsque l'application est lancé avec la commande "python manage.py runserver" dans le dossier de l'application à l'aide d'une CMD
http://127.0.0.1:8000/alim/"NOM DE L'ALIMENTATION"/"NOM DE LA FONCTION"/"ARGUMENTS"/


## HMP4040

    • LIMITE VALEURS HMP4040
    Channel = 1, 2, 3, 4
    Intensité = 0 -> 10 A
    Tension = 0 -> 32  V

    • PILOTER HMP4040 
    http://127.0.0.1:8000/alim/HMP4040/piloter/1/2/2/10/
    HMP4040/piloter/<int:portCom>/<str:channel>/<str:intensity>/<str:tension>

    RETOUR {"status": status}

    • LIRE VALEUR HMP4040 
    http://127.0.0.1:8000/alim/HMP4040/lire/1/2/
    HMP4040/Lire/<int:portCom>/<int:channel>/

    RETOUR {"status": status,
            "portCom": portCom,
            "channel": channel,
            "intensity": intensite,
            "tension": tension
            }

    • ACTIVER COURANT HMP4040 
    http://127.0.0.1:8000/alim/HMP4040/activerSortieAlim/1/2/
    HMP4040/activerCourant/<int:portCom>

    RETOUR {"status": status}

    • TESTER PORT COM HMP4040 
    http://127.0.0.1:8000/alim/HMP4040/testerPortCom/1/
    HMP4040/testerPortCom/<int:portCom>/

    RETOUR {"status": status}
    
## KEITHLEY2200    

    • LIMITE VALEURS KEITHLEY2200   TODO

    portCom = 1
    Intensité = 0 -> 10 000 mA
    Tension = 0 -> 32 000 mV

    
    • PILOTER KEITHLEY2200
    http://127.0.0.1:8000/alim/Keithley2200/piloter/1/5000/16000/
    Keithley2200/piloter/<int:portCom>/<int:intensity>/<int:tension>/

    RETOUR {"status": status,
            "portCom": portCom,
            "intensity": intensity,
            "tension": tension
            }



    • LIRE KEITHLEY2200
    http://127.0.0.1:8000/alim/Keithley2200/lire/1/
    Keithley2200/lire/<int:portCom>/

    RETOUR {"status": status,
            "portCom": portCom,
            "intensity": intensite,
            "tension": tension
            }



    • DEFINIR INTENSITE KEITHLEY2200
    http://127.0.0.1:8000/alim/Keithley2200/definirIntensite/1/5000/
    Keithley2200/definirIntensite/<int:portCom>/<int:intensity>/

    RETOUR {"status": status}

    

    • DEFINIR TENSION KEITHLEY2200
    http://127.0.0.1:8000/alim/Keithley2200/definirTension/1/16000/
    Keithley2200/definirTension/<int:portCom>/<int:tension>/
    
    RETOUR {"status": status}



    • TESTER PORT COM KEITHLEY2200
    http://127.0.0.1:8000/alim/Keithley2200/testerPortCom/1/
    Keithley2200/testerPortCom/<int:portCom>/

    RETOUR {"status": status}

    • ACTIVER SORTIE KEITHLEY2200
    http://127.0.0.1:8000/alim/Keithley2200/activerSortieAlim/1/
    Keithley2200/activerSortieAlim/<int:portCom>/

    RETOUR {"status": status}
    

## KEITHLEY2602A 

    • LIMITE VALEURS KEITHLEY2602A   TODO

    portCom = 1
    Intensité = 0 -> 10 000 mA
    Tension = 0 -> 32 000 mV

    • ACTIVER SORTIE KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/activerSortieAlim/test/1/
    #Keithley2602A/activerSortieAlim/<str:ip>/<int:portCom>/

    RETOUR {"status": status,
            "portCom": portCom,
            "ip": ip}

    • ALLUMER ALIM KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/allumerAlim/test/1/
    #Keithley2602A/allumerAlim/<str:ip>/<int:portCom>/

    RETOUR {"status": status,
            "portCom": portCom,
            "ip": ip}
    
    • ETEINDRE ALIM KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/eteindreAlim/test/1/
    #Keithley2602A/eteindreAlim/<str:ip>/<int:portCom>/

    RETOUR {"status": status,
            "portCom": portCom,
            "ip": ip}

    • PILOTER KEITHLEY2602A 
    http://127.0.0.1:8000/alim/Keithley2602A/piloter/test/1/5000/16000/1000/
    Keithley2602A/piloter/<str:ip>/<int:portCom>/<int:intensity>/<int:tension>/<int:frequency>/

    RETOUR {"status": status,
            "portCom": portCom,
            "ip": ip,
            "intensity": intensity,
            "tension": tension
            "frequency": frequency
            }

    • LIRE KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/lire/test/1/
    #Keithley2602A/lire/<str:ip>/<int:portCom>/

    RETOUR {"status": status,
            "ip": ip,
            "portCom": portCom,
            "intensity": intensite,
            "tension": tension
            }

    • DEFINIR INTENSITE KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/definirIntensite/test/1/5000/
    #Keithley2602A/definirIntensite/<str:ip>/<int:portCom>/<int:intensity>/

    RETOUR {"status": status,
            "intensity": intensite
            }

    • DEFINIR TENSION KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/definirTension/test/1/5000/
    #Keithley2602A/definirTension/<int:portCom>/<int:tension>/

    RETOUR {"status": status,
            "tension": tension
            }

    • DEFINIR FREQUENCE KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/definirFrequence/test/1/1000/
    #Keithley2602A/definirFrequence/<str:ip>/<int:portCom>/<int:frequency>/

    RETOUR {"status": status,
            "frequency": frequency
            }

    • TESTER PORT COM KEITHLEY2602A
    http://127.0.0.1:8000/alim/Keithley2602A/testerPortCom/test/1/
    #Keithley2602A/testerPortCom/<str:ip>/<int:portCom>/

    RETOUR {"status": "ok"}