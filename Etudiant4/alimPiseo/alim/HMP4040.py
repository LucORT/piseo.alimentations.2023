from django.shortcuts import render

from django.http import JsonResponse

from .classeHMP4040 import HMP4040
hameg = HMP4040()
#-----------------------POUR PASSAGE PORT COM ARGUMENT AUTO----------------------
#hameg.cPort = hameg.detCPORt
#if hameg.cPort == null
#   return error fin prgm

# HMP4040/piloter/<int:portCom>/<int:channel>/<int:intensity>/<int:tension>/
def piloter(request, portCom, channel, intensity, tension):  # Unité intensité et tension => mA & mV

 #  EL  cPort = "COM" + str(portCom)
    hameg.setCPort(portCom)
    status = hameg.piloter(channel, intensity, tension)
    data = {"status": status}

    return JsonResponse(data)

def listePorts(request):
    liste = hameg.getPorts()

    data = {"liste": liste}
    return JsonResponse(data)


    return JsonResponse(data=liste,safe=False)


# HMP4040/Lire/<int:portCom>/<int:channel>/
def lire(request, portCom, channel):
    #if (portCom == 1, channel > 0, channel <= 4):
    #    status = "ok"
    #    intensite = 5000
    #    tension = 16000
    #else:
    #    status = "ko"
    #    intensite = "ko"
    #    tension = "ko"
    #  EL  cPort = "COM" + str(portCom)
    cPort = "/dev/"  + str(portCom)
    hameg.setCPort(cPort)
    hameg.ecrire("MEAS:VOLT?")
    tens = hameg.lireLigne()
    hameg.ecrire("MEAS:CURR?")
    intens = hameg.lireLigne()
    tens = tens[0:len(tens)-1]              #Les variables sont retournées sous le format "tension": "4\n", la Hameg renvoie le
    intens = intens[0:len(intens) - 1]      #caractère de fin de ligne, afin de s'en débarasser on utilise str[0:len(intens) - 1]
                                            #appelé du "slicing" en python afin de s'en débarasser

    data = {"tension": tens,
            "intensity": intens}
    return JsonResponse(data)


# HMP4040/activerSortieAlim/<int:channel>/
def activerSortieAlim(request, portCom, channel):
    #if (portCom == 1, channel > 0, channel <= 4):
    #   status = "ok"
    #else:
    #   status = "ko"
        #  EL  cPort = "COM" + str(portCom)
    cPort = "/dev/" + str(portCom)
    hameg.setCPort(cPort)
    status = hameg.ActiverSortie()
    data = {"status": status}

    return JsonResponse(data)

def desactiverSortieAlim(request, portCom, channel):
    #if (portCom == 1, channel > 0, channel <= 4):
    #   status = "ok"
    #else:
    #   status = "ko"
        #  EL  cPort = "COM" + str(portCom)
    cPort = "/dev/" + str(portCom)
    hameg.setCPort(cPort)
    status = hameg.DesactiverSortie()
    data = {"status": status}

    return JsonResponse(data)


# HMP4040/definirIntensite/<int:portCom>/<int:channel>/<int:intensity>/
def definirIntensite(request, portCom, channel, intensity):
    #if (portCom == 1, channel > 0, channel <= 4, intensity > 0, intensity <= 10000):
    #    status = "ok"
    #else:
    #    status = "ko"

    status = hameg.ecrireIntensite(channel, intensity)
    data = {"status": status}

    return JsonResponse(data)

def setPCom(request, portCom):
        #  EL  cPort = "COM" + str(portCom)
    cPort = "/dev/" + str(portCom)
    status = hameg.setCPort(cPort)
    data = {"status": status}

    return JsonResponse(data)

# HMP4040/definirTension/<int:portCom>/<int:channel>/<int:tension>/
def definirTension(request, portCom, channel, tension):
    #if (portCom == 1, channel > 0, channel <= 4, tension > 0, tension <= 32000):
     #   status = "ok"
    #else:
     #   status = "ko"
         #  EL  cPort = "COM" + str(portCom)
    cPort = "/dev/" + str(portCom)
    hameg.setCPort(cPort)
    status = hameg.ecrireVolt(channel, tension)
    data = {"status": status}

    return JsonResponse(data)


# HMP4040/testerPortCom/<int:portCom>/
def testerPortCom(request, portCom):
    #    if portCom == 1:
    #    status = "ok"
    #else:
        #  EL  cPort = "COM" + str(portCom)
    cPort = "" + str(portCom)
# LG old    hameg.setCPort(cPort)
    hameg.defClose()
    hameg.cPort = cPort
# LG old    status = hameg.TestConnexion()
    if hameg.TestConnexion():
        status = "OK"
    else:
        status = "KO"
    status1 = hameg.getHamegPorts()
    data = {"status": status,
            "isHameg": status1}
    return JsonResponse(data)

def desactiverSortieChannel(request, portCom, channel):
        #  EL  cPort = "COM" + str(portCom)
    cPort = "/dev/" + str(portCom)
    hameg.setCPort(cPort)
    status = hameg.desactiverSortieChannel(channel)
    data = {"status": status}

    return JsonResponse(data)

def activerSortieChannel(request, portCom, channel):
        #  EL  cPort = "COM" + str(portCom)
    #cPort = "/dev/" + str(portCom)
    hameg.setCPort(portCom)
    status = hameg.activerSortieChannel(channel)
    data = {"status": status}

    return JsonResponse(data)

def ActiverCourant(request, portCom):
        #  EL  cPort = "COM" + str(portCom)
    #cPort = "/dev/" + str(portCom)
    hameg.setCPort(portCom)
    status = hameg.ActiverCourant()
    data = {"status": status}

    return JsonResponse(data)

def DesactiverCourant(request, portCom):

    # EL  cPort = "COM" + str(portCom)
    #cPort = "/dev/" + str(portCom)
    hameg.setCPort(portCom)
    status = hameg.DesactiverCourant()
    data = {"status": status}

    return JsonResponse(data)

# Create your views here.
