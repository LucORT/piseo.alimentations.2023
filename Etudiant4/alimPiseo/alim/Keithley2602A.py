from django.http import JsonResponse

from .classeKeithley2602A import CKeithley2602A
o = CKeithley2602A()

def piloter(request, ip, limiteI, limiteV, levelI, delay):  # Unité intensité et tension => A & V
    o.CheminFichier = "Pulse 25ms_350mA_Trig_spectro APO.tsp"
    try:
        o.fileRead()
        o.SetLimitI(limiteI)
        o.SetLimitV(limiteV)
        o.SetLevelI(levelI)
        o.Setdelay(delay)
        o.Send()

        data = {"status": "ok"}
    except:
        data = {"status": "ko"}
    return JsonResponse(data)

def lire(request, ip):  # Unité intensité et tension => mA & mV

    data = {"status": "ok"}
    return JsonResponse(data)
