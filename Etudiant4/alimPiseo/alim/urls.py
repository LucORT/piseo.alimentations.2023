from django.urls import path
from . import HMP4040, Keithley2200, Keithley2602A

urlpatterns = [

#URL HMP4040

    path('HMP4040/piloter/<str:portCom>/<int:channel>/<int:intensity>/<int:tension>/', HMP4040.piloter, name='pilot'),
    path('HMP4040/testerPortCom/<str:portCom>/', HMP4040.testerPortCom, name='testportcom'),
    path('HMP4040/lire/<str:portCom>/<int:channel>/', HMP4040.lire, name='lire'),
    path('HMP4040/definirCom/<str:portCom>/', HMP4040.setPCom, name='definirTension'),
    path('HMP4040/activerCourant/<str:portCom>/', HMP4040.ActiverCourant, name='activerCourant'),
    path('HMP4040/desactiverCourant/<str:portCom>/', HMP4040.DesactiverCourant, name='activerCourant'),
    path('HMP4040/activerChannel/<str:portCom>/<int:channel>/', HMP4040.activerSortieChannel, name='activerCourant'),
    path('HMP4040/desactiverChannel/<str:portCom>/<int:channel>/', HMP4040.desactiverSortieChannel, name='activerCourant'),
    path('HMP4040/listePort/', HMP4040.listePorts, name='listeport'),

#URL KEITHLEY2200

    path('Keithley2200/piloter/<int:portCom>/<int:intensity>/<int:tension>/', Keithley2200.piloter, name='pilot'),
    path('Keithley2200/lire/<int:portCom>/', Keithley2200.lire, name='lire'),
    path('Keithley2200/definirIntensite/<int:portCom>/<int:intensity>/', Keithley2200.definirIntensite, name='definirIntensite'),
    path('Keithley2200/definirTension/<int:portCom>/<int:tension>/', Keithley2200.definirTension, name='definirTension'),
    path('Keithley2200/testerPortCom/<int:portCom>/', Keithley2200.testerPortCom, name='testerPortCom'),
    path('Keithley2200/activerSortieAlim/<int:portCom>/', Keithley2200.activerSortieAlim, name='activerSortieAlim'),

#URL KEITHLEY2602A

    path('Keithley2602A/piloter/<str:ip>/<str:limiteI>/<str:limiteV>/<str:levelI>/<str:delay>/',
         Keithley2602A.piloter, name='pilot'),

    path('Keithley2602A/lire/<str:ip>/', Keithley2602A.lire, name='lire'),


]
