# Doc Arborescence DJango

    • Ce dossier est le fichier accueillant l'application Django, le sous fichier alimPiseo est le fichier où se trouve les paramètres généraux de l'application.

    L'autre dossier alim vient recueillir tout le reste de l'application, on pourra retrouver les fichiers python comme : 
    
    • "urls.py" : est le fichier où toutes les URLs constituant l'API sont mises.
    • "HMP4040/Keitlhey2200/Keithley2602A.py" sont les fichiers qui vont respectivement accueillir la redirection des URLs misent précedemment dans "urls.py"
    • "HMP4040.py" va utiliser les classes de commande de l'alimentation HMP4040 se trouvant dans le fichier "classeHMP4040.py"