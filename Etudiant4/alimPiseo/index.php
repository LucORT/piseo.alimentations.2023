<?php
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Test API</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../css/normalize.css">
    </head>

    <body>
        <div class="m-3">
            <div class="starter-template text-center p-3">
                <h1>
                    Fonctions Test API
                </h1>
                <h2>
                    HMP 4040
                </h2>
                <h4>
                    <p> <a href="http://127.0.0.1:8000/alim/HMP4040/piloter/1/2/2500/2500/">PILOTER HMP4040</a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/HMP4040/lire/1/2/">LIRE VALEUR HMP4040 </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/HMP4040/activerSortieAlim/1/2/">ACTIVER SORTIE HMP4040</a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/HMP4040/definirIntensite/1/2/5000/">DEFINIR INTENSITE HMP4040 </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/HMP4040/definirTension/1/2/5000/">DEFINIR TENSION HMP4040 </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/HMP4040/testerPortCom/1/">TESTER PORT COM HMP4040 </a></p>

                </h4>
                <h2>
                    KEITHLEY2200
                </h2>
                <h4>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2200/piloter/1/5000/16000/">PILOTER KEITHLEY2200</a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2200/lire/1/">LIRE KEITHLEY2200 </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2200/definirIntensite/1/5000/">DEFINIR INTENSITE KEITHLEY2200</a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2200/definirTension/1/16000/">DEFINIR TENSION KEITHLEY2200 </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2200/activerSortieAlim/1/">ACTIVER SORTIE KEITHLEY2200 </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2200/testerPortCom/1/">TESTER PORT COM KEITHLEY2200 </a></p>

                </h4>

                <h2>
                    KEITHLEY2602A
                </h2>
                <h4>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/piloter/test/1/5000/16000/1000/">PILOTER KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/lire/test/1/">LIRE KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/definirIntensite/test/1/5000/">DEFINIR INTENSITE KEITHLEY2602A</a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/definirTension/test/1/5000/">DEFINIR TENSION KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/definirFrequence/test/1/1000/">DEFINIR FREQUENCE KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/testerPortCom/test/1/">TESTER PORT COM KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/activerSortieAlim/test/1/">ACTIVER SORTIE KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/allumerAlim/test/1/">ALLUMER ALIM KEITHLEY2602A </a></p>
                    <p> <a href="http://127.0.0.1:8000/alim/Keithley2602A/eteindreAlim/test/1/">ETEINDRE ALIM KEITHLEY2602A </a></p>

                </h4>
            </div>
        </div>

    </body>
</html>
