<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="Style/AlimStyle.css" type="text/css" rel="stylesheet">
   <!--Lien du site :https://jquery.com/download/ -->
    <script type="text/javascript" src="jquery-3.6.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

</head>
<body>
<!-- <form action="testcsv.php" method="post"> -->
    <nav>
      <h1 id="h1Nav">Piseo</h1>
      <ul>
        <li><a href="Keithley2200.php">Keithley2200</a></li>
        <li><a href="Keithley260A.php">Keithley2060A</a></li>
      </ul>
    </nav>

          <div id="bdId">
          <h1>Hameg 4040</h1>

  <div id="div_Port">
     <h1 id="h1Port">Port:</h1>
      <select id="pet-select" name="SelectPort">
         <option value="port">--Port--</option>
         <option value="1" id="PortCom1">COM1</option>
         <option value="2" id="PortCom2">COM2</option>
         <option value="3" id="PortCom3">COM3</option>
         <option value="4" id="PortCom4">COM4</option>
         <!--<option value="USB">USB</option>
          <option value="Ethernet">Ethernet</option>-->
      </select>
  </div>

      <div id=btId>
        <button name="Test_connexion" onclick="myFunction()" id="demo" type="submit">Test connexion</button>
        <button name="Allumer" id="Allumer" onclick="Myfunction1()" class="off">Allumer</button> 
        <style type="text/css">.off {
         background-color:red;
        }
        .on {
         background-color:green;
        }</style> 
        <button name="Enrengistre_Val" id="Enrengistre_Val" >Modifier Valeur</button >
        <button type="submit" onclick="EnrengistrerCSV()"><a href="testcsv.csv">Enrengistre CSV</a></button>
      </div>

  <section id="div_Channel">
       
      <div id="chBoxId">
    
    <div id="chBox1"> 
      <input type="checkbox" id="Id_Channel"  name="Channel" value="1" >   
      <label for="Lb_Channel1">1</label>
    </div>
  
    <div id="chBox2">
      <input type="checkbox" id="Id_Channel" name="Channel"  value="2">
      <label for="Lb_Channel2">2</label>
    </div>
       
    <div id="chBox3">
      <input type="checkbox" id="Id_Channel" name="Channel" value="3">
      <label for="Lb_Channel3">3</label>
    </div>
       
    <div id="chBox4">
      <input type="checkbox" id="Id_Channel" name="Channel" value="4">
      <label for="Lb_Channel4">4</label>
    </div>
      
   
       </div>

    <div id="lbDiv">
        <h1>Valeur max</h1>
        
          <!-- input pour le channel 1 , Les ampére seront en A ou miliAmpére, keithley 2200 30V et 2.5A-->
           <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut1" id="inPut1" min="0" max="2"></label></br>
          <label for="name">A:<input type="number" name="inPut2" id="inPut2" min="0" max="30"></label></br></br>
          </div>
          <!-- input pour le channel 2-->
           <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut3"  id="inPut3" min="0" max="0"></label></br>
          <label for="name">A:<input type="number" name="inPut4"  id="inPut4" min="0" max="30"></label></br></br>
          </div>
          <!-- input pour le channel 3-->
           <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut5"  id="inPut5" min="0" max="0"></label></br>
          <label for="name">A:<input type="number" name="inPut6"  id="inPut6" min="0" max="30"></label></br></br>
          </div>
           <!-- input pour le channel 4-->
           <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut7"  id="inPut7" min="0" max="0"></label></br>
          <label for="name">A:<input type="number" name="inPut8"   id="inPut8" min="0" max="30"></label></br></br>
          <button type="reset">reset</button>
          </div>
      </div>
     
  </section>  

    <div id="div_ValeurReel">
       <h1>Valeur de sortie</h1>
           <div id="ch12ValeurReel">
         <h2 id="h2ValeurReel">cH1</h2>
            <h3>V:<div id="valeur"></div></h3>
            <h3>A:</h3>
         <h2 id="h2ValeurReel">cH2</h2>
            <h3>V:</h3>
            <h3>A:</h3>
           </div>

           <div id="ch34ValeurReel">
        <h2 id="h23ValeurReel">cH3</h2>
             <h3>V:</h3>
             <h3>A:</h3>
        <h2 id="h24ValeurReel">cH4</h2>
             <h3>V:</h3>
             <h3>A:</h3>
           </div>
           <p>Définir interval de temps</p><input type="number" id="IntervalTemps" placeholder="F échatillonage">
    </div>


     <div id="aide">
     <div id="TexteAide"></div>     
      
    


    <!--Script pour Afficher Valeur Réelle -->
    <script>
      function EnrengistrerCSV(){
    //var timer =document.getElementById("IntervalTemps").value;
    var filename="testcsv.php";
 
    var intervalId = setInterval(function() {
    $.ajax({                
      // Exécution d’une requête Ajax avec la configuration donnée par l'objet suivant :
            async: "true",               // - requête asynchrone
            type: "GET",                 // - type HTTP GET
            url: filename,           // - URL de la page à charger
            //data: "email=" + encodeURIComponent(email) + "&action=get_email", // - données à envoyer
            error: function(errorData) { // - fonction de rappel en cas d’erreur
                $("#error").html(errorData);
            },
            success: function(data) { 
              //alert("bing");       
            }
        });
  }, 1500);


    
    const fakeHamegCsv = 'testcsv.csv';

// Création d'un lien de téléchargement
var downloadLink = document.createElement('a');
downloadLink.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent));//La fonction encodeURIComponent encode une chaîne de caractères
downloadLink.setAttribute('download', fakeHamegCsv);
document.body.appendChild(downloadLink);
downloadLink.click();

  }
    </script>     
            </div>
            </div>
      
    <script>
           var AideTexte = document.getElementById("TexteAide");
                AideTexte.innerHTML = "Veuillez Testé séléctioner Port";
            var SelectPort=document.getElementById("pet-select");
            //Premiére instruction           
          // Quand on appuie sur tester connexion
          function myFunction(){
          //Je créé une variable ValCom cette variable prend la valeur de la list déroulante
          var ValCom =$("#pet-select").val();
            $.ajax({        
              
                  async: "true",               // - requête asynchrone
                  type: "GET",                 // - type HTTP GET
                  url: "http://127.0.0.1:8000/alim/HMP4040/testerPortCom/"+ValCom ,           // - URL de la page à charger
                  //data: "email=" + encodeURIComponent(email) + "&action=get_email", // - données à envoyer
                  error: function(errorData) { // - fonction de rappel en cas d’erreur
                      $("#error").html(errorData);
                      alert("Le port com ne marche pas ")
                  },
                  success: function(data) {  
                    document.getElementById("TexteAide").innerHTML =data.status; 
                    //"data.status" est une propriété de l'objet "data" retourné par une API
                    if(data.status=='OK'){
                    SelectPort.disabled = true; // désactiver l'input
                    SelectPort.style.opacity = "1.5";
                    
                     } 
                     AideTexte.innerHTML = "Veuillez entrez les Tensions et intensité";
                  }
              }); // Fermeture       

            }
    </script>
    <script src="scripthameg.js"></script>
<!--    </form> -->
  </body>

</html>