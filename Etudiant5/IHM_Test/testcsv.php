<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Récupérer les valeurs des inputs
    $inPut1 = $_POST['inPut1'];
    $inPut2 = $_POST['inPut2'];
    $inPut3 = $_POST['inPut3'];
    $inPut4 = $_POST['inPut4'];
    $inPut5 = $_POST['inPut5'];
    $inPut6 = $_POST['inPut6'];
    $inPut7 = $_POST['inPut7'];
    $inPut8 = $_POST['inPut8'];

    // Créer le contenu CSV
    $csvContent = "inPut1, inPut2, inPut3, inPut4, inPut5, inPut6, inPut7, inPut8\n";
    $csvContent .= "$inPut1, $inPut2, $inPut3, $inPut4, $inPut5, $inPut6, $inPut7, $inPut8\n";

    // Définir le nom du fichier CSV
    $csvFilename = "testcsv.csv";

    // Écrire le contenu dans le fichier CSV
    file_put_contents($csvFilename, $csvContent);
}

?>