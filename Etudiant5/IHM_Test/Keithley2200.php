<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Keithley260A</title>
    <link href="Style/test.css" type="text/css" rel="stylesheet">
   <!--Lien du site :https://jquery.com/download/ -->
    <script type="text/javascript" src="jquery-3.6.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

</head>
<body>

    <nav>
      <h1 id="h1Nav">Piseo</h1>
      <ul>
        <li><a href="Keithley260A.php">Keithley260A</a>
     
        <li><a href="Keithley260AContinu.php">Keithley260A en Continue</a></li>
      </li>
        <li><a href="Hameg4040.php">Hmeg4040</a></li>
      </ul>
    </nav>

          <div id="bdId">

          <h1>Keitlhey 2200</h1>

  <div id="div_Port">
     <h1 id="h1Port">Port:</h1>
      <select id="pet-select" name="SelectPort">
         <option value="port">--Port--</option>
         <option value="1" >COM1</option>
         <!--<option value="USB">USB</option>
          <option value="Ethernet">Ethernet</option>-->
      </select>
  </div>

      <div id=btId>
        <button name="Test_connexion" onclick="Testconnexion()" id="demo" type="submit">Test connexion</button>
        <button name="Allumer" onclick="Allumer()">Allumer</button>  
        <button name="Enrengistre_Val" id="Enrengistre_Val" >Modifier Valeur</button >
      </div>

  <section id="div_Channel">
      <div id="chBoxId">
       </div>

      <form action="">
    <div id="lbDiv">
        <h1>Valeur souhaitée</h1>     
          <!-- input pour le channel 1 , Les ampére seront en A ou miliAmpére-->
          <div id="ldChaDiv">
          <label for="name">A:<input type="number" name="inPut1" id="inPut1" min="0" max="10"></label></br>
          <label for="name">V:<input type="number" name="inPut2" id="inPut2"min="0" max="10"></label></br></br>
          </div>
          <!-- input pour le channel 2-->    
      </div>
      </form> 
  </section>  

    <div id="div_ValeurReel">
       <h1>Valeur réelle</h1>
       <div id="ch12ValeurReel">
       <h2 id="h2ValeurReel">cH1</h2>
        <h3>V:<div id="valeur"></div></h3>
        <h3>A:<div id="valeur1"></div></h3>
        </div>
        <p>Définir interval de temps</p><input type="number" id="IntervalTemps" placeholder="F échatillonage">
    </div>


     <div id="aide">
     <div id="TexteAide">
     </div>
     


<script>
  //Script pour afficher valeur réelle 
  var intervalId = setInterval(function() {
    $.ajax({                   
      // Exécution d’une requête Ajax avec la configuration donnée par l'objet suivant :
            async: "true",               // - requête asynchrone
            type: "GET",                 // - type HTTP GET
            url: "http://127.0.0.1:8000/alim/Keithley2200/lire/1/",           // - URL de la page à charger
            //data: "email=" + encodeURIComponent(email) + "&action=get_email", // - données à envoyer
            error: function(errorData) { // - fonction de rappel en cas d’erreur
                $("#error").html(errorData);
            },
            success: function(data) {  
              document.getElementById("valeur").innerHTML =data.intensity; 
              document.getElementById("valeur1").innerHTML =data.tension; 
                // - fonction de rappel pour le traitement des données reçues en cas de succès
            }
        }); // Fermeture de l'appel à la fonction $.ajax
  }, 5000);

</script>   
</script>
<script>
           var AideTexte = document.getElementById("TexteAide");
                AideTexte.innerHTML = "Veuillez Testé séléctioner Port";
          
            var SelectPort=document.getElementById("pet-select");
            var check=$("#Test_connexion").val();
            console.log(check);            
                          
           function Testconnexion(){
          var ValCom =$("#pet-select").val();

            $.ajax({        
              
                  async: "true",               // - requête asynchrone
                  type: "GET",                 // - type HTTP GET
                  url: "http://127.0.0.1:8000/alim/Keithley2200/testerPortCom/" +ValCom ,           // - URL de la page à charger
                  //data: "email=" + encodeURIComponent(email) + "&action=get_email", // - données à envoyer
                  error: function(errorData) { // - fonction de rappel en cas d’erreur
                      $("#error").html(errorData);
                  },
                  success: function(data) {  
                  
                    document.getElementById("TexteAide").innerHTML =data.status ; 
                    if(data.status=='ok'){
                    SelectPort.disabled = true; // désactiver l'input
                    SelectPort.style.opacity = "2.5";
                      // - fonction de rappel pour le traitement des données reçues en cas de succès
                      AideTexte.innerHTML = "Veuillez entrez les Tensions et intensité";
                     } //$("#deg").html(data); $("#error").append("Contenu chargé");
                  }
              }); // Fermeture
              
            }
    </script>
    <script src="scriptkeithley2200.js"></script>
  </div>
            </div>
          </form>  
  </body>

</html>