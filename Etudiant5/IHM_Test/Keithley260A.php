
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Keithley260A</title>
    <link href="Style/Alim_keithley2060A.css" type="text/css" rel="stylesheet">
   <!--Lien du site :https://jquery.com/download/ -->
    <script type="text/javascript" src="jquery-3.6.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

</head>
<body>

    <nav>
      <h1 id="h1Nav">Piseo</h1>
      <ul>
        <li><a href="Keithley2200.php">Keithley2200</a></li>
        <li><a href="Hameg4040.php">Hmeg4040</a></li>
        <li><a href="Keithley260AContinu.php">Keithley260A2 continue</a></li>
      </ul>
    </nav>

  <div id="bdId">
    
  <h1>Keitlhey 2602A</h1>

  <div id="div_Port">
     <h1 id="h1Port">Port:</h1>
    <!--  <select id="pet-select" name="SelectPort">
         Grâce à l'API il y'a aura une liste de port COM
         <option value="port">--Port--</option>
         <option value="1" >COM1</option>
         <option value="USB">USB</option>
          <option value="Ethernet">Ethernet</option>
      </select>-->
      <input type="number" placeholder="Port" name="port" id="port" value="5025">
      <input type="text" name="ip" placeholder="Address IP" id="ip" value="127.0.0.1">
  </div>

      <div id=btId> 
      <button name="Test_connexion" onclick="Testconnexion()" id="demo" type="submit">Test connexion</button>
        <!--<button name="Allumer"  name="Enrengistre_Val" id="Enrengistre_Val">Envoyer</button>--> 
        <button name="piloter" onclick="Piloter()"  type="submit">Piloter</button>
        <!--<button name="Enrengistre_Val" id="Enrengistre_Val" >Modifié Valeur</button >-->
        <!--<button name="Bt_Eteindre" onclick="functionEteindre()">Eteindre</button>-->    
      </div>

  <section id="div_Channel">
      <!--<div id="chBoxId">   
      </div>-->
    <form action="">
    <div id="lbDiv">
        <h2 id="h1_definir">Définir</h2>
        <h2 id="h1_Intensite">Valeur max:</h2>
         
       <!-- input pour le channel 1 , Les ampére seront en A ou miliAmpére-->
        <div id="ldChaDiv">
          <label for="name">A Max:<input type="number" name="LimiteIntensite" id="LimiteIntensite" min="0" max="10"></label></br>
          <label for="name">V Max:<input type="number" name="LimiteTension" id="LimiteTension" min="0" max="10"></label></br></br>
        </div>
         <!-- input pour le channel 1 , Les ampére seront en A ou miliAmpére-->
         <div id="ldChaDivS">
          <h2 id="h1_Valeursouhaiter">Valeur souhaitée:</h2>
          <label for="name">A:<input type="number" name="Vsouhaite" id="Vsouhaite" min="0" max="10"></label></br>
          <!-- input pour le channel 2-->
         </div>
       
        <h2 id="h1_duree_on">Durée du ON:</h2>
        <div id="DuOFF"><label for="name"><input type="number" name="LbDureeON" id="Id_DureeON"></label></div>
        <h2 id="h1_duree_on">Durée du OFF:</h2>
        <div id="DuOn"><label for="name"><input type="number" name="LbDureeOFF" id="Id_DureeOFF"></label></div>
        <h2 id="h1_NbPulse">Nombre de pulse:</h2>
        <div id="NbPulse"><label for="name"><input type="number" name="LbPulse" id="Id_LbPulse"></label></div>     
    </div>
    </form> 
  </section>  

    <div id="div_ValeurReel">
       <h1>Valeur de sortie</h1>
        <div id="ch12ValeurReel">
          <h2 id="h2ValeurReel">cH1</h2>
            <h3>V:<div id="valeur"></div></h3>
            <h3>A:</h3>
        </div>
    </div>


     <div id="aide">
     <div id="TexteAide">
     </div>
   
<script>
  var intervalId = setInterval(function() {
    $.ajax({                
      // Exécution d’une requête Ajax avec la configuration donnée par l'objet suivant :
            async: "true",               // - requête asynchrone
            type: "GET",                 // - type HTTP GET
            url: "http://127.0.0.1:8000/alim/Keithley2602A/lire/test/1/",           // - URL de la page à charger
            //data: "email=" + encodeURIComponent(email) + "&action=get_email", // - données à envoyer
            error: function(errorData) { // - fonction de rappel en cas d’erreur
                $("#error").html(errorData);
            },
            success: function(data) {  
              document.getElementById("valeur").innerHTML =data.tension; 
              document.getElementById("valeur1").innerHTML =data.intensity; 
            }
        }); // Fermeture de l'appel à la fonction $.ajax 
  }, 5000);

</script>     
</div>
            </div>
    <script>
                var AideTexte = document.getElementById("TexteAide");
                AideTexte.innerHTML = "Veuillez Testé séléctioner Port";           
            //Premiére instruction       
          
           function Testconnexion(){
            var ValPort=document.getElementById("port").value;
            var ip=document.getElementById("ip").value;
            $.ajax({        
              
                  async: "true",               // - requête asynchrone
                  type: "GET",                 // - type HTTP GET
                  url: "http://127.0.0.1:8000/alim/Keithley2602A/testerPortCom/"+ip+"/" +ValPort ,           // - URL de la page à charger
                  //data: "email=" + encodeURIComponent(email) + "&action=get_email", // - données à envoyer
                  error: function(errorData) { // - fonction de rappel en cas d’erreur
                      $("#error").html(errorData);
                  },
                  success: function(data) {                    
                    document.getElementById("TexteAide").innerHTML =data.status;
                    if(data.status=='ok'){
                      // - fonction de rappel pour le traitement des données reçues en cas de succès
                     } 
                  }
              }); // Fermeture
              AideTexte.innerHTML = "Veuillez entrez les Tensions et intensité";
            }
    </script>
            <script src="scriptKeithley260A.js"></script> -->
  </body>

</html>