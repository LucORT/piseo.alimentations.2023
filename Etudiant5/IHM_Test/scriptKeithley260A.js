// Lire les valeurs des inputs et envoyer les requêtes AJAX pour les enregistrer
//nombre de pulse

function Piloter(){
    var  nbPulse=document.getElementById("Id_LbPulse").value;
    for (var i = 0; i < nbPulse; i++){
        var levelI =document.getElementById("Vsouhaite").value;
        var LimiteI = document.getElementById("LimiteIntensite").value;
        var LimiteV = document.getElementById("LimiteTension").value;
        var delay=document.getElementById("Id_DureeON").value;
        var ValPort = document.getElementById("port").value;
        var ip = document.getElementById("ip").value;

  // Requête AJAX pour piloter l'alimentation
  $.ajax({              
      async: true,               
      type: "GET",
      url: "http://127.0.0.1:8000/alim/Keithley2602A/piloter/" + ip + "/" +LimiteI+ "/" +LimiteV+ "/"+levelI+"/"+delay+"/",
      error: function(errorData) { // fonction de rappel en cas d’erreur
          $("#error").html(errorData);
      },
      success: function(data) {
          // Code à exécuter en cas de succès de la requête
      }
  });
}

}

function functionEteindre() {
  var ip = document.getElementById("ip").value;
  var ValPort = document.getElementById("port").value;
  
  // Requête AJAX pour éteindre l'alimentation
  $.ajax({              
      async: true,               
      type: "GET",          
      url: "http://127.0.0.1:8000/alim/Keithley2602A/eteindreAlim/" + ip + "/" + ValPort,
      error: function(errorData) { // fonction de rappel en cas d’erreur
          $("#error").html(errorData);
      },
      success: function(data) {
          // Code à exécuter en cas de succès de la requête
      }
  });
}
