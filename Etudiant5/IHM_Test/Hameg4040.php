<!DOCTYPE html>
<html>
<head> 
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="Style/test.css" type="text/css" rel="stylesheet">
    <title>Hameg 4040</title>
    <!--Lien du site :https://jquery.com/download/ -->
    <script type="text/javascript" src="jquery-3.6.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
</head>
<body>
  <nav>
    <h1 id="h1Nav">Piseo</h1>
    <ul>
      <li><a href="Keithley2200.php">Keithley2200</a></li>
      <li><a href="Keithley260A.php">Keithley2060A</a></li>
    </ul>
  </nav>

  <div id="bdId">
    <h1>Hameg 4040</h1>

    <div id="div_Port">
      <h1 id="h1Port">Port:</h1>
      <select id="pet-select" name="SelectPort">
        <!-- <option value="port">--Port--</option>
        <option value="5" id="PortCom1">COM1</option>
        <option value="2" id="PortCom2">COM2</option>
        <option value="3" id="PortCom3">COM3</option>
        <option value="4" id="PortCom4">COM4</option>-->
        <option value="4" id="port"></option>
         <!-- <option value="Ethernet">Ethernet</option>-->
      </select>
    </div>

    <div id=btId>
      <button name="Test_connexion" onclick="testConnexion()" id="demo" type="submit">Test connexion</button>
      <button name="DesactiverSortie" id="DesactiverSortie" id="demo" type="submit">Désactiver Sortie</button>
      <button name="Allumer" id="Allumer" onclick="Myfunction1()" class="off">Piloter</button>
      <style type="text/css">
        .off {
          background-color: red;
        }

        .on {
          background-color: green;
        }
      </style>
     <button name="Enrengistre_Val" id="Enrengistre_Val">Envoyer</button>
     <button name="DesactiverCourant" id="DesactiverCourant">Désactiver courant</button>
      <button type="submit" onclick="EnrengistrerCSV()"><a href="testcsv.csv">Enrengistre CSV</a></button>
    </div>

    <section id="div_Channel">

      <div id="chBoxId">

        <div id="chBox1">
          <input type="checkbox" id="Id_Channel" name="Channel" value="1">
          <label for="Lb_Channel1">1</label>
        </div>

        <div id="chBox2">
          <input type="checkbox" id="Id_Channel" name="Channel" value="2">
          <label for="Lb_Channel2">2</label>
        </div>

        <div id="chBox3">
          <input type="checkbox" id="Id_Channel" name="Channel" value="3">
          <label for="Lb_Channel3">3</label>
        </div>

        <div id="chBox4">
          <input type="checkbox" id="Id_Channel" name="Channel" value="4">
          <label for="Lb_Channel4">4</label>
        </div>

      </div>

      <div id="lbDiv">
        <h1>Valeur max</h1>
        <form action="">
        <!-- input pour le channel 1 , Les ampéres seront en A ou miliAmpére, keithley 2200 30V et 2.5A-->
        <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut1" id="inPut1" min="0" max="2"></label><br>
          <label for="name">A:<input type="number" name="inPut2" id="inPut2" min="0" max="30"></label><br><br>
        </div>

        <!-- input pour le channel 2-->
        <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut3" id="inPut3" min="0" max="0"></label><br>
          <label for="name">A:<input type="number" name="inPut4" id="inPut4" min="0" max="30"></label><br><br>
        </div>

        <!-- input pour le channel 3-->
        <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut5" id="inPut5" min="0" max="0"></label><br>
          <label for="name">A:<input type="number" name="inPut6" id="inPut6" min="0" max="30"></label><br><br>
        </div>

        <!-- input pour le channel 4-->
        <div id="ldChaDiv">
          <label for="name">V:<input type="number" name="inPut7" id="inPut7" min="0" max="0"></label><br>
          <label for="name">A:<input type="number" name="inPut8" id="inPut8" min="0" max="30"></label><br><br>
          <button type="reset">reset</button>
          </form>
        </div>
      </div>

    </section>

    <div id="div_ValeurReel">
      <h1>Valeur de sortie</h1>
      <div id="ch12ValeurReel">
        <h2 id="h2ValeurReel">cH1</h2>
        <h3>V:<div id="valeur"></div></h3>
        <h3>A:<div id="valeur1"></div></h3>
        <h2 id="h2ValeurReel">cH2</h2>
        <h3>V:<div id="valeur2"></div></h3>
        <h3>A:<div id="valeur3"></div></h3>
      </div>

      <div id="ch34ValeurReel">
        <h2 id="h23ValeurReel">cH3</h2>
        <h3>V:<div id="valeur4"></div></h3>
        <h3>A:<div id="valeur5"></div></h3>
        <h2 id="h24ValeurReel">cH4</h2>
        <h3>V:<div id="valeur6"></div></h3>
        <h3>A:<div id="valeur7"></div></h3>
      </div>
      <p>Définir interval de temps</p><input type="number" id="IntervalTemps" placeholder="Fréchantillonnage">
    </div>

  <div id="aide">
    <div id="TexteAide"></div>
  </div>

  <!--Script pour le csv Valeur Réelle -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <!--Script pour afficher valeur réelle -->
  <!--<script>

   var ValCom = $("#pet-select").val();
    var intervalId = setInterval(function() {
        $.ajax({
          async: "true",
          type: "GET",
          url: "http://127.0.0.1:8000/alim/HMP4040/lire/1/1/",
          error: function(errorData) {
            $("#error").html(errorData);
          },
          success: function(data) {
            // Handle success response
            document.getElementById("valeur").innerHTML =data.tension; 
            document.getElementById("valeur1").innerHTML =data.intensity; 
          }
        });
      }, 500);
    

   
    var  intervalId = setInterval(function() {
        $.ajax({
          async: "true",
          type: "GET",
          url: "http://127.0.0.1:8000/alim/HMP4040/lire/"+Valcom+"/2/",
          error: function(errorData) {
            $("#error").html(errorData);
          },
          success: function(data) {
            // Handle success response
            document.getElementById("valeur2").innerHTML =data.tension; 
            document.getElementById("valeur3").innerHTML =data.intensity; 
          }
        });
      }, 500);

    var  intervalId = LireValeurSorti3(function() {
        $.ajax({
          async: "true",
          type: "GET",
          url: "http://127.0.0.1:8000/alim/HMP4040/lire/"+Valcom+"/3/",
          error: function(errorData) {
            $("#error").html(errorData);
          },
          success: function(data) {
            // Handle success response
            document.getElementById("valeur4").innerHTML =data.tension; 
            document.getElementById("valeur5").innerHTML =data.intensity; 
          }
        });
      }, 500);

    var  intervalId = LireValeurSorti4(function() {
        $.ajax({
          async: "true",
          type: "GET",
          url: "http://127.0.0.1:8000/alim/HMP4040/lire/"+Valcom+"/4/",
          error: function(errorData) {
            $("#error").html(errorData);
          },
          success: function(data) {
            // Handle success response
            document.getElementById("valeur6").innerHTML =data.tension; 
            document.getElementById("valeur7").innerHTML =data.intensity; 
          }
        });
      }, 500);
 
  //Afficher les ports disponibles
  $.ajax({
          async: "true",
          type: "GET",
          url: "http://127.0.0.1:8000/alim/HMP4040/listePort/",
          error: function(errorData) { 
            $("#error").html(errorData);
          },
          success: function(data) {
            alert(data[0]);
          }
        });
    
  </script>-->
  <script>
    $.ajax({
          async: "true",
          type: "GET",
          url: "http://127.0.0.1:8000/alim/HMP4040/listePort/",
          error: function(errorData) { 
            $("#error").html(errorData);
          },
          success: function(data) {
            alert("Le(s) port(s) disponible(s) sont les suivants :"+data.liste);
            document.getElementById("port").innerHTML = data.liste;
          }
        });
  </script>
  <script>
    var intervalId;
    var filename = "testcsv.php";

    function startInterval() {
      intervalId = setInterval(function() {
        $.ajax({
          async: "true",
          type: "GET",
          url: filename,
          error: function(errorData) {
            $("#error").html(errorData);
          },
          success: function(data) {
            // Handle success response
          }
        });
      }, 1500);
    }

    function stopInterval() {
      clearInterval(intervalId);
    }

    function enregistrerCSV() {
      const fakeHamegCsv = "testcsv.csv";
      var csvContent = "CSV data"; // Replace with your CSV data

      var downloadLink = document.createElement('a');
      downloadLink.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent));
      downloadLink.setAttribute('download', fakeHamegCsv);
      document.body.appendChild(downloadLink);
      downloadLink.click();
    }

    var AideTexte = document.getElementById("TexteAide");
    AideTexte.innerHTML = "Veuillez tester et sélectionner un port";

    var SelectPort = document.getElementById("pet-select");

    function testConnexion() {
      var ValCom = $("#pet-select").val();
      $.ajax({
        async: "true",
        type: "GET",
        url: "http://127.0.0.1:8000/alim/HMP4040/testerPortCom/" + ValCom,
        error: function(errorData) {
          $("#error").html(errorData);
          alert("Le port com ne fonctionne pas");
        },
        success: function(data) {
          document.getElementById("TexteAide").innerHTML = data.status;
          alert("Le port com fonctionne");
          if (data.status == 'OK') {
           // SelectPort.disabled = true;
            //SelectPort.style.opacity = "1.5";
          }
          AideTexte.innerHTML = "Veuillez entrer les tensions et l'intensité puis cocher le channel";
        }
      });
    }
  </script>
  <script src="scripthameg.js"></script>
</body>


