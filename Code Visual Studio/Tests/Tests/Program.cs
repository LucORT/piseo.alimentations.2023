﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class Program
    {
        static void Main(string[] args)
        {
            SerialPort hameg = new SerialPort("COM3");

            try
            {
                if (hameg.IsOpen == false)
                {
                    hameg.Open();
                    Console.WriteLine("Success !");
                }

                bool Stop = false;

                do
                {
                    Console.Write("Commande : ");
                    string commande = Console.ReadLine();
                    hameg.WriteLine(commande);

                    if (commande.Contains("?"))
                    {
                        Console.WriteLine(hameg.ReadLine());
                    }

                } while (Stop == false);

                hameg.Close();

            }
            catch (Exception)
            {
                if (hameg.IsOpen == true)
                {
                    Console.WriteLine("Connexion échouée !");
                }
                else
                {
                    Console.WriteLine("Le port est déjà utilisé !");
                }
            }


            Console.ReadKey();

        }
    }
}
