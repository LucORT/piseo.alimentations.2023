﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlimDeTest;
using System.Windows.Forms;

namespace IHMdeTest
{
    public partial class IHMdeTest : Form
    {
        HMP4040_Channel oAlim;


        public IHMdeTest()
        {
            InitializeComponent();   
        }

        //Bouton connexion
        private void bConnexion_Click(object sender, EventArgs e)
        {
            if (bConnexion.Text == "Test de connexion")
            {
                nCOM.Enabled = false;
                bConnexion.Enabled = false;
                rtbInfos.Text = "Tentative de connexion...";

                string port = "COM" + nCOM.Value;
                oAlim = new HMP4040_Channel(port, 1);

                //Tentative de connexion
                if (oAlim.TestConnexion() == true)
                {
                    rtbInfos.Text = "Connexion réussi !";
                    bConnexion.Text = "Déconnexion";
                    bConnexion.Enabled = true;

                    VerificationG();
                    ActivationG();
                    
                } else {
                    bConnexion.Enabled = true;
                    nCOM.Enabled = true;
                    rtbInfos.Text = "Echec de connexion, le port n'est pas reconnu ou il est déjà utilisé";
                }

            } else 
            if (bConnexion.Text == "Déconnexion") 
            {
                DesactivationG();

                //Déconnexion
                oAlim.Deconnexion();

                rtbInfos.Text = "";
                bConnexion.Text = "Test de connexion";
                nCOM.Enabled = true;
            }
        }

        private void bOnOff_Click(object sender, EventArgs e)
        {
            if (bEtatChannel.Text == "Allumer")
            {
                oAlim.Allumer();
                pEtatChannel.BackColor = Color.Lime;
                bEtatChannel.Text = "Eteindre";
                CH1.Enabled = true;
            } else if (bEtatChannel.Text == "Eteindre")
            {
                oAlim.Eteindre();
                pEtatChannel.BackColor = Color.Red;
                bEtatChannel.Text = "Allumer";

                CH1.Enabled = false;
                lTensionR.Text = "0.000 V";
                lIntensiteR.Text = "0.0000 A";
            }
        }

        public void ActivationG()
        {
            gbChannel.Enabled = true;
            gbValeursC.Enabled = true;
            gbValeursR.Enabled = true;
            bModifierValeurs.Enabled = false;
        }

        public void DesactivationG()
        {
            gbChannel.Enabled = false;
            gbValeursC.Enabled = false;
            gbValeursR.Enabled = false;

            CH1.Enabled = false;
        }

        public void VerificationG()
        {
            if (oAlim.VerifChannel())
            {
                pEtatChannel.BackColor = Color.Lime;
                bEtatChannel.Text = "Eteindre";
                CH1.Enabled = true;
            } else
            {
                pEtatChannel.BackColor = Color.Red;
                CH1.Enabled = false;
                bEtatChannel.Text = "Allumer";
                lTensionR.Text = "0.000 V";
                lIntensiteR.Text = "0.0000 A";
            }

            tbIntensiteC.Text = oAlim.LireIntensitenConsigne();
            tbTensionC.Text = oAlim.LireTensionConsigne();
        }

        private void CH1_Tick(object sender, EventArgs e)
        {
            lIntensiteR.Text = oAlim.LireIntensiteReelle() + " A";
            lTensionR.Text = oAlim.LireTensionReelle() + " V";
        }

        private void bModifierValeurs_Click(object sender, EventArgs e)
        {
            string tension = tbTensionC.Text;
            string intensite = tbIntensiteC.Text;

            oAlim.ModifierValeurs(tension, intensite);
            rtbInfos.Text = "Valeurs modifiées !";

            tbTensionC.Text = oAlim.LireTensionConsigne();
            tbIntensiteC.Text = oAlim.LireIntensitenConsigne();
        }

        private void tbTesnionC_TextChanged(object sender, EventArgs e)
        {
            bModifierValeurs.Enabled = true;
        }

        private void tbTensionC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbIntensiteC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void bReset_Click(object sender, EventArgs e)
        {
            oAlim.Reset();
            VerificationG();

            rtbInfos.Text = "Réinitialisation terminée";
        }
    }
}
