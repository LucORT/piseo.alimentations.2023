﻿
namespace IHMdeTest
{
    partial class IHMdeTest
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbConnexion = new System.Windows.Forms.GroupBox();
            this.bConnexion = new System.Windows.Forms.Button();
            this.nCOM = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.gbChannel = new System.Windows.Forms.GroupBox();
            this.bReset = new System.Windows.Forms.Button();
            this.bEtatChannel = new System.Windows.Forms.Button();
            this.pEtatChannel = new System.Windows.Forms.Panel();
            this.gbValeursR = new System.Windows.Forms.GroupBox();
            this.lIntensiteR = new System.Windows.Forms.Label();
            this.lTensionR = new System.Windows.Forms.Label();
            this.gbValeursC = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bModifierValeurs = new System.Windows.Forms.Button();
            this.tbIntensiteC = new System.Windows.Forms.TextBox();
            this.tbTensionC = new System.Windows.Forms.TextBox();
            this.gbInfos = new System.Windows.Forms.GroupBox();
            this.rtbInfos = new System.Windows.Forms.RichTextBox();
            this.CH1 = new System.Windows.Forms.Timer(this.components);
            this.gbConnexion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nCOM)).BeginInit();
            this.gbChannel.SuspendLayout();
            this.gbValeursR.SuspendLayout();
            this.gbValeursC.SuspendLayout();
            this.gbInfos.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbConnexion
            // 
            this.gbConnexion.Controls.Add(this.bConnexion);
            this.gbConnexion.Controls.Add(this.nCOM);
            this.gbConnexion.Controls.Add(this.label1);
            this.gbConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbConnexion.Location = new System.Drawing.Point(16, 15);
            this.gbConnexion.Margin = new System.Windows.Forms.Padding(4);
            this.gbConnexion.Name = "gbConnexion";
            this.gbConnexion.Padding = new System.Windows.Forms.Padding(4);
            this.gbConnexion.Size = new System.Drawing.Size(268, 145);
            this.gbConnexion.TabIndex = 0;
            this.gbConnexion.TabStop = false;
            this.gbConnexion.Text = "Connexion";
            // 
            // bConnexion
            // 
            this.bConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bConnexion.Location = new System.Drawing.Point(15, 85);
            this.bConnexion.Margin = new System.Windows.Forms.Padding(4);
            this.bConnexion.Name = "bConnexion";
            this.bConnexion.Size = new System.Drawing.Size(237, 43);
            this.bConnexion.TabIndex = 2;
            this.bConnexion.Text = "Test de connexion";
            this.bConnexion.UseVisualStyleBackColor = true;
            this.bConnexion.Click += new System.EventHandler(this.bConnexion_Click);
            // 
            // nCOM
            // 
            this.nCOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCOM.Location = new System.Drawing.Point(157, 39);
            this.nCOM.Margin = new System.Windows.Forms.Padding(4);
            this.nCOM.Name = "nCOM";
            this.nCOM.Size = new System.Drawing.Size(76, 32);
            this.nCOM.TabIndex = 1;
            this.nCOM.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port : COM";
            // 
            // gbChannel
            // 
            this.gbChannel.Controls.Add(this.bReset);
            this.gbChannel.Controls.Add(this.bEtatChannel);
            this.gbChannel.Controls.Add(this.pEtatChannel);
            this.gbChannel.Enabled = false;
            this.gbChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbChannel.Location = new System.Drawing.Point(292, 15);
            this.gbChannel.Margin = new System.Windows.Forms.Padding(4);
            this.gbChannel.Name = "gbChannel";
            this.gbChannel.Padding = new System.Windows.Forms.Padding(4);
            this.gbChannel.Size = new System.Drawing.Size(180, 334);
            this.gbChannel.TabIndex = 1;
            this.gbChannel.TabStop = false;
            this.gbChannel.Text = "Channel";
            // 
            // bReset
            // 
            this.bReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReset.Location = new System.Drawing.Point(21, 267);
            this.bReset.Margin = new System.Windows.Forms.Padding(4);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(139, 53);
            this.bReset.TabIndex = 2;
            this.bReset.Text = "Reset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // bEtatChannel
            // 
            this.bEtatChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bEtatChannel.Location = new System.Drawing.Point(21, 192);
            this.bEtatChannel.Margin = new System.Windows.Forms.Padding(4);
            this.bEtatChannel.Name = "bEtatChannel";
            this.bEtatChannel.Size = new System.Drawing.Size(137, 57);
            this.bEtatChannel.TabIndex = 1;
            this.bEtatChannel.Text = "Allumer";
            this.bEtatChannel.UseVisualStyleBackColor = true;
            this.bEtatChannel.Click += new System.EventHandler(this.bOnOff_Click);
            // 
            // pEtatChannel
            // 
            this.pEtatChannel.BackColor = System.Drawing.Color.Red;
            this.pEtatChannel.Location = new System.Drawing.Point(44, 65);
            this.pEtatChannel.Margin = new System.Windows.Forms.Padding(4);
            this.pEtatChannel.Name = "pEtatChannel";
            this.pEtatChannel.Size = new System.Drawing.Size(87, 80);
            this.pEtatChannel.TabIndex = 0;
            // 
            // gbValeursR
            // 
            this.gbValeursR.Controls.Add(this.lIntensiteR);
            this.gbValeursR.Controls.Add(this.lTensionR);
            this.gbValeursR.Enabled = false;
            this.gbValeursR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValeursR.Location = new System.Drawing.Point(480, 15);
            this.gbValeursR.Margin = new System.Windows.Forms.Padding(4);
            this.gbValeursR.Name = "gbValeursR";
            this.gbValeursR.Padding = new System.Windows.Forms.Padding(4);
            this.gbValeursR.Size = new System.Drawing.Size(258, 127);
            this.gbValeursR.TabIndex = 2;
            this.gbValeursR.TabStop = false;
            this.gbValeursR.Text = "Valeurs instantanées";
            // 
            // lIntensiteR
            // 
            this.lIntensiteR.AutoSize = true;
            this.lIntensiteR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIntensiteR.Location = new System.Drawing.Point(64, 79);
            this.lIntensiteR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lIntensiteR.Name = "lIntensiteR";
            this.lIntensiteR.Size = new System.Drawing.Size(99, 26);
            this.lIntensiteR.TabIndex = 1;
            this.lIntensiteR.Text = "0.0000 A";
            // 
            // lTensionR
            // 
            this.lTensionR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lTensionR.AutoSize = true;
            this.lTensionR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTensionR.Location = new System.Drawing.Point(69, 46);
            this.lTensionR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTensionR.Name = "lTensionR";
            this.lTensionR.Size = new System.Drawing.Size(87, 26);
            this.lTensionR.TabIndex = 0;
            this.lTensionR.Text = "0.000 V";
            this.lTensionR.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gbValeursC
            // 
            this.gbValeursC.Controls.Add(this.label3);
            this.gbValeursC.Controls.Add(this.label2);
            this.gbValeursC.Controls.Add(this.bModifierValeurs);
            this.gbValeursC.Controls.Add(this.tbIntensiteC);
            this.gbValeursC.Controls.Add(this.tbTensionC);
            this.gbValeursC.Enabled = false;
            this.gbValeursC.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValeursC.Location = new System.Drawing.Point(480, 149);
            this.gbValeursC.Margin = new System.Windows.Forms.Padding(4);
            this.gbValeursC.Name = "gbValeursC";
            this.gbValeursC.Padding = new System.Windows.Forms.Padding(4);
            this.gbValeursC.Size = new System.Drawing.Size(258, 199);
            this.gbValeursC.TabIndex = 3;
            this.gbValeursC.TabStop = false;
            this.gbValeursC.Text = "Valeurs consignes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(152, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(152, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "V";
            // 
            // bModifierValeurs
            // 
            this.bModifierValeurs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bModifierValeurs.Location = new System.Drawing.Point(16, 143);
            this.bModifierValeurs.Margin = new System.Windows.Forms.Padding(4);
            this.bModifierValeurs.Name = "bModifierValeurs";
            this.bModifierValeurs.Size = new System.Drawing.Size(207, 43);
            this.bModifierValeurs.TabIndex = 2;
            this.bModifierValeurs.Text = "Modifier valeurs";
            this.bModifierValeurs.UseVisualStyleBackColor = true;
            this.bModifierValeurs.Click += new System.EventHandler(this.bModifierValeurs_Click);
            // 
            // tbIntensiteC
            // 
            this.tbIntensiteC.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntensiteC.Location = new System.Drawing.Point(60, 86);
            this.tbIntensiteC.Margin = new System.Windows.Forms.Padding(4);
            this.tbIntensiteC.Name = "tbIntensiteC";
            this.tbIntensiteC.Size = new System.Drawing.Size(89, 32);
            this.tbIntensiteC.TabIndex = 1;
            this.tbIntensiteC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbIntensiteC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntensiteC_KeyPress);
            // 
            // tbTensionC
            // 
            this.tbTensionC.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTensionC.Location = new System.Drawing.Point(60, 46);
            this.tbTensionC.Margin = new System.Windows.Forms.Padding(4);
            this.tbTensionC.Name = "tbTensionC";
            this.tbTensionC.Size = new System.Drawing.Size(89, 32);
            this.tbTensionC.TabIndex = 0;
            this.tbTensionC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTensionC.TextChanged += new System.EventHandler(this.tbTesnionC_TextChanged);
            this.tbTensionC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTensionC_KeyPress);
            // 
            // gbInfos
            // 
            this.gbInfos.Controls.Add(this.rtbInfos);
            this.gbInfos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbInfos.Location = new System.Drawing.Point(16, 167);
            this.gbInfos.Margin = new System.Windows.Forms.Padding(4);
            this.gbInfos.Name = "gbInfos";
            this.gbInfos.Padding = new System.Windows.Forms.Padding(4);
            this.gbInfos.Size = new System.Drawing.Size(268, 181);
            this.gbInfos.TabIndex = 4;
            this.gbInfos.TabStop = false;
            this.gbInfos.Text = "Informations";
            // 
            // rtbInfos
            // 
            this.rtbInfos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInfos.Location = new System.Drawing.Point(7, 32);
            this.rtbInfos.Margin = new System.Windows.Forms.Padding(4);
            this.rtbInfos.Name = "rtbInfos";
            this.rtbInfos.ReadOnly = true;
            this.rtbInfos.Size = new System.Drawing.Size(255, 143);
            this.rtbInfos.TabIndex = 0;
            this.rtbInfos.Text = "";
            // 
            // CH1
            // 
            this.CH1.Interval = 500;
            this.CH1.Tick += new System.EventHandler(this.CH1_Tick);
            // 
            // IHMdeTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 361);
            this.Controls.Add(this.gbInfos);
            this.Controls.Add(this.gbValeursC);
            this.Controls.Add(this.gbValeursR);
            this.Controls.Add(this.gbChannel);
            this.Controls.Add(this.gbConnexion);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "IHMdeTest";
            this.Text = "IHM de test";
            this.gbConnexion.ResumeLayout(false);
            this.gbConnexion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nCOM)).EndInit();
            this.gbChannel.ResumeLayout(false);
            this.gbValeursR.ResumeLayout(false);
            this.gbValeursR.PerformLayout();
            this.gbValeursC.ResumeLayout(false);
            this.gbValeursC.PerformLayout();
            this.gbInfos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbConnexion;
        private System.Windows.Forms.Button bConnexion;
        private System.Windows.Forms.NumericUpDown nCOM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbChannel;
        private System.Windows.Forms.Button bEtatChannel;
        private System.Windows.Forms.Panel pEtatChannel;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.GroupBox gbValeursR;
        private System.Windows.Forms.Label lIntensiteR;
        private System.Windows.Forms.Label lTensionR;
        private System.Windows.Forms.GroupBox gbValeursC;
        private System.Windows.Forms.Button bModifierValeurs;
        private System.Windows.Forms.TextBox tbIntensiteC;
        private System.Windows.Forms.TextBox tbTensionC;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbInfos;
        private System.Windows.Forms.RichTextBox rtbInfos;
        private System.Windows.Forms.Timer CH1;
    }
}

