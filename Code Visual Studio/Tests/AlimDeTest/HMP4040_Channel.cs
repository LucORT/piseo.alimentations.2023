﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;

namespace AlimDeTest
{
    public class HMP4040_Channel
    {
        private SerialPort port;
        private uint numChannel;
        private bool connecte;

        private bool EtatChannel;

        public HMP4040_Channel(string port, uint numChannel)
        {
            this.port = new SerialPort(port);
            this.numChannel = numChannel;
        }

        public void Deconnexion()
        {
            port.Close();
        }

        public bool TestConnexion()
        {
            try
            {
                if (port.IsOpen == false)
                {
                    port.Open();
                    connecte = true;
                }
            } 
            catch (Exception)
            {
                connecte = false;
            }

            return connecte;
        }

        public bool Allumer()
        {
            port.Open();
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("OUTP ON");
            port.Close();

            EtatChannel = true;
            return EtatChannel;
        }

        public bool Eteindre()
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("OUTP OFF");
            port.WriteLine("OUTP:GEN OFF");

            EtatChannel = false;
            return EtatChannel;
        }

        public string LireTensionConsigne()
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("VOLT?");

            string tensionC = port.ReadLine();
            return tensionC;
        }
        public string LireIntensitenConsigne()
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("CURR?");
            string intensiteC = port.ReadLine();
            return intensiteC;
        }

        public string LireTensionReelle()
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("MEAS:VOLT?");

            string tensionR = port.ReadLine();
            return tensionR;
        }

        public string LireIntensiteReelle()
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("MEAS:CURR?");

            string intensiteR = port.ReadLine();
            return intensiteR;
        }

        public void ModifierValeurs(string tension, string intensite)
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("APPLY " + tension + ", " + intensite);
        }

        public bool VerifChannel()
        {
            port.WriteLine("INST OUT" + this.numChannel);
            port.WriteLine("OUTP?");
            if (port.ReadLine() == "1")
            {
                EtatChannel = true;
            } else {
                EtatChannel = false;
            }

            return EtatChannel;
        }

        public void Reset()
        {
            port.WriteLine("*RST");
            this.EtatChannel = false;
        }
    }
}
