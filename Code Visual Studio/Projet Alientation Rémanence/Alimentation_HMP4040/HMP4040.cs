﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alimentation_HMP4040
{
    public class HMP4040
    {
        //Création d'un tableau composé de 4 objets de type channels représentant les 4 channels de l'alimentation
        private Channel[] channels = new Channel[4];
        public Channel[] Channels { get => channels; }

        private SerialPort oPort;

        //Paramètre port = "COMX" (Exple : COM3 / COM1)
        public HMP4040(string poPort)
        {
            //Association de la variable avec le port
            this.oPort = new SerialPort(poPort);

            //Le premier channel se trouve à l'index 0 !
            for (uint i = 0; i < Channels.Length; i++)
            {
                this.Channels[i] = new Channel(this.oPort, i);
            }
        }

        ~HMP4040()
        {
            if (oPort.IsOpen == true)
            {
                oPort.Close();
            }
        }

        //Récupération des ports dans un tableau de type string
        public static string[] DetecterPorts()
        {
            return SerialPort.GetPortNames();
        }
        
        //Tester la connexion afin de vérifier si nous pouvons établir une connexion avec l'Hameg
        public void TestConnexion()
        {
            try
            {
                //Vérification de l'état du canal de communication
                if (oPort.IsOpen == false)
                {
                    oPort.ReadTimeout = 5000;

                    //Ouverture du canal de communication
                    oPort.Open();

                    oPort.WriteLine("*IDN?");
                    string lsResultat = oPort.ReadLine();

                    if (lsResultat.Substring(0, 13) == "HAMEG,HMP4040")
                    {
                        Log loAction = new Log();
                        loAction.logWriteAction("Test connexion réussi.");
                    } else {
                        throw new TimeoutException();
                    }
                }
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est plus connectée au PC.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch(TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new TimeoutException("Le port spécifié n'est pas une alimentation Hameg HMP4040.", e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Erreur de connexion : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Désactive les channels et le bouton de sortie (output)
        public void DesactiverToutesSorties()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("OUTP:GEN OFF");

                //i+1 car les channels vont de 1 à 4 et le channel "0" n'existe pas
                for (uint i = 0; i < channels.Length; i++)
                {
                    oPort.WriteLine("INST OUT" + (i+1));
                    oPort.WriteLine("OUTP:SEL OFF");
                }               

                Log loAction = new Log();
                loAction.logWriteAction("Tous les channels et les sorties ont été désactivés.");

            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Echec, impossible de désactiver les sorties.", e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Active le bouton de sortie de courant
        public void ActiverSortie()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("OUTP:GEN ON");

                Log loAction = new Log();
                loAction.logWriteAction("Sortie activée.");
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible d'activer les sorties : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Désactive le bouton de sortie de courant
        public void DesactiverSortie()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("OUTP:GEN OFF");

                Log loAction = new Log();
                loAction.logWriteAction("Sortie désactivée.");

            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de désactiver la sortie : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Réinitialise entièrement l'Hameg
        public void Reset()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("*RST");

                Log loAction = new Log();
                loAction.logWriteAction("Réinitialisation de l'alimentation.");

            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de réinitialiser l'alimentation : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }
        //Retourne l'état du bouton de sortie de courant en format string
        public bool EtatSortie()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("OUTP:GEN?");
                string lsEtat = oPort.ReadLine();

                Log loAction = new Log();
                loAction.logWriteAction("Vérification de l'état du bouton de sortie (Output).");


                if (lsEtat == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (System.TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new OperationException("L'alimentation n'a pas répondu : " + e.Message, e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de vérifier l'état du bouton de sortie OUTPUT : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }
    }
}
