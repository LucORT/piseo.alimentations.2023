﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alimentation_HMP4040
{
    public class OperationException : Exception
    {
        public OperationException(string message, Exception e) : base(message, e)
        {

        }
        public OperationException(Exception e) : base("", e)
        {

        }
    }
}
