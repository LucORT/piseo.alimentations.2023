﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alimentation_HMP4040
{
    public class ConnexionException : Exception
    {
        public ConnexionException(string message, Exception e) : base(message, e) 
        {

        }
        public ConnexionException(Exception e) : base("", e)
        {

        }
    }
}
