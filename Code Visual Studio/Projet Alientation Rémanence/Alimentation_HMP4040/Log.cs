﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Alimentation_HMP4040
{
    public class Log
    {
        public void logWriteException(Exception poException)  //Sert à logger dans un fichier text.
        {
            
            //string docPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); Récupère le chemin du dossier d'installation
            //Console.WriteLine(Guid.NewGuid().ToString("N")); génére code aléatoire

            StreamWriter loSwriter = new StreamWriter(".\\Exceptions_Alimentation_HMP4040.txt", true, Encoding.UTF8); //Création objet qui écrira dans le fichier
            string lsDate = DateTime.Now.ToString(); //Récupère la date et l'heure à l'instant présent et le convertie en string
            string lsMessage = poException.Message;  //Récupère message de l'exception              
            string lsTrace = poException.StackTrace; //Récupère les informations de l'exceptions

            loSwriter.Write("[" + lsDate + "] " + lsMessage + " | " + lsTrace + "\n"); //écriture dans le fichier
            loSwriter.Close();
            
            
            
        }

        public void logWriteAction(string poMessage)
        {
            StreamWriter loSwriter = new StreamWriter(".\\Historique_Alimentation_HMP4040.txt", true, Encoding.UTF8); //Création objet qui écrira dans le fichier
            string lsDate = DateTime.Now.ToString();

            loSwriter.Write("[" + lsDate + "] " + poMessage + "\n");
            loSwriter.Close();

        }

        public Log()
        {
            
        }
    }
}
