﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alimentation_HMP4040
{
    public class Channel
    {
        private SerialPort oPort;
        private uint iNumChannel;

        //Constructeur utiliser avec le constructeur de la classe HMP4040
        public Channel(SerialPort poPort, uint piNumChannel)
        {
            this.oPort = poPort;

            //+1 car les tableaux commencent à 0 mais les channels à 1
            this.iNumChannel = piNumChannel + 1;
        }

        ~Channel()
        {
            if (oPort.IsOpen == true)
            {
                oPort.Close();
            }
        }

        //Active uniquement le channel visé
        public void Activer()
        {
            try
            {
                oPort.Open();

                //Séléctionne le channel "iNumChannel"
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("OUTP:SEL ON");

                Log loAction = new Log();
                loAction.logWriteAction("Channel " + this.iNumChannel + " activé.");
            }
            catch (System.IO.IOException e)
            {                
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible d'activer le channel : " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Retourne le channel visé
        public void Desactiver()
        {
            try
            {
                oPort.Open();

                //Séléctionne le channel "iNumChannel
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("OUTP:SEL OFF");

                Log loAction = new Log();
                loAction.logWriteAction("Channel " + this.iNumChannel + " désactivé.");
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);

            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de désactiver le channel : " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Retourne la tension max du channel visé
        public double LireTensionMax()
        {
            try
            {
                oPort.Open();

                //Séléctionne le channel "iNumChannel"
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("VOLT?");
                string lsValeur = oPort.ReadLine();

                double lnTensionMax = Convert.ToDouble(lsValeur, CultureInfo.InvariantCulture);
                return lnTensionMax;
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (System.TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new OperationException("L'alimentation n'a pas répondu.", e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de lire la tension maximale du channel " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Retourne l'intensité max du channel visé
        public double LireIntensiteMax()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("CURR?");
                string lsValeur = oPort.ReadLine();
                
                double lnIntensiteMax = Convert.ToDouble(lsValeur, CultureInfo.InvariantCulture);
                return lnIntensiteMax;
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);

            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (System.TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new OperationException("L'alimentation n'a pas répondu.", e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de lire l'intensité maximale du channel " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Retourne la tension sortante du channel visé
        public double LireTensionReelle()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("MEAS:VOLT?");
                string lsValeur = oPort.ReadLine();

                double lnTensionReel = Convert.ToDouble(lsValeur, CultureInfo.InvariantCulture);
                return lnTensionReel;
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (System.TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new OperationException("L'alimentation n'a pas répondu.", e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de lire la tension réelle du channel " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Retourne l'intensité sortante du channel visé
        public double LireIntensiteReelle()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("MEAS:CURR?");
                string lsValeur = oPort.ReadLine();

                double lnIntensiteReel = Convert.ToDouble(lsValeur, CultureInfo.InvariantCulture);
                return lnIntensiteReel;
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }

            }
            catch (System.TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new OperationException("L'alimentation n'a pas répondu.", e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de lire l'intensité réelle du channel " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Modifie la tension et l'intensité maximales
        public void ModifierValeurs(double pnTensionMax, double pnIntensiteMax)
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("INST OUT" + this.iNumChannel);

                //Modification de la tension max et l'intensité max
                oPort.WriteLine("APPLY " + pnTensionMax.ToString(CultureInfo.InvariantCulture) + ", " + pnIntensiteMax.ToString(CultureInfo.InvariantCulture));

                Log loAction = new Log();
                loAction.logWriteAction("Modification des valeurs maximale. Tension : " + pnTensionMax + ", Intensité : " + pnIntensiteMax + "." );
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de modifier les valeurs maximales : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }

        //Retourne l'état du channel visé en format string
        public bool EtatChannel()
        {
            try
            {
                oPort.Open();
                oPort.WriteLine("INST OUT" + this.iNumChannel);
                oPort.WriteLine("OUTP:SEL?");
                string lsEtat = oPort.ReadLine();

                Log loAction = new Log();
                loAction.logWriteAction("Vérification de l'état du channel " + this.iNumChannel + ".");

                if (lsEtat == "1")
                {
                    return true;
                } else {
                    return false;
                }
            }
            catch (System.IO.IOException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new ConnexionException("L'alimentation n'est pas connectée ou n'est pas reconnue.", e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                if (oPort.IsOpen)
                {
                    throw new PortOccupeException("Le port spécifié est déjà occupé.", e);
                } else {
                    throw new Exception(e.Message, e);
                }
            }
            catch (System.TimeoutException e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new OperationException("L'alimentation n'a pas répondu.", e);
            }
            catch (Exception e)
            {
                Log loLogger = new Log();
                loLogger.logWriteException(e);
                throw new Exception("Impossible de vérifier l'état du channel " + this.iNumChannel + " : " + e.Message, e);
            }
            finally
            {
                if (oPort.IsOpen)
                {
                    //Fermeture du canal de communication
                    oPort.Close();
                }
            }
        }
    }
}
