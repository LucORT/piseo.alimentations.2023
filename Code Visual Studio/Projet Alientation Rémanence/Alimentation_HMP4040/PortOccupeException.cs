﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alimentation_HMP4040
{
    public class PortOccupeException : Exception
    {
        public PortOccupeException(string message, Exception e) : base(message, e)
        {

        }
        public PortOccupeException(Exception e) : base("", e)
        {

        }
    }
}
