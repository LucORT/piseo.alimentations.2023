﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Alimentation_HMP4040;

namespace IHMFinal
{
    public partial class IHMFinal : Form
    {
        
        HMP4040 oAlimentation;
        

        public IHMFinal()
        {
            InitializeComponent();

            //Détection et ajout des ports dans la liste déroulante
            
            cbPorts.Items.AddRange(HMP4040.DetecterPorts());
        }

        public void RemiseZeroLabels()
        {
            lTensionR1.Text = "0.000 V";
            lIntensiteR1.Text = "0.0000 A";

            lTensionR2.Text = "0.000 V";
            lIntensiteR2.Text = "0.0000 A";

            lTensionR3.Text = "0.000 V";
            lIntensiteR3.Text = "0.0000 A";

            lTensionR4.Text = "0.000 V";
            lIntensiteR4.Text = "0.0000 A";
        }

        public void Desactiver()
        {
            
            RemiseZeroLabels();

            gbChannel.Enabled = false;
            gbPilotage.Enabled = false;
            gbValeursR.Enabled = false;

            bConnexion.Text = "Test connexion";

            bPortCom.Enabled = true;
            cbPorts.Enabled = true;

            ThreadChannel.Enabled = false;
        }

        private void bConnexion_Click(object sender, EventArgs e)
        {
            bConnexion.Enabled = false;

            if (bConnexion.Text == "Test connexion")
            {
                rtbInfos.Text = "Tentative de connexion...";

                oAlimentation = new HMP4040(cbPorts.Text);

                try
                {
                    oAlimentation.TestConnexion();
                    Verification();

                    rtbInfos.Text = "Connexion réussie";
                    bConnexion.Text = "Changer de port";

                    bPortCom.Enabled = false;
                    cbPorts.Enabled = false;
                } 
                catch (Exception ex)
                {
                    rtbInfos.Text = ex.Message;
                }

            } else 
            {
                rtbInfos.Text = "Veuillez patienter...";
                Desactiver();
            }

            bConnexion.Enabled = true;
        }

        private void cbPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            bConnexion.Enabled = true;
        }

        private void bOnOff_Click(object sender, EventArgs e)
        {
            

            if (bOnOff.Text == "Allumer")
            {
                try
                {
                    rtbInfos.Text = "Activation...";
                    if (cbChannel1.Checked == true)
                    {
                        oAlimentation.Channels[0].Activer();
                        pChannel1.BackColor = Color.Lime;                        

                    }

                    if (cbChannel2.Checked == true)
                    {
                        oAlimentation.Channels[1].Activer();
                        pChannel2.BackColor = Color.Lime;                       

                    }

                    if (cbChannel3.Checked == true)
                    {
                        oAlimentation.Channels[2].Activer();
                        pChannel3.BackColor = Color.Lime;                        

                    }

                    if (cbChannel4.Checked == true)
                    {
                        oAlimentation.Channels[3].Activer();
                        pChannel4.BackColor = Color.Lime;                        

                    }

                    double lnTensionMax1 = Convert.ToDouble(tbTensionMax1.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax1 = Convert.ToDouble(tbIntensiteMax1.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[1 - 1].ModifierValeurs(lnTensionMax1, lnIntensiteMax1);

                    double lnTensionMax2 = Convert.ToDouble(tbTensionMax2.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax2 = Convert.ToDouble(tbIntensiteMax2.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[2 - 1].ModifierValeurs(lnTensionMax2, lnIntensiteMax2);

                    double lnTensionMax3 = Convert.ToDouble(tbTensionMax3.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax3 = Convert.ToDouble(tbIntensiteMax3.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[3 - 1].ModifierValeurs(lnTensionMax3, lnIntensiteMax3);

                    double lnTensionMax4 = Convert.ToDouble(tbTensionMax4.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax4 = Convert.ToDouble(tbIntensiteMax4.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[4 - 1].ModifierValeurs(lnTensionMax4, lnIntensiteMax4);

                    oAlimentation.ActiverSortie();
                    ThreadChannel.Enabled = true;

                    Verification();

                    pSortie.BackColor = Color.Lime;
                    gbChannel.Enabled = false;

                    bOnOff.Text = "Eteindre";
                    bModifierValeurs.Enabled = true;
                    

                    rtbInfos.Text = "Activation terminée.";
                }
                catch (Exception ex)
                {
                    rtbInfos.Text = "Veuillez patienter...";
                    Desactiver();
                    rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
                }
            }
            else
            {
                try
                {
                    rtbInfos.Text = "Désactivation...";
                    oAlimentation.DesactiverToutesSorties();
                    ThreadChannel.Enabled = false;

                    bModifierValeurs.Enabled = false;
                    bModifierValeurs.Text = "Modifier valeurs";
                    bAnnuler.Enabled = false;

                    bOnOff.Text = "Allumer";
                    pChannel1.BackColor = Color.Red;
                    pChannel2.BackColor = Color.Red;
                    pChannel3.BackColor = Color.Red;
                    pChannel4.BackColor = Color.Red;
                    pSortie.BackColor = Color.Red;



                    bModifierValeurs.Enabled = false;
                    bAnnuler.Enabled = false;
                    
                    gbChannel.Enabled = true;
                    RemiseZeroLabels();
                    rtbInfos.Text = "Désactivation terminée...";
                }
                catch (Exception ex)
                {
                    rtbInfos.Text = "Veuillez patienter...";
                    Desactiver();
                    rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
                }
            }
        }

        private void bReset_Click(object sender, EventArgs e)
        {
            try
            {
                rtbInfos.Text = "Réinitialisation en cours";

                oAlimentation.Reset();
                Verification();

                rtbInfos.Text = "Réinitialisation terminé";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = "Veuillez patienter...";
                Desactiver();
                rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
            }
        }

        private void bModifierValeurs_Click(object sender, EventArgs e)
        {
            if(bModifierValeurs.Text == "Modifier valeurs")
            {
                rtbInfos.Text = "Modifiez les valeurs";
                gbChannel.Enabled = true;
                bModifierValeurs.Text = "Appliquer";
                bAnnuler.Enabled = true;
            }

            else if(bModifierValeurs.Text == "Appliquer")
            {
                try
                {
                    rtbInfos.Text = "Application des valeurs...";
                    if (cbChannel1.Checked == true)
                    {
                        oAlimentation.Channels[1-1].Activer();
                        pChannel1.BackColor = Color.Lime;
                    }
                    else
                    {
                        oAlimentation.Channels[1-1].Desactiver();
                        pChannel1.BackColor = Color.Red;
                    }

                    double lnTensionMax1 = Convert.ToDouble(tbTensionMax1.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax1 = Convert.ToDouble(tbIntensiteMax1.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[1 - 1].ModifierValeurs(lnTensionMax1, lnIntensiteMax1);



                    if (cbChannel2.Checked == true)
                    {
                        oAlimentation.Channels[2-1].Activer();
                        pChannel2.BackColor = Color.Lime;
                    }
                    else
                    {
                        oAlimentation.Channels[2-1].Desactiver();
                        pChannel2.BackColor = Color.Red;
                    }

                    double lnTensionMax2 = Convert.ToDouble(tbTensionMax2.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax2 = Convert.ToDouble(tbIntensiteMax2.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[2 - 1].ModifierValeurs(lnTensionMax2, lnIntensiteMax2);



                    if (cbChannel3.Checked == true)
                    {
                        oAlimentation.Channels[3-1].Activer();
                        pChannel3.BackColor = Color.Lime;
                    }
                    else
                    {
                        oAlimentation.Channels[3-1].Desactiver();
                        pChannel3.BackColor = Color.Red;
                    }

                    double lnTensionMax3 = Convert.ToDouble(tbTensionMax3.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax3 = Convert.ToDouble(tbIntensiteMax3.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[3 - 1].ModifierValeurs(lnTensionMax3, lnIntensiteMax3);



                    if (cbChannel4.Checked == true)
                    {
                        oAlimentation.Channels[4-1].Activer();
                        pChannel4.BackColor = Color.Lime;               
                    }
                    else
                    {
                        oAlimentation.Channels[4 - 1].Desactiver();
                        pChannel4.BackColor = Color.Red;
                    }

                    double lnTensionMax4 = Convert.ToDouble(tbTensionMax4.Text, CultureInfo.InvariantCulture);
                    double lnIntensiteMax4 = Convert.ToDouble(tbIntensiteMax4.Text, CultureInfo.InvariantCulture);
                    oAlimentation.Channels[4 - 1].ModifierValeurs(lnTensionMax4, lnIntensiteMax4);



                    double lnTensionMaxLue1 = oAlimentation.Channels[1 - 1].LireTensionMax();
                    double lnIntensiteLue1 = oAlimentation.Channels[1 - 1].LireIntensiteMax();

                    double lnTensionMaxLue2 = oAlimentation.Channels[2 - 1].LireTensionMax();
                    double lnIntensiteLue2 = oAlimentation.Channels[2 - 1].LireIntensiteMax();

                    double lnTensionMaxLue3 = oAlimentation.Channels[3 - 1].LireTensionMax();
                    double lnIntensiteLue3 = oAlimentation.Channels[3 - 1].LireIntensiteMax();

                    double lnTensionMaxLue4 = oAlimentation.Channels[4 - 1].LireTensionMax();
                    double lnIntensiteLue4 = oAlimentation.Channels[4 - 1].LireIntensiteMax();


                    tbTensionMax1.Text = lnTensionMaxLue1.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax1.Text = lnIntensiteLue1.ToString(CultureInfo.InvariantCulture);

                    tbTensionMax2.Text = lnTensionMaxLue2.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax2.Text = lnIntensiteLue2.ToString(CultureInfo.InvariantCulture);

                    tbTensionMax3.Text = lnTensionMaxLue3.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax3.Text = lnIntensiteLue3.ToString(CultureInfo.InvariantCulture);

                    tbTensionMax4.Text = lnTensionMaxLue4.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax4.Text = lnIntensiteLue4.ToString(CultureInfo.InvariantCulture);

                    rtbInfos.Text = "Valeurs appliquées.\n";


                    if (lnTensionMax1 > 32.05 || lnTensionMax2 > 32.05 || lnTensionMax3 > 32.05 || lnTensionMax4 > 32.05)
                    {
                        rtbInfos.Text += "La tension maximale possible est de 32.05 V.\n";
                    }
                    if (lnTensionMaxLue1 < lnTensionMax1)
                    {
                        rtbInfos.Text += "La tension maximale ne peut pas dépasser " + lnTensionMaxLue1 +
                            " V lorsque la valeur de l'intensité vaut " + lnIntensiteLue1 + " A.\n";
                    }
                    if (lnTensionMaxLue2 < lnTensionMax2)
                    {
                        rtbInfos.Text += "La tension maximale ne peut pas dépasser " + lnTensionMaxLue2 +
                            " V lorsque la valeur de l'intensité vaut " + lnIntensiteLue2 + " A.\n";
                    }
                    if (lnTensionMaxLue3 < lnTensionMax3)
                    {
                        rtbInfos.Text += "La tension maximale ne peut pas dépasser " + lnTensionMaxLue3 +
                            " V lorsque la valeur de l'intensité vaut " + lnIntensiteLue3 + " A.\n";
                    }
                    if (lnTensionMaxLue4 < lnTensionMax4)
                    {
                        rtbInfos.Text += "La tension maximale ne peut pas dépasser " + lnTensionMaxLue4 +
                            " V lorsque la valeur de l'intensité vaut " + lnIntensiteLue4 + " A.\n";
                    }


                    if (lnIntensiteMax1 > 10 || lnIntensiteMax2 > 10 || lnIntensiteMax3 > 10 || lnIntensiteMax4 > 10)
                    {
                        rtbInfos.Text += "L'intensité maximale possible est de 10 ampères.\n";
                    }
                    if (lnIntensiteLue1 < lnIntensiteMax1)
                    {
                        rtbInfos.Text += "L'intensité maximale ne peut pas dépasser " + lnIntensiteLue1 +
                            " A lorsque la valeur de la tension vaut " + lnTensionMaxLue1 + " A.\n";
                    }
                    if (lnIntensiteLue2 < lnIntensiteMax2)
                    {
                        rtbInfos.Text += "L'intensité maximale ne peut pas dépasser " + lnIntensiteLue2 +
                            " A lorsque la valeur de la tension vaut " + lnTensionMaxLue2 + " A.\n";
                    }
                    if (lnIntensiteLue3 < lnIntensiteMax3)
                    {
                        rtbInfos.Text += "L'intensité maximale ne peut pas dépasser " + lnIntensiteLue3 +
                            " A lorsque la valeur de la tension vaut " + lnTensionMaxLue3 + " A.\n";
                    }
                    if (lnIntensiteLue4 < lnIntensiteMax4)
                    {
                        rtbInfos.Text += "L'intensité maximale ne peut pas dépasser " + lnIntensiteLue4 +
                            " A lorsque la valeur de la tension vaut " + lnTensionMaxLue4 + " A.\n";
                    }



                    tbTensionMax1.Text = lnTensionMaxLue1.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax1.Text = lnIntensiteLue1.ToString(CultureInfo.InvariantCulture);

                    tbTensionMax2.Text = lnTensionMaxLue2.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax2.Text = lnIntensiteLue2.ToString(CultureInfo.InvariantCulture);

                    tbTensionMax3.Text = lnTensionMaxLue3.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax3.Text = lnIntensiteLue3.ToString(CultureInfo.InvariantCulture);

                    tbTensionMax4.Text = lnTensionMaxLue4.ToString(CultureInfo.InvariantCulture);
                    tbIntensiteMax4.Text = lnIntensiteLue4.ToString(CultureInfo.InvariantCulture);


                    bModifierValeurs.Text = "Modifier valeurs";
                    bAnnuler.Enabled = false;
                    gbChannel.Enabled = false;

                }
                catch (Exception ex)
                {
                    rtbInfos.Text = "Veuillez patienter...";
                    Desactiver();
                    rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
                }
            }
            
                
        }

        private void bPortCom_Click(object sender, EventArgs e)
        {
            bConnexion.Enabled = false;
            cbPorts.Items.Clear();

            
            cbPorts.Items.AddRange(HMP4040.DetecterPorts());

            cbPorts.Text = "";
        }

        private void cbPorts_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void ThreadChannel_Tick(object sender, EventArgs e)
        {
            try
            {
                if (cbChannel1.Checked == true)
                {
                    lTensionR1.Text = oAlimentation.Channels[1 - 1].LireTensionReelle().ToString(CultureInfo.InvariantCulture) + " V";
                    lIntensiteR1.Text = oAlimentation.Channels[1 - 1].LireIntensiteReelle().ToString(CultureInfo.InvariantCulture) + " A";
                }

                if (cbChannel2.Checked == true)
                {
                    lTensionR2.Text = oAlimentation.Channels[2 - 1].LireTensionReelle().ToString(CultureInfo.InvariantCulture) + " V";
                    lIntensiteR2.Text = oAlimentation.Channels[2 - 1].LireIntensiteReelle().ToString(CultureInfo.InvariantCulture) + " A";
                }

                if (cbChannel3.Checked == true)
                {
                    lTensionR3.Text = oAlimentation.Channels[3 - 1].LireTensionReelle().ToString(CultureInfo.InvariantCulture) + " V";
                    lIntensiteR3.Text = oAlimentation.Channels[3 - 1].LireIntensiteReelle().ToString(CultureInfo.InvariantCulture) + " A";
                }

                if (cbChannel4.Checked == true)
                {
                    lTensionR4.Text = oAlimentation.Channels[4 - 1].LireTensionReelle().ToString(CultureInfo.InvariantCulture) + " V";
                    lIntensiteR4.Text = oAlimentation.Channels[4 - 1].LireIntensiteReelle().ToString(CultureInfo.InvariantCulture) + " A";
                }
            }
            catch (Exception ex)
            {
                rtbInfos.Text = "Veuillez patienter...";
                Desactiver();
                rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
            }

        }

        public void Verification()
        {
            try
            {
                gbChannel.Enabled = false;
                gbPilotage.Enabled = false;
                gbValeursR.Enabled = false;

                

                if (oAlimentation.Channels[1-1].EtatChannel() == true)
                {
                    cbChannel1.Checked = true;
                    pChannel1.BackColor = Color.Lime;
                    
                }
                else
                {
                    cbChannel1.Checked = false;
                    pChannel1.BackColor = Color.Red;

                }

                if (oAlimentation.Channels[2 - 1].EtatChannel() == true)
                {
                    cbChannel2.Checked = true;
                    pChannel2.BackColor = Color.Lime;

                }
                else
                {
                    cbChannel2.Checked = false;
                    pChannel2.BackColor = Color.Red;

                }

                if (oAlimentation.Channels[3 - 1].EtatChannel() == true)
                {
                    cbChannel3.Checked = true;
                    pChannel3.BackColor = Color.Lime;

                }
                else
                {
                    cbChannel3.Checked = false;
                    pChannel3.BackColor = Color.Red;

                }

                if (oAlimentation.Channels[4-1].EtatChannel() == true)
                {
                    cbChannel4.Checked = true;
                    pChannel4.BackColor = Color.Lime;

                }
                else
                {
                    cbChannel4.Checked = false;
                    pChannel4.BackColor = Color.Red;

                }

                tbTensionMax1.Text = oAlimentation.Channels[1 - 1].LireTensionMax().ToString(CultureInfo.InvariantCulture);
                tbIntensiteMax1.Text = oAlimentation.Channels[1 - 1].LireIntensiteMax().ToString(CultureInfo.InvariantCulture);

                tbTensionMax2.Text = oAlimentation.Channels[2 - 1].LireTensionMax().ToString(CultureInfo.InvariantCulture);
                tbIntensiteMax2.Text = oAlimentation.Channels[2 - 1].LireIntensiteMax().ToString(CultureInfo.InvariantCulture);

                tbTensionMax3.Text = oAlimentation.Channels[3 - 1].LireTensionMax().ToString(CultureInfo.InvariantCulture);
                tbIntensiteMax3.Text = oAlimentation.Channels[3 - 1].LireIntensiteMax().ToString(CultureInfo.InvariantCulture);

                tbTensionMax4.Text = oAlimentation.Channels[4 - 1].LireTensionMax().ToString(CultureInfo.InvariantCulture);
                tbIntensiteMax4.Text = oAlimentation.Channels[4 - 1].LireIntensiteMax().ToString(CultureInfo.InvariantCulture);


                if (oAlimentation.EtatSortie() == true)
                {
                    bOnOff.Text = "Eteindre";
                    bModifierValeurs.Enabled = true;
                    bAnnuler.Enabled = true;
                    pSortie.BackColor = Color.Lime;
                    gbChannel.Enabled = false;
                    ThreadChannel.Enabled = true;
                }
                else
                {
                    bOnOff.Text = "Allumer";
                    bModifierValeurs.Enabled = false;
                    bAnnuler.Enabled = false;
                    pSortie.BackColor = Color.Red;
                    gbChannel.Enabled = true;
                    ThreadChannel.Enabled = false;
                }

                
                gbPilotage.Enabled = true;
                gbValeursR.Enabled = true;
            }
            catch (Exception ex)
            {
                rtbInfos.Text = "Veuillez patienter...";
                Desactiver();
                rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
            }
        }

        private void bAnnuler_Click(object sender, EventArgs e)
        {
            try
            {
                bAnnuler.Enabled = false;

                rtbInfos.Text = "Annulation des valeurs...";
                Verification();
                gbChannel.Enabled = false;
                bModifierValeurs.Text = "Modifier valeurs";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = "Veuillez patienter...";
                Desactiver();
                rtbInfos.Text = ex.Message + "\nSélectionnez un port.";
            }

        }

        private void tbTensionMax1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbIntensiteMax1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbTensionMax2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbIntensiteMax2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbTensionMax3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbIntensiteMax3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbTensionMax4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbIntensiteMax4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }


    }
}
