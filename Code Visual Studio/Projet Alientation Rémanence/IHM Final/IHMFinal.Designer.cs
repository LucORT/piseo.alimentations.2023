﻿
namespace IHMFinal
{
    partial class IHMFinal
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbConnexion = new System.Windows.Forms.GroupBox();
            this.bPortCom = new System.Windows.Forms.Button();
            this.cbPorts = new System.Windows.Forms.ComboBox();
            this.bConnexion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbChannel = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pChannel4 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.pChannel3 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.pChannel2 = new System.Windows.Forms.Panel();
            this.tbTensionMax4 = new System.Windows.Forms.TextBox();
            this.pChannel1 = new System.Windows.Forms.Panel();
            this.tbIntensiteMax4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbTensionMax3 = new System.Windows.Forms.TextBox();
            this.tbIntensiteMax3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbTensionMax2 = new System.Windows.Forms.TextBox();
            this.tbIntensiteMax2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbChannel3 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbChannel2 = new System.Windows.Forms.CheckBox();
            this.cbChannel4 = new System.Windows.Forms.CheckBox();
            this.cbChannel1 = new System.Windows.Forms.CheckBox();
            this.tbTensionMax1 = new System.Windows.Forms.TextBox();
            this.tbIntensiteMax1 = new System.Windows.Forms.TextBox();
            this.lTensionR4 = new System.Windows.Forms.Label();
            this.lIntensiteR4 = new System.Windows.Forms.Label();
            this.lTensionR3 = new System.Windows.Forms.Label();
            this.lIntensiteR3 = new System.Windows.Forms.Label();
            this.lTensionR2 = new System.Windows.Forms.Label();
            this.lIntensiteR2 = new System.Windows.Forms.Label();
            this.lTensionR1 = new System.Windows.Forms.Label();
            this.lIntensiteR1 = new System.Windows.Forms.Label();
            this.bOnOff = new System.Windows.Forms.Button();
            this.bReset = new System.Windows.Forms.Button();
            this.gbInfos = new System.Windows.Forms.GroupBox();
            this.rtbInfos = new System.Windows.Forms.RichTextBox();
            this.ThreadChannel = new System.Windows.Forms.Timer(this.components);
            this.PopupInfos = new System.Windows.Forms.ToolTip(this.components);
            this.bModifierValeurs = new System.Windows.Forms.Button();
            this.bAnnuler = new System.Windows.Forms.Button();
            this.gbValeursR = new System.Windows.Forms.GroupBox();
            this.gbPilotage = new System.Windows.Forms.GroupBox();
            this.pSortie = new System.Windows.Forms.Panel();
            this.gbConnexion.SuspendLayout();
            this.gbChannel.SuspendLayout();
            this.gbInfos.SuspendLayout();
            this.gbValeursR.SuspendLayout();
            this.gbPilotage.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbConnexion
            // 
            this.gbConnexion.Controls.Add(this.bPortCom);
            this.gbConnexion.Controls.Add(this.cbPorts);
            this.gbConnexion.Controls.Add(this.bConnexion);
            this.gbConnexion.Controls.Add(this.label1);
            this.gbConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbConnexion.Location = new System.Drawing.Point(16, 14);
            this.gbConnexion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbConnexion.Name = "gbConnexion";
            this.gbConnexion.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbConnexion.Size = new System.Drawing.Size(268, 146);
            this.gbConnexion.TabIndex = 0;
            this.gbConnexion.TabStop = false;
            this.gbConnexion.Text = "Connexion";
            // 
            // bPortCom
            // 
            this.bPortCom.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPortCom.Location = new System.Drawing.Point(195, 30);
            this.bPortCom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bPortCom.Name = "bPortCom";
            this.bPortCom.Size = new System.Drawing.Size(51, 48);
            this.bPortCom.TabIndex = 4;
            this.bPortCom.Text = "🔄";
            this.PopupInfos.SetToolTip(this.bPortCom, "Actualiser les ports");
            this.bPortCom.UseVisualStyleBackColor = true;
            this.bPortCom.Click += new System.EventHandler(this.bPortCom_Click);
            // 
            // cbPorts
            // 
            this.cbPorts.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPorts.FormattingEnabled = true;
            this.cbPorts.Location = new System.Drawing.Point(73, 37);
            this.cbPorts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbPorts.Name = "cbPorts";
            this.cbPorts.Size = new System.Drawing.Size(115, 34);
            this.cbPorts.TabIndex = 3;
            this.cbPorts.SelectedIndexChanged += new System.EventHandler(this.cbPorts_SelectedIndexChanged);
            this.cbPorts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbPorts_KeyDown);
            // 
            // bConnexion
            // 
            this.bConnexion.AccessibleDescription = "";
            this.bConnexion.Enabled = false;
            this.bConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bConnexion.Location = new System.Drawing.Point(11, 86);
            this.bConnexion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bConnexion.Name = "bConnexion";
            this.bConnexion.Size = new System.Drawing.Size(235, 43);
            this.bConnexion.TabIndex = 2;
            this.bConnexion.Tag = "";
            this.bConnexion.Text = "Test connexion";
            this.PopupInfos.SetToolTip(this.bConnexion, "Tester la connexion");
            this.bConnexion.UseVisualStyleBackColor = true;
            this.bConnexion.Click += new System.EventHandler(this.bConnexion_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port :";
            // 
            // gbChannel
            // 
            this.gbChannel.Controls.Add(this.label18);
            this.gbChannel.Controls.Add(this.pChannel4);
            this.gbChannel.Controls.Add(this.label16);
            this.gbChannel.Controls.Add(this.pChannel3);
            this.gbChannel.Controls.Add(this.label17);
            this.gbChannel.Controls.Add(this.pChannel2);
            this.gbChannel.Controls.Add(this.tbTensionMax4);
            this.gbChannel.Controls.Add(this.pChannel1);
            this.gbChannel.Controls.Add(this.tbIntensiteMax4);
            this.gbChannel.Controls.Add(this.label12);
            this.gbChannel.Controls.Add(this.label13);
            this.gbChannel.Controls.Add(this.tbTensionMax3);
            this.gbChannel.Controls.Add(this.tbIntensiteMax3);
            this.gbChannel.Controls.Add(this.label8);
            this.gbChannel.Controls.Add(this.label9);
            this.gbChannel.Controls.Add(this.tbTensionMax2);
            this.gbChannel.Controls.Add(this.tbIntensiteMax2);
            this.gbChannel.Controls.Add(this.label4);
            this.gbChannel.Controls.Add(this.label2);
            this.gbChannel.Controls.Add(this.cbChannel3);
            this.gbChannel.Controls.Add(this.label3);
            this.gbChannel.Controls.Add(this.cbChannel2);
            this.gbChannel.Controls.Add(this.cbChannel4);
            this.gbChannel.Controls.Add(this.cbChannel1);
            this.gbChannel.Controls.Add(this.tbTensionMax1);
            this.gbChannel.Controls.Add(this.tbIntensiteMax1);
            this.gbChannel.Enabled = false;
            this.gbChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbChannel.Location = new System.Drawing.Point(292, 14);
            this.gbChannel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbChannel.Name = "gbChannel";
            this.gbChannel.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbChannel.Size = new System.Drawing.Size(451, 454);
            this.gbChannel.TabIndex = 1;
            this.gbChannel.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, -2);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 25);
            this.label18.TabIndex = 31;
            this.label18.Text = "Channel";
            // 
            // pChannel4
            // 
            this.pChannel4.BackColor = System.Drawing.Color.Red;
            this.pChannel4.Location = new System.Drawing.Point(371, 383);
            this.pChannel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pChannel4.Name = "pChannel4";
            this.pChannel4.Size = new System.Drawing.Size(29, 28);
            this.pChannel4.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(256, 362);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 26);
            this.label16.TabIndex = 29;
            this.label16.Text = "V";
            // 
            // pChannel3
            // 
            this.pChannel3.BackColor = System.Drawing.Color.Red;
            this.pChannel3.Location = new System.Drawing.Point(371, 283);
            this.pChannel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pChannel3.Name = "pChannel3";
            this.pChannel3.Size = new System.Drawing.Size(29, 28);
            this.pChannel3.TabIndex = 30;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(256, 402);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 26);
            this.label17.TabIndex = 30;
            this.label17.Text = "A";
            // 
            // pChannel2
            // 
            this.pChannel2.BackColor = System.Drawing.Color.Red;
            this.pChannel2.Location = new System.Drawing.Point(371, 177);
            this.pChannel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pChannel2.Name = "pChannel2";
            this.pChannel2.Size = new System.Drawing.Size(29, 28);
            this.pChannel2.TabIndex = 30;
            // 
            // tbTensionMax4
            // 
            this.tbTensionMax4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTensionMax4.Location = new System.Drawing.Point(149, 356);
            this.tbTensionMax4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTensionMax4.Name = "tbTensionMax4";
            this.tbTensionMax4.Size = new System.Drawing.Size(89, 32);
            this.tbTensionMax4.TabIndex = 26;
            this.tbTensionMax4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTensionMax4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTensionMax4_KeyPress);
            // 
            // pChannel1
            // 
            this.pChannel1.BackColor = System.Drawing.Color.Red;
            this.pChannel1.Location = new System.Drawing.Point(371, 82);
            this.pChannel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pChannel1.Name = "pChannel1";
            this.pChannel1.Size = new System.Drawing.Size(29, 28);
            this.pChannel1.TabIndex = 29;
            // 
            // tbIntensiteMax4
            // 
            this.tbIntensiteMax4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntensiteMax4.Location = new System.Drawing.Point(149, 396);
            this.tbIntensiteMax4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbIntensiteMax4.Name = "tbIntensiteMax4";
            this.tbIntensiteMax4.Size = new System.Drawing.Size(89, 32);
            this.tbIntensiteMax4.TabIndex = 28;
            this.tbIntensiteMax4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbIntensiteMax4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntensiteMax4_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(256, 260);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 26);
            this.label12.TabIndex = 23;
            this.label12.Text = "V";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(256, 300);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 26);
            this.label13.TabIndex = 24;
            this.label13.Text = "A";
            // 
            // tbTensionMax3
            // 
            this.tbTensionMax3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTensionMax3.Location = new System.Drawing.Point(149, 254);
            this.tbTensionMax3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTensionMax3.Name = "tbTensionMax3";
            this.tbTensionMax3.Size = new System.Drawing.Size(89, 32);
            this.tbTensionMax3.TabIndex = 20;
            this.tbTensionMax3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTensionMax3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTensionMax3_KeyPress);
            // 
            // tbIntensiteMax3
            // 
            this.tbIntensiteMax3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntensiteMax3.Location = new System.Drawing.Point(149, 294);
            this.tbIntensiteMax3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbIntensiteMax3.Name = "tbIntensiteMax3";
            this.tbIntensiteMax3.Size = new System.Drawing.Size(89, 32);
            this.tbIntensiteMax3.TabIndex = 22;
            this.tbIntensiteMax3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbIntensiteMax3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntensiteMax3_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(256, 156);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 26);
            this.label8.TabIndex = 17;
            this.label8.Text = "V";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(256, 197);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 26);
            this.label9.TabIndex = 18;
            this.label9.Text = "A";
            // 
            // tbTensionMax2
            // 
            this.tbTensionMax2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTensionMax2.Location = new System.Drawing.Point(149, 150);
            this.tbTensionMax2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTensionMax2.Name = "tbTensionMax2";
            this.tbTensionMax2.Size = new System.Drawing.Size(89, 32);
            this.tbTensionMax2.TabIndex = 14;
            this.tbTensionMax2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTensionMax2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTensionMax2_KeyPress);
            // 
            // tbIntensiteMax2
            // 
            this.tbIntensiteMax2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntensiteMax2.Location = new System.Drawing.Point(149, 191);
            this.tbIntensiteMax2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbIntensiteMax2.Name = "tbIntensiteMax2";
            this.tbIntensiteMax2.Size = new System.Drawing.Size(89, 32);
            this.tbIntensiteMax2.TabIndex = 16;
            this.tbIntensiteMax2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbIntensiteMax2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntensiteMax2_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(144, -2);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Valeurs maximales";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(256, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "V";
            // 
            // cbChannel3
            // 
            this.cbChannel3.AutoSize = true;
            this.cbChannel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannel3.Location = new System.Drawing.Point(33, 262);
            this.cbChannel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbChannel3.Name = "cbChannel3";
            this.cbChannel3.Size = new System.Drawing.Size(45, 29);
            this.cbChannel3.TabIndex = 10;
            this.cbChannel3.Text = "3";
            this.cbChannel3.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(256, 91);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "A";
            // 
            // cbChannel2
            // 
            this.cbChannel2.AutoSize = true;
            this.cbChannel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannel2.Location = new System.Drawing.Point(33, 156);
            this.cbChannel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbChannel2.Name = "cbChannel2";
            this.cbChannel2.Size = new System.Drawing.Size(45, 29);
            this.cbChannel2.TabIndex = 9;
            this.cbChannel2.Text = "2";
            this.cbChannel2.UseVisualStyleBackColor = true;
            // 
            // cbChannel4
            // 
            this.cbChannel4.AutoSize = true;
            this.cbChannel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannel4.Location = new System.Drawing.Point(33, 362);
            this.cbChannel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbChannel4.Name = "cbChannel4";
            this.cbChannel4.Size = new System.Drawing.Size(45, 29);
            this.cbChannel4.TabIndex = 8;
            this.cbChannel4.Text = "4";
            this.cbChannel4.UseVisualStyleBackColor = true;
            // 
            // cbChannel1
            // 
            this.cbChannel1.AutoSize = true;
            this.cbChannel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannel1.Location = new System.Drawing.Point(33, 50);
            this.cbChannel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbChannel1.Name = "cbChannel1";
            this.cbChannel1.Size = new System.Drawing.Size(45, 29);
            this.cbChannel1.TabIndex = 7;
            this.cbChannel1.Text = "1";
            this.cbChannel1.UseVisualStyleBackColor = true;
            // 
            // tbTensionMax1
            // 
            this.tbTensionMax1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTensionMax1.Location = new System.Drawing.Point(149, 44);
            this.tbTensionMax1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTensionMax1.Name = "tbTensionMax1";
            this.tbTensionMax1.Size = new System.Drawing.Size(89, 32);
            this.tbTensionMax1.TabIndex = 0;
            this.tbTensionMax1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTensionMax1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTensionMax1_KeyPress);
            // 
            // tbIntensiteMax1
            // 
            this.tbIntensiteMax1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntensiteMax1.Location = new System.Drawing.Point(149, 85);
            this.tbIntensiteMax1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbIntensiteMax1.Name = "tbIntensiteMax1";
            this.tbIntensiteMax1.Size = new System.Drawing.Size(89, 32);
            this.tbIntensiteMax1.TabIndex = 1;
            this.tbIntensiteMax1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbIntensiteMax1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntensiteMax1_KeyPress);
            // 
            // lTensionR4
            // 
            this.lTensionR4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lTensionR4.AutoSize = true;
            this.lTensionR4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTensionR4.Location = new System.Drawing.Point(24, 361);
            this.lTensionR4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTensionR4.Name = "lTensionR4";
            this.lTensionR4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lTensionR4.Size = new System.Drawing.Size(87, 26);
            this.lTensionR4.TabIndex = 25;
            this.lTensionR4.Text = "0.000 V";
            this.lTensionR4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lIntensiteR4
            // 
            this.lIntensiteR4.AutoSize = true;
            this.lIntensiteR4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIntensiteR4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lIntensiteR4.Location = new System.Drawing.Point(24, 393);
            this.lIntensiteR4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lIntensiteR4.Name = "lIntensiteR4";
            this.lIntensiteR4.Size = new System.Drawing.Size(99, 26);
            this.lIntensiteR4.TabIndex = 27;
            this.lIntensiteR4.Text = "0.0000 A";
            this.lIntensiteR4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lTensionR3
            // 
            this.lTensionR3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lTensionR3.AutoSize = true;
            this.lTensionR3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTensionR3.Location = new System.Drawing.Point(24, 261);
            this.lTensionR3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTensionR3.Name = "lTensionR3";
            this.lTensionR3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lTensionR3.Size = new System.Drawing.Size(87, 26);
            this.lTensionR3.TabIndex = 19;
            this.lTensionR3.Text = "0.000 V";
            this.lTensionR3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lIntensiteR3
            // 
            this.lIntensiteR3.AutoSize = true;
            this.lIntensiteR3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIntensiteR3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lIntensiteR3.Location = new System.Drawing.Point(24, 290);
            this.lIntensiteR3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lIntensiteR3.Name = "lIntensiteR3";
            this.lIntensiteR3.Size = new System.Drawing.Size(99, 26);
            this.lIntensiteR3.TabIndex = 21;
            this.lIntensiteR3.Text = "0.0000 A";
            this.lIntensiteR3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lTensionR2
            // 
            this.lTensionR2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lTensionR2.AutoSize = true;
            this.lTensionR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTensionR2.Location = new System.Drawing.Point(24, 155);
            this.lTensionR2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTensionR2.Name = "lTensionR2";
            this.lTensionR2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lTensionR2.Size = new System.Drawing.Size(87, 26);
            this.lTensionR2.TabIndex = 13;
            this.lTensionR2.Text = "0.000 V";
            this.lTensionR2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lIntensiteR2
            // 
            this.lIntensiteR2.AutoSize = true;
            this.lIntensiteR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIntensiteR2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lIntensiteR2.Location = new System.Drawing.Point(24, 187);
            this.lIntensiteR2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lIntensiteR2.Name = "lIntensiteR2";
            this.lIntensiteR2.Size = new System.Drawing.Size(99, 26);
            this.lIntensiteR2.TabIndex = 15;
            this.lIntensiteR2.Text = "0.0000 A";
            this.lIntensiteR2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lTensionR1
            // 
            this.lTensionR1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lTensionR1.AutoSize = true;
            this.lTensionR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTensionR1.Location = new System.Drawing.Point(24, 49);
            this.lTensionR1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTensionR1.Name = "lTensionR1";
            this.lTensionR1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lTensionR1.Size = new System.Drawing.Size(87, 26);
            this.lTensionR1.TabIndex = 0;
            this.lTensionR1.Text = "0.000 V";
            this.lTensionR1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lIntensiteR1
            // 
            this.lIntensiteR1.AutoSize = true;
            this.lIntensiteR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIntensiteR1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lIntensiteR1.Location = new System.Drawing.Point(24, 81);
            this.lIntensiteR1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lIntensiteR1.Name = "lIntensiteR1";
            this.lIntensiteR1.Size = new System.Drawing.Size(99, 26);
            this.lIntensiteR1.TabIndex = 1;
            this.lIntensiteR1.Text = "0.0000 A";
            this.lIntensiteR1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bOnOff
            // 
            this.bOnOff.AccessibleDescription = "ttttt";
            this.bOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOnOff.Location = new System.Drawing.Point(21, 25);
            this.bOnOff.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bOnOff.Name = "bOnOff";
            this.bOnOff.Size = new System.Drawing.Size(127, 44);
            this.bOnOff.TabIndex = 1;
            this.bOnOff.Text = "Allumer";
            this.PopupInfos.SetToolTip(this.bOnOff, "Allumer ou Eteindre le Channel");
            this.bOnOff.UseVisualStyleBackColor = true;
            this.bOnOff.Click += new System.EventHandler(this.bOnOff_Click);
            // 
            // bReset
            // 
            this.bReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReset.Location = new System.Drawing.Point(21, 76);
            this.bReset.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(127, 44);
            this.bReset.TabIndex = 2;
            this.bReset.Text = "Reset";
            this.PopupInfos.SetToolTip(this.bReset, "Réinitialiser l\'Hameg");
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // gbInfos
            // 
            this.gbInfos.Controls.Add(this.rtbInfos);
            this.gbInfos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbInfos.Location = new System.Drawing.Point(16, 167);
            this.gbInfos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbInfos.Name = "gbInfos";
            this.gbInfos.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbInfos.Size = new System.Drawing.Size(268, 449);
            this.gbInfos.TabIndex = 4;
            this.gbInfos.TabStop = false;
            this.gbInfos.Text = "Informations";
            // 
            // rtbInfos
            // 
            this.rtbInfos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInfos.Location = new System.Drawing.Point(7, 32);
            this.rtbInfos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rtbInfos.Name = "rtbInfos";
            this.rtbInfos.ReadOnly = true;
            this.rtbInfos.Size = new System.Drawing.Size(255, 409);
            this.rtbInfos.TabIndex = 0;
            this.rtbInfos.Text = "Sélectionnez un port.";
            // 
            // ThreadChannel
            // 
            this.ThreadChannel.Interval = 1000;
            this.ThreadChannel.Tick += new System.EventHandler(this.ThreadChannel_Tick);
            // 
            // bModifierValeurs
            // 
            this.bModifierValeurs.Enabled = false;
            this.bModifierValeurs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bModifierValeurs.Location = new System.Drawing.Point(156, 26);
            this.bModifierValeurs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bModifierValeurs.Name = "bModifierValeurs";
            this.bModifierValeurs.Size = new System.Drawing.Size(207, 43);
            this.bModifierValeurs.TabIndex = 2;
            this.bModifierValeurs.Text = "Modifier valeurs";
            this.PopupInfos.SetToolTip(this.bModifierValeurs, "Modifier les valeurs max");
            this.bModifierValeurs.UseVisualStyleBackColor = true;
            this.bModifierValeurs.Click += new System.EventHandler(this.bModifierValeurs_Click);
            // 
            // bAnnuler
            // 
            this.bAnnuler.Enabled = false;
            this.bAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bAnnuler.Location = new System.Drawing.Point(156, 76);
            this.bAnnuler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bAnnuler.Name = "bAnnuler";
            this.bAnnuler.Size = new System.Drawing.Size(207, 44);
            this.bAnnuler.TabIndex = 3;
            this.bAnnuler.Text = "Annuler";
            this.PopupInfos.SetToolTip(this.bAnnuler, "Modifier les valeurs max");
            this.bAnnuler.UseVisualStyleBackColor = true;
            this.bAnnuler.Click += new System.EventHandler(this.bAnnuler_Click);
            // 
            // gbValeursR
            // 
            this.gbValeursR.Controls.Add(this.lTensionR1);
            this.gbValeursR.Controls.Add(this.lTensionR2);
            this.gbValeursR.Controls.Add(this.lTensionR4);
            this.gbValeursR.Controls.Add(this.lIntensiteR2);
            this.gbValeursR.Controls.Add(this.lIntensiteR1);
            this.gbValeursR.Controls.Add(this.lIntensiteR4);
            this.gbValeursR.Controls.Add(this.lIntensiteR3);
            this.gbValeursR.Controls.Add(this.lTensionR3);
            this.gbValeursR.Enabled = false;
            this.gbValeursR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValeursR.Location = new System.Drawing.Point(763, 15);
            this.gbValeursR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbValeursR.Name = "gbValeursR";
            this.gbValeursR.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbValeursR.Size = new System.Drawing.Size(173, 453);
            this.gbValeursR.TabIndex = 28;
            this.gbValeursR.TabStop = false;
            this.gbValeursR.Text = "Valeurs réelles";
            // 
            // gbPilotage
            // 
            this.gbPilotage.Controls.Add(this.pSortie);
            this.gbPilotage.Controls.Add(this.bAnnuler);
            this.gbPilotage.Controls.Add(this.bReset);
            this.gbPilotage.Controls.Add(this.bModifierValeurs);
            this.gbPilotage.Controls.Add(this.bOnOff);
            this.gbPilotage.Enabled = false;
            this.gbPilotage.Location = new System.Drawing.Point(292, 476);
            this.gbPilotage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPilotage.Name = "gbPilotage";
            this.gbPilotage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPilotage.Size = new System.Drawing.Size(532, 140);
            this.gbPilotage.TabIndex = 31;
            this.gbPilotage.TabStop = false;
            // 
            // pSortie
            // 
            this.pSortie.BackColor = System.Drawing.Color.Red;
            this.pSortie.Location = new System.Drawing.Point(399, 26);
            this.pSortie.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pSortie.Name = "pSortie";
            this.pSortie.Size = new System.Drawing.Size(105, 95);
            this.pSortie.TabIndex = 31;
            // 
            // IHMFinal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 623);
            this.Controls.Add(this.gbPilotage);
            this.Controls.Add(this.gbValeursR);
            this.Controls.Add(this.gbInfos);
            this.Controls.Add(this.gbChannel);
            this.Controls.Add(this.gbConnexion);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "IHMFinal";
            this.Text = "IHM de test";
            this.gbConnexion.ResumeLayout(false);
            this.gbConnexion.PerformLayout();
            this.gbChannel.ResumeLayout(false);
            this.gbChannel.PerformLayout();
            this.gbInfos.ResumeLayout(false);
            this.gbValeursR.ResumeLayout(false);
            this.gbValeursR.PerformLayout();
            this.gbPilotage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbConnexion;
        private System.Windows.Forms.Button bConnexion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbChannel;
        private System.Windows.Forms.Button bOnOff;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Label lIntensiteR1;
        private System.Windows.Forms.Label lTensionR1;
        private System.Windows.Forms.TextBox tbIntensiteMax1;
        private System.Windows.Forms.TextBox tbTensionMax1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbInfos;
        private System.Windows.Forms.RichTextBox rtbInfos;
        private System.Windows.Forms.Timer ThreadChannel;
        private System.Windows.Forms.ComboBox cbPorts;
        private System.Windows.Forms.Button bPortCom;
        private System.Windows.Forms.ToolTip PopupInfos;
        private System.Windows.Forms.CheckBox cbChannel1;
        private System.Windows.Forms.CheckBox cbChannel3;
        private System.Windows.Forms.CheckBox cbChannel2;
        private System.Windows.Forms.CheckBox cbChannel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lTensionR4;
        private System.Windows.Forms.Label lIntensiteR4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbTensionMax4;
        private System.Windows.Forms.TextBox tbIntensiteMax4;
        private System.Windows.Forms.Label lTensionR3;
        private System.Windows.Forms.Label lIntensiteR3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbTensionMax3;
        private System.Windows.Forms.TextBox tbIntensiteMax3;
        private System.Windows.Forms.Label lTensionR2;
        private System.Windows.Forms.Label lIntensiteR2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbTensionMax2;
        private System.Windows.Forms.TextBox tbIntensiteMax2;
        private System.Windows.Forms.Button bModifierValeurs;
        private System.Windows.Forms.GroupBox gbValeursR;
        private System.Windows.Forms.Panel pChannel1;
        private System.Windows.Forms.Panel pChannel4;
        private System.Windows.Forms.Panel pChannel3;
        private System.Windows.Forms.Panel pChannel2;
        private System.Windows.Forms.GroupBox gbPilotage;
        private System.Windows.Forms.Button bAnnuler;
        private System.Windows.Forms.Panel pSortie;
    }
}

