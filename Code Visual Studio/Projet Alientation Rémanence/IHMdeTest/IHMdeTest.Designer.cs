﻿
namespace IHMdeTest
{
    partial class IHMdeTest
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbConnexion = new System.Windows.Forms.GroupBox();
            this.bPortCom = new System.Windows.Forms.Button();
            this.cbPorts = new System.Windows.Forms.ComboBox();
            this.bConnexion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbChannel = new System.Windows.Forms.GroupBox();
            this.rbCH3 = new System.Windows.Forms.RadioButton();
            this.rbCH4 = new System.Windows.Forms.RadioButton();
            this.rbCH2 = new System.Windows.Forms.RadioButton();
            this.rbCH1 = new System.Windows.Forms.RadioButton();
            this.bOnOff = new System.Windows.Forms.Button();
            this.pEtatChannel = new System.Windows.Forms.Panel();
            this.bReset = new System.Windows.Forms.Button();
            this.gbValeursR = new System.Windows.Forms.GroupBox();
            this.lIntensiteR = new System.Windows.Forms.Label();
            this.lTensionR = new System.Windows.Forms.Label();
            this.gbValeursMax = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bModifierValeurs = new System.Windows.Forms.Button();
            this.tbIntensiteMax = new System.Windows.Forms.TextBox();
            this.tbTensionMax = new System.Windows.Forms.TextBox();
            this.gbInfos = new System.Windows.Forms.GroupBox();
            this.rtbInfos = new System.Windows.Forms.RichTextBox();
            this.ThreadChannel = new System.Windows.Forms.Timer(this.components);
            this.PopupInfos = new System.Windows.Forms.ToolTip(this.components);
            this.gbReset = new System.Windows.Forms.GroupBox();
            this.gbConnexion.SuspendLayout();
            this.gbChannel.SuspendLayout();
            this.gbValeursR.SuspendLayout();
            this.gbValeursMax.SuspendLayout();
            this.gbInfos.SuspendLayout();
            this.gbReset.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbConnexion
            // 
            this.gbConnexion.Controls.Add(this.bPortCom);
            this.gbConnexion.Controls.Add(this.cbPorts);
            this.gbConnexion.Controls.Add(this.bConnexion);
            this.gbConnexion.Controls.Add(this.label1);
            this.gbConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbConnexion.Location = new System.Drawing.Point(16, 14);
            this.gbConnexion.Margin = new System.Windows.Forms.Padding(4);
            this.gbConnexion.Name = "gbConnexion";
            this.gbConnexion.Padding = new System.Windows.Forms.Padding(4);
            this.gbConnexion.Size = new System.Drawing.Size(268, 146);
            this.gbConnexion.TabIndex = 0;
            this.gbConnexion.TabStop = false;
            this.gbConnexion.Text = "Connexion";
            // 
            // bPortCom
            // 
            this.bPortCom.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPortCom.Location = new System.Drawing.Point(195, 30);
            this.bPortCom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bPortCom.Name = "bPortCom";
            this.bPortCom.Size = new System.Drawing.Size(51, 48);
            this.bPortCom.TabIndex = 4;
            this.bPortCom.Text = "🔄";
            this.PopupInfos.SetToolTip(this.bPortCom, "Actualiser les ports");
            this.bPortCom.UseVisualStyleBackColor = true;
            this.bPortCom.Click += new System.EventHandler(this.bPortCom_Click);
            // 
            // cbPorts
            // 
            this.cbPorts.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPorts.FormattingEnabled = true;
            this.cbPorts.Location = new System.Drawing.Point(73, 37);
            this.cbPorts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbPorts.Name = "cbPorts";
            this.cbPorts.Size = new System.Drawing.Size(115, 34);
            this.cbPorts.TabIndex = 3;
            this.cbPorts.SelectedIndexChanged += new System.EventHandler(this.cbPorts_SelectedIndexChanged);
            this.cbPorts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbPorts_KeyDown);
            // 
            // bConnexion
            // 
            this.bConnexion.AccessibleDescription = "";
            this.bConnexion.Enabled = false;
            this.bConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bConnexion.Location = new System.Drawing.Point(19, 86);
            this.bConnexion.Margin = new System.Windows.Forms.Padding(4);
            this.bConnexion.Name = "bConnexion";
            this.bConnexion.Size = new System.Drawing.Size(236, 43);
            this.bConnexion.TabIndex = 2;
            this.bConnexion.Tag = "";
            this.bConnexion.Text = "Tester la connexion";
            this.PopupInfos.SetToolTip(this.bConnexion, "Tester la connexion");
            this.bConnexion.UseVisualStyleBackColor = true;
            this.bConnexion.Click += new System.EventHandler(this.bConnexion_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port :";
            // 
            // gbChannel
            // 
            this.gbChannel.Controls.Add(this.rbCH3);
            this.gbChannel.Controls.Add(this.rbCH4);
            this.gbChannel.Controls.Add(this.rbCH2);
            this.gbChannel.Controls.Add(this.rbCH1);
            this.gbChannel.Controls.Add(this.bOnOff);
            this.gbChannel.Controls.Add(this.pEtatChannel);
            this.gbChannel.Enabled = false;
            this.gbChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbChannel.Location = new System.Drawing.Point(292, 14);
            this.gbChannel.Margin = new System.Windows.Forms.Padding(4);
            this.gbChannel.Name = "gbChannel";
            this.gbChannel.Padding = new System.Windows.Forms.Padding(4);
            this.gbChannel.Size = new System.Drawing.Size(185, 243);
            this.gbChannel.TabIndex = 1;
            this.gbChannel.TabStop = false;
            this.gbChannel.Text = "Channel";
            // 
            // rbCH3
            // 
            this.rbCH3.AutoSize = true;
            this.rbCH3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCH3.Location = new System.Drawing.Point(33, 61);
            this.rbCH3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbCH3.Name = "rbCH3";
            this.rbCH3.Size = new System.Drawing.Size(44, 29);
            this.rbCH3.TabIndex = 6;
            this.rbCH3.Text = "3";
            this.PopupInfos.SetToolTip(this.rbCH3, "Sélectionner le channel 3");
            this.rbCH3.UseVisualStyleBackColor = true;
            this.rbCH3.Click += new System.EventHandler(this.rbCH3_Click);
            // 
            // rbCH4
            // 
            this.rbCH4.AutoSize = true;
            this.rbCH4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCH4.Location = new System.Drawing.Point(109, 63);
            this.rbCH4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbCH4.Name = "rbCH4";
            this.rbCH4.Size = new System.Drawing.Size(44, 29);
            this.rbCH4.TabIndex = 5;
            this.rbCH4.Text = "4";
            this.PopupInfos.SetToolTip(this.rbCH4, "Sélectionner le channel 4");
            this.rbCH4.UseVisualStyleBackColor = true;
            this.rbCH4.Click += new System.EventHandler(this.rbCH4_Click);
            // 
            // rbCH2
            // 
            this.rbCH2.AutoSize = true;
            this.rbCH2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCH2.Location = new System.Drawing.Point(109, 30);
            this.rbCH2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbCH2.Name = "rbCH2";
            this.rbCH2.Size = new System.Drawing.Size(44, 29);
            this.rbCH2.TabIndex = 4;
            this.rbCH2.Text = "2";
            this.PopupInfos.SetToolTip(this.rbCH2, "Sélectionner le channel 2");
            this.rbCH2.UseVisualStyleBackColor = true;
            this.rbCH2.Click += new System.EventHandler(this.rbCH2_Click);
            // 
            // rbCH1
            // 
            this.rbCH1.AutoSize = true;
            this.rbCH1.Checked = true;
            this.rbCH1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCH1.Location = new System.Drawing.Point(33, 30);
            this.rbCH1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbCH1.Name = "rbCH1";
            this.rbCH1.Size = new System.Drawing.Size(44, 29);
            this.rbCH1.TabIndex = 3;
            this.rbCH1.TabStop = true;
            this.rbCH1.Text = "1";
            this.PopupInfos.SetToolTip(this.rbCH1, "Sélectionner le channel 1");
            this.rbCH1.UseVisualStyleBackColor = true;
            this.rbCH1.Click += new System.EventHandler(this.rbCH1_Click);
            // 
            // bOnOff
            // 
            this.bOnOff.AccessibleDescription = "ttttt";
            this.bOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOnOff.Location = new System.Drawing.Point(26, 187);
            this.bOnOff.Margin = new System.Windows.Forms.Padding(4);
            this.bOnOff.Name = "bOnOff";
            this.bOnOff.Size = new System.Drawing.Size(127, 44);
            this.bOnOff.TabIndex = 1;
            this.bOnOff.Text = "Allumer";
            this.PopupInfos.SetToolTip(this.bOnOff, "Allumer ou Eteindre le Channel");
            this.bOnOff.UseVisualStyleBackColor = true;
            this.bOnOff.Click += new System.EventHandler(this.bOnOff_Click);
            // 
            // pEtatChannel
            // 
            this.pEtatChannel.BackColor = System.Drawing.Color.Red;
            this.pEtatChannel.Location = new System.Drawing.Point(49, 100);
            this.pEtatChannel.Margin = new System.Windows.Forms.Padding(4);
            this.pEtatChannel.Name = "pEtatChannel";
            this.pEtatChannel.Size = new System.Drawing.Size(75, 75);
            this.pEtatChannel.TabIndex = 0;
            // 
            // bReset
            // 
            this.bReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReset.Location = new System.Drawing.Point(33, 31);
            this.bReset.Margin = new System.Windows.Forms.Padding(4);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(120, 44);
            this.bReset.TabIndex = 2;
            this.bReset.Text = "Reset";
            this.PopupInfos.SetToolTip(this.bReset, "Réinitialiser l\'Hameg");
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // gbValeursR
            // 
            this.gbValeursR.Controls.Add(this.lIntensiteR);
            this.gbValeursR.Controls.Add(this.lTensionR);
            this.gbValeursR.Enabled = false;
            this.gbValeursR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValeursR.Location = new System.Drawing.Point(493, 14);
            this.gbValeursR.Margin = new System.Windows.Forms.Padding(4);
            this.gbValeursR.Name = "gbValeursR";
            this.gbValeursR.Padding = new System.Windows.Forms.Padding(4);
            this.gbValeursR.Size = new System.Drawing.Size(219, 129);
            this.gbValeursR.TabIndex = 2;
            this.gbValeursR.TabStop = false;
            this.gbValeursR.Text = "Valeurs réelles";
            // 
            // lIntensiteR
            // 
            this.lIntensiteR.AutoSize = true;
            this.lIntensiteR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIntensiteR.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lIntensiteR.Location = new System.Drawing.Point(52, 78);
            this.lIntensiteR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lIntensiteR.Name = "lIntensiteR";
            this.lIntensiteR.Size = new System.Drawing.Size(99, 26);
            this.lIntensiteR.TabIndex = 1;
            this.lIntensiteR.Text = "0.0000 A";
            this.lIntensiteR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lTensionR
            // 
            this.lTensionR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lTensionR.AutoSize = true;
            this.lTensionR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTensionR.Location = new System.Drawing.Point(52, 45);
            this.lTensionR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTensionR.Name = "lTensionR";
            this.lTensionR.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lTensionR.Size = new System.Drawing.Size(87, 26);
            this.lTensionR.TabIndex = 0;
            this.lTensionR.Text = "0.000 V";
            this.lTensionR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbValeursMax
            // 
            this.gbValeursMax.Controls.Add(this.label3);
            this.gbValeursMax.Controls.Add(this.label2);
            this.gbValeursMax.Controls.Add(this.bModifierValeurs);
            this.gbValeursMax.Controls.Add(this.tbIntensiteMax);
            this.gbValeursMax.Controls.Add(this.tbTensionMax);
            this.gbValeursMax.Enabled = false;
            this.gbValeursMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValeursMax.Location = new System.Drawing.Point(485, 149);
            this.gbValeursMax.Margin = new System.Windows.Forms.Padding(4);
            this.gbValeursMax.Name = "gbValeursMax";
            this.gbValeursMax.Padding = new System.Windows.Forms.Padding(4);
            this.gbValeursMax.Size = new System.Drawing.Size(227, 199);
            this.gbValeursMax.TabIndex = 3;
            this.gbValeursMax.TabStop = false;
            this.gbValeursMax.Text = "Valeurs maximales";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(141, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(143, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "V";
            // 
            // bModifierValeurs
            // 
            this.bModifierValeurs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bModifierValeurs.Location = new System.Drawing.Point(8, 148);
            this.bModifierValeurs.Margin = new System.Windows.Forms.Padding(4);
            this.bModifierValeurs.Name = "bModifierValeurs";
            this.bModifierValeurs.Size = new System.Drawing.Size(207, 43);
            this.bModifierValeurs.TabIndex = 2;
            this.bModifierValeurs.Text = "Modifier valeurs";
            this.PopupInfos.SetToolTip(this.bModifierValeurs, "Modifier les valeurs max");
            this.bModifierValeurs.UseVisualStyleBackColor = true;
            this.bModifierValeurs.Click += new System.EventHandler(this.bModifierValeurs_Click);
            // 
            // tbIntensiteMax
            // 
            this.tbIntensiteMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntensiteMax.Location = new System.Drawing.Point(49, 89);
            this.tbIntensiteMax.Margin = new System.Windows.Forms.Padding(4);
            this.tbIntensiteMax.Name = "tbIntensiteMax";
            this.tbIntensiteMax.Size = new System.Drawing.Size(89, 32);
            this.tbIntensiteMax.TabIndex = 1;
            this.tbIntensiteMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbIntensiteMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIntensiteMax_KeyPress);
            // 
            // tbTensionMax
            // 
            this.tbTensionMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTensionMax.Location = new System.Drawing.Point(49, 47);
            this.tbTensionMax.Margin = new System.Windows.Forms.Padding(4);
            this.tbTensionMax.Name = "tbTensionMax";
            this.tbTensionMax.Size = new System.Drawing.Size(89, 32);
            this.tbTensionMax.TabIndex = 0;
            this.tbTensionMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTensionMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTensionMax_KeyPress);
            // 
            // gbInfos
            // 
            this.gbInfos.Controls.Add(this.rtbInfos);
            this.gbInfos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbInfos.Location = new System.Drawing.Point(16, 167);
            this.gbInfos.Margin = new System.Windows.Forms.Padding(4);
            this.gbInfos.Name = "gbInfos";
            this.gbInfos.Padding = new System.Windows.Forms.Padding(4);
            this.gbInfos.Size = new System.Drawing.Size(268, 181);
            this.gbInfos.TabIndex = 4;
            this.gbInfos.TabStop = false;
            this.gbInfos.Text = "Informations";
            // 
            // rtbInfos
            // 
            this.rtbInfos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbInfos.Location = new System.Drawing.Point(7, 32);
            this.rtbInfos.Margin = new System.Windows.Forms.Padding(4);
            this.rtbInfos.Name = "rtbInfos";
            this.rtbInfos.ReadOnly = true;
            this.rtbInfos.Size = new System.Drawing.Size(255, 143);
            this.rtbInfos.TabIndex = 0;
            this.rtbInfos.Text = "Sélectionnez un port.";
            // 
            // ThreadChannel
            // 
            this.ThreadChannel.Interval = 500;
            this.ThreadChannel.Tick += new System.EventHandler(this.ThreadChannel_Tick);
            // 
            // gbReset
            // 
            this.gbReset.Controls.Add(this.bReset);
            this.gbReset.Enabled = false;
            this.gbReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbReset.Location = new System.Drawing.Point(292, 264);
            this.gbReset.Name = "gbReset";
            this.gbReset.Size = new System.Drawing.Size(185, 84);
            this.gbReset.TabIndex = 5;
            this.gbReset.TabStop = false;
            this.gbReset.Text = "Réinitialiser";
            // 
            // IHMdeTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 359);
            this.Controls.Add(this.gbReset);
            this.Controls.Add(this.gbInfos);
            this.Controls.Add(this.gbValeursMax);
            this.Controls.Add(this.gbValeursR);
            this.Controls.Add(this.gbChannel);
            this.Controls.Add(this.gbConnexion);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "IHMdeTest";
            this.Text = "IHM de test";
            this.gbConnexion.ResumeLayout(false);
            this.gbConnexion.PerformLayout();
            this.gbChannel.ResumeLayout(false);
            this.gbChannel.PerformLayout();
            this.gbValeursR.ResumeLayout(false);
            this.gbValeursR.PerformLayout();
            this.gbValeursMax.ResumeLayout(false);
            this.gbValeursMax.PerformLayout();
            this.gbInfos.ResumeLayout(false);
            this.gbReset.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbConnexion;
        private System.Windows.Forms.Button bConnexion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbChannel;
        private System.Windows.Forms.Button bOnOff;
        private System.Windows.Forms.Panel pEtatChannel;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.GroupBox gbValeursR;
        private System.Windows.Forms.Label lIntensiteR;
        private System.Windows.Forms.Label lTensionR;
        private System.Windows.Forms.GroupBox gbValeursMax;
        private System.Windows.Forms.Button bModifierValeurs;
        private System.Windows.Forms.TextBox tbIntensiteMax;
        private System.Windows.Forms.TextBox tbTensionMax;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbInfos;
        private System.Windows.Forms.RichTextBox rtbInfos;
        private System.Windows.Forms.Timer ThreadChannel;
        private System.Windows.Forms.ComboBox cbPorts;
        private System.Windows.Forms.RadioButton rbCH3;
        private System.Windows.Forms.RadioButton rbCH4;
        private System.Windows.Forms.RadioButton rbCH2;
        private System.Windows.Forms.RadioButton rbCH1;
        private System.Windows.Forms.Button bPortCom;
        private System.Windows.Forms.ToolTip PopupInfos;
        private System.Windows.Forms.GroupBox gbReset;
    }
}

