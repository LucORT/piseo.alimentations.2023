﻿using Alimentation_HMP4040;
using System;
using System.Drawing;
using System.Globalization;
using System.IO.Ports;
using System.Windows.Forms;

namespace IHMdeTest
{
    public partial class IHMdeTest : Form
    {
        HMP4040 oAlimentation;
        int iNumChannel = 1;

        public IHMdeTest()
        {
            InitializeComponent();

            //Détection et ajout des ports dans la liste déroulante

            cbPorts.Items.AddRange(HMP4040.DetecterPorts());
        }

        //Tester la connexion vers l'hameg
        private void bConnexion_Click(object sender, EventArgs e)
        {
            //Désactiver le bouton pour éviter de lancer plusieurs requete
            bConnexion.Enabled = false;

            if (bConnexion.Text == "Tester la connexion")
            {
                if (cbPorts.Text != "")
                {
                    rtbInfos.Text = "Tentative de connexion...";

                    oAlimentation = new HMP4040(cbPorts.Text);

                    try
                    {
                        oAlimentation.TestConnexion();
                        Verification();

                        bPortCom.Enabled = false;
                        cbPorts.Enabled = false;

                        rtbInfos.Text = "Connexion réussie.";
                        bConnexion.Text = "Changer de port";
                    }
                    catch (ConnexionException ex)
                    {
                        rtbInfos.Text = ex.Message + "\n";
                    }
                    catch (PortOccupeException ex)
                    {
                        rtbInfos.Text = ex.Message + "\n";
                    }
                    catch (TimeoutException ex)
                    {
                        rtbInfos.Text = ex.Message + "\n";
                    }
                    catch (Exception ex)
                    {
                        rtbInfos.Text = ex.Message + "\n";
                    }
                } else
                {
                    rtbInfos.Text = "Veuillez sélectionner un port.";
                }

            } else {
                rtbInfos.Text = "Veuillez patientez...";
                Desactiver();
                rtbInfos.Text = "Sélectionnez un port.";
            }

            bConnexion.Enabled = true;
        }

        private void cbPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            bConnexion.Enabled = true;
        }

        private void bOnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (bOnOff.Text == "Allumer")
                {
                    rtbInfos.Text = "Activation du channel " + iNumChannel + "...";

                    oAlimentation.Channels[iNumChannel-1].Activer();
                    oAlimentation.ActiverSortie();

                    bOnOff.Text = "Eteindre";
                    pEtatChannel.BackColor = Color.Lime;

                    ThreadChannel.Enabled = true;

                    rtbInfos.Text = "Channel " + iNumChannel + " activée.";
                }
                else
                {
                    rtbInfos.Text = "Désactivation du channel " + iNumChannel + "...";

                    oAlimentation.Channels[iNumChannel - 1].Desactiver();

                    if (oAlimentation.Channels[0].EtatChannel() == false
                        && oAlimentation.Channels[1].EtatChannel() == false
                        && oAlimentation.Channels[2].EtatChannel() == false
                        && oAlimentation.Channels[3].EtatChannel() == false)
                    {
                        oAlimentation.DesactiverSortie();
                    }

                    rtbInfos.Text = "" + iNumChannel;

                    bOnOff.Text = "Allumer";
                    pEtatChannel.BackColor = Color.Red;

                    ThreadChannel.Enabled = false;

                    lTensionR.Text = "0.000 V";
                    lIntensiteR.Text = "0.0000 A";

                    rtbInfos.Text = "Channel " + iNumChannel + " désactivé.";
                }
            }
            catch (ConnexionException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (PortOccupeException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
        }

        private void bReset_Click(object sender, EventArgs e)
        {
            try
            {
                rtbInfos.Text = "Réinitialisation en cours...";

                oAlimentation.Reset();
                Verification();

                rtbInfos.Text = "Réinitialisation terminée.";
            }
            catch (ConnexionException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (PortOccupeException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
        }

        private void bModifierValeurs_Click(object sender, EventArgs e)
        {
            try
            {
                rtbInfos.Text = "Modifications des valeurs max...";

                double lnTensionMax = Convert.ToDouble(tbTensionMax.Text, CultureInfo.InvariantCulture);
                double lnIntensiteMax = Convert.ToDouble(tbIntensiteMax.Text, CultureInfo.InvariantCulture);

                oAlimentation.Channels[iNumChannel - 1].ModifierValeurs(lnTensionMax, lnIntensiteMax);

                rtbInfos.Text = "Valeurs max modifiées\n";


                double lnTensionLue = oAlimentation.Channels[iNumChannel - 1].LireTensionMax();
                double lnIntensiteLue = oAlimentation.Channels[iNumChannel - 1].LireIntensiteMax();


                if (lnTensionMax > 32.05)
                {
                    rtbInfos.Text += "La tension maximale possible est de 32.05 V.\n";
                } else if (lnTensionLue < lnTensionMax)
                {
                    rtbInfos.Text += "La tension maximale ne peut pas dépasser " + lnTensionLue +
                        " V lorsque la valeur de l'intensité vaut " + lnIntensiteLue + " A.\n";
                }


                if (lnIntensiteMax > 10)
                {
                    rtbInfos.Text += "L'intensité maximale possible est de 10 A.\n";
                } else if (lnIntensiteLue < lnIntensiteMax)
                {
                    rtbInfos.Text += "L'intensité maximale ne peut pas dépasser " + lnIntensiteLue +
                        " A lorsque la tension max vaut " + lnTensionLue + " V.";
                }

                tbTensionMax.Text = lnTensionLue.ToString(CultureInfo.InvariantCulture);
                tbIntensiteMax.Text = lnIntensiteLue.ToString(CultureInfo.InvariantCulture);
            }
            catch (ConnexionException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (PortOccupeException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
        }

        private void bPortCom_Click(object sender, EventArgs e)
        {
            rtbInfos.Text = "Actualisation des ports...";

            bConnexion.Enabled = false;
            cbPorts.Items.Clear();

            cbPorts.Items.AddRange(SerialPort.GetPortNames());

            cbPorts.Text = "";

            rtbInfos.Text = "Actualisation des ports terminées.";
        }

        private void cbPorts_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void ThreadChannel_Tick(object sender, EventArgs e)
        {
            try
            {
                lTensionR.Text = oAlimentation.Channels[iNumChannel - 1].LireTensionReelle().ToString(CultureInfo.InvariantCulture) + " V";
                lIntensiteR.Text = oAlimentation.Channels[iNumChannel - 1].LireIntensiteReelle().ToString(CultureInfo.InvariantCulture) + " A";
            }
            catch (ConnexionException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (PortOccupeException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
        }

        public void Verification()
        {
            try
            {
                gbChannel.Enabled = false;
                gbValeursR.Enabled = false;
                gbValeursMax.Enabled = false;
                gbReset.Enabled = false;

                if (oAlimentation.Channels[iNumChannel - 1].EtatChannel() == true && oAlimentation.EtatSortie() == true)
                {
                    bOnOff.Text = "Eteindre";
                    pEtatChannel.BackColor = Color.Lime;

                    if (ThreadChannel.Enabled == false)
                    {
                        ThreadChannel.Enabled = true;
                    }
                }
                else
                {
                    bOnOff.Text = "Allumer";
                    pEtatChannel.BackColor = Color.Red;

                    if (ThreadChannel.Enabled == true)
                    {
                        ThreadChannel.Enabled = false;
                    }

                    lTensionR.Text = "0.000 V";
                    lIntensiteR.Text = "0.0000 A";
                }

                tbTensionMax.Text = oAlimentation.Channels[iNumChannel - 1].LireTensionMax().ToString(CultureInfo.InvariantCulture);
                tbIntensiteMax.Text = oAlimentation.Channels[iNumChannel - 1].LireIntensiteMax().ToString(CultureInfo.InvariantCulture);

                gbChannel.Enabled = true;
                gbValeursR.Enabled = true;
                gbValeursMax.Enabled = true;
                gbReset.Enabled = true;
            }
            catch (ConnexionException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (PortOccupeException ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
            catch (Exception ex)
            {
                rtbInfos.Text = ex.Message + ".\nVeuillez patientez...";
                Desactiver();
                rtbInfos.Text = ex.Message + ".\nSélectionnez un port.";
            }
        }
                
        private void rbCH1_Click(object sender, EventArgs e)
        {
            iNumChannel = 1;
            Verification();
        }

        private void rbCH2_Click(object sender, EventArgs e)
        {
            iNumChannel = 2;
            Verification();
        }
        private void rbCH3_Click(object sender, EventArgs e)
        {
            iNumChannel = 3;
            Verification();
        }
        private void rbCH4_Click(object sender, EventArgs e)
        {
            iNumChannel = 4;
            Verification();
        }

        public void Desactiver()
        {
            bConnexion.Text = "Tester la connexion";

            gbChannel.Enabled = false;
            gbValeursR.Enabled = false;
            gbValeursMax.Enabled = false;
            gbReset.Enabled = false;

            bPortCom.Enabled = true;
            cbPorts.Enabled = true;

            ThreadChannel.Enabled = false;
            pEtatChannel.BackColor = Color.Red;
            lTensionR.Text = "0.000 V";
            lIntensiteR.Text = "0.0000 A";
        }

        private void tbTensionMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void tbIntensiteMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
