using System.Data;
using System.Security.Cryptography.X509Certificates;
using System;
using System.IO.Ports;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Text;
using IHMKeithley.v2;
using testDLLAlim;

namespace IHMKeithley.v2
{
	public partial class IHMKeithley : Form
	{
		public IHMKeithley()
		{
			InitializeComponent();
			label6.Visible = false;

		}
		public bool limV, limI, levI, pulse = false;

		private void IHMKeithley_Load(object sender, EventArgs e)
		{
			/*string[] ports = SerialPort.GetPortNames();
			textBox1.Items.AddRange(ports);
			if (textBox1.Items.Count > 0)
			{
				textBox1.SelectedIndex = 0;
			}*/

		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{

		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			/*float num;
			bool NumberOK = float.TryParse(textBox1.Text, out num);
			if (!NumberOK)
			{
				button1.Enabled = false;
			}
			else
			{
				button1.Enabled = true;
			}*/
		}

		private void label2_Click(object sender, EventArgs e)
		{

		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{
			bool limV = true;
			float num;
			bool NumberOK = float.TryParse(textBox2.Text, out num);
			if (!NumberOK)
			{
				textBox3.Text = null;
			}
			else
			{
				if (num < 40 || num > 0)
					limV = true;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			textBox2.Text = "";
			textBox3.Text = "";
			textBox4.Text = "";
			textBox5.Text = "";
		}

		private void textBox3_TextChanged(object sender, EventArgs e)
		{
			float num;
			bool NumberOK = float.TryParse(textBox3.Text, out num);
			if (!NumberOK)
			{
				textBox3.Text = null;
			}
			else
			{
				if (num < 0)
					limI = true;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			label6.Visible = true;
			Listener.StartListening(this);
			//Le Serveur commence a ecouter si on appuie sur ce bouton
		}

		public void OnScriptRe�u(string script)
		{
			//Utilisation des Gets de testAlim
			DLLAlim maDLL = new DLLAlim();
			maDLL.setTexteScript(script); 
			string LimitI = maDLL.GetLimitI();
			string LimitV = maDLL.GetLimitV();
			string LevelI = maDLL.GetLevelI();
			string Delay = maDLL.GetDelay();
			//Affichage dans le textBox
			textBox3.Text = LimitI.ToString().Replace(".", ",");
			textBox5.Text = Delay.ToString();
			textBox2.Text = LimitV.ToString().Replace(".", ",");
			textBox4.Text = LevelI.ToString().Replace(".", ",");

			System.Windows.Forms.MessageBox.Show("J'ai re�u quelque chose :"+ LimitI + Delay + LevelI + LimitV);
		}

		private void label6_VisibleChanged(object sender, EventArgs e)
		{
			label6.Visible = true;
		}

		private void button2_Click(object sender, EventArgs e)
		{

		}

		private void label6_Click(object sender, EventArgs e)
		{

		}

		private void textBox5_TextChanged(object sender, EventArgs e)
		{
			float num;
			bool NumberOK = float.TryParse(textBox5.Text, out num);
			if (!NumberOK)
			{
				textBox5.Text = null;
			}
			else
			{
				if (num < 0)
					pulse = true;
			}
		}

		private void textBox4_TextChanged(object sender, EventArgs e)
		{
			float num;
			bool NumberOK = float.TryParse(textBox4.Text, out num);
			if (!NumberOK)
			{
				textBox4.Text = null;
			}
			else
			{
				if (num < 0)
					levI = true;
			}
		}
		public void CheckedButton()
		{
		}
	}
}