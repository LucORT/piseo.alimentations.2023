using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace testDLLAlim
{
    public class DLLAlim
    {
        private TcpClient client;

        public TcpClient Client
        {
            get { return client; }
            set { client = value; }
        }

        private NetworkStream stream;

        public NetworkStream Stream
        {
            get { return stream; }
            set { stream = value; }
        }

        // A modifier
        private string[] textLines;

        public string[] TextLines
        {
            get { return textLines; }
            set { textLines = value; }
        }

        private int nbLines;

        public int NbLines
        {
            get { return nbLines; }
            set { nbLines = value; }
        }

        private string returndata;

        public string Returndata
        {
            get { return returndata; }
            set { returndata = value; }
        }
        // !!!!!!!!!!!!!!!!!!!! fin des propiete

        // $$$$$$$$$$$$ debut des methodes


        public bool Disconnect(string disc)
        {
            try
            {
                // TODO : ??????
                this.fileRead(disc);
                this.Send();
                this.Recevoir();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de lecture de fichier :" + ex);
                return false;
            }
        }

        // methode permettant de se connecter en utilisant une adresse et un port
        public bool Connect(string address, int port)
        {
            try
            {
                // utilisation de tcpclient
                this.client = new TcpClient();
                this.client.Connect(address, port);
                // Console.WriteLine(this.client.Connected);
                this.stream = this.client.GetStream();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de connexion :" + ex);
                return false;
            }
        }

        // methode pour lire un fichier placer dans un chemin
        public bool fileRead(string path)
        {
            try
            {
                // textLines est un tableau de string, ici on va lire toutes ses lignes
                this.textLines = System.IO.File.ReadAllLines(path);
                // sert a compter les lignes de textLines
                this.nbLines = System.IO.File.ReadAllLines(path).Count();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de lecture de fichier :" + ex);
                return false;
            }
        }

        public void setTexteScript(string leTexteScript)
        {
			this.textLines = leTexteScript.Split(
	            new string[] { "\n" },
	            StringSplitOptions.None
            );

		}

        // methode pour lire et modifier un fichiers piseo 
        public bool fileReadModifierPiseo(string path, string limiti, string limitv, string leveli, string delay)
        {
            try
            {
                this.textLines = System.IO.File.ReadAllLines(path);
                this.nbLines = System.IO.File.ReadAllLines(path).Count();

                // initialisationde i à 0
                int i = 0;

                // pour chaque ligne dans textLines
                foreach (string line in textLines)
                {
                    // si la ligne contient "smu1.source.limiti"   on va mettre "smu1.source.limiti" + sa limiti dans textLines selon i
                    if (line.Contains("smu1.source.limiti = "))
                    {
                        this.textLines[i] = "smu1.source.limiti = " + limiti;
                        // Console.WriteLine("smua.source.limiti = " + limiti);
                    }

                    // sinon si la ligne contient "smu1.source.limitv" on va mettre "smu1.source.limitv" + sa limitv dans textLines
                    else if (line.Contains("smu1.source.limitv = "))
                    {
                        this.textLines[i] = "smu1.source.limitv = " + limitv;
                        // Console.WriteLine("smua.source.limitv = " + limitv);
                    }

                    // sinon si la ligne contient "smu1.source.leveli" on va mettre "smu1.source.leveli" + sa leveli dans textLines
                    else if (line.Contains("smu1.source.leveli = "))
                    {
                        this.textLines[i] = "smu1.source.leveli = " + leveli;
                        // Console.WriteLine("smua.source.leveli = " + leveli);
                    }

                    // sinon si la ligne contient "delay(???)" on va mettre "delay" + son delay dans textLines
                    else if (line.Contains("delay(25E-3)"))
                    {
                        this.textLines[i] = "delay(" + delay + "E-3 " + "" + (" Application de " + delay + " ms de delai)");
                        // Console.WriteLine("delay(" + delay + ")");
                    }

                    // sinon mettre la ligne dans textLignes
                    else
                    {
                        this.textLines[i] = line;
                    }
                    Console.WriteLine(this.textLines[i]);
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de lecture du fichier ou de modification :" + ex);
                return false;
            }
        }

        // methode permettant de lire et modifier un fichier
        public bool fileReadModifier(string path, string range, string intensite)
        {
            try
            {
                this.textLines = System.IO.File.ReadAllLines(path);
                this.nbLines = System.IO.File.ReadAllLines(path).Count();

                // initialisationde i à 0
                int i = 0;

                // pour chaque ligne dans textLines
                foreach (string line in textLines)
                {
                    // si la ligne contient "smua.source.rangev"   on va mettre "smua.source.rangev" + sa porter dans textLines selon i
                    if (line.Contains("smua.source.rangev"))
                    {
                        this.textLines[i] = "smua.source.rangev = " + range;
                    }

                    // sinon si la ligne contient "smua.source.levelv" on va mettre "smua.source.levelv" + l'intensite dans textLines
                    else if (line.Contains("smua.source.levelv"))
                    {
                        this.textLines[i] = "smua.source.levelv = " + intensite;
                    }

                    // sinon mettre la ligne dans textLignes
                    else
                    {
                        this.textLines[i] = line;
                    }
                    Console.WriteLine(this.textLines[i]);
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de lecture du fichier ou de modification :" + ex);
                return false;
            }
        }


        //permet de lire un fichier situé dans le channel A
        /*   public bool ChannelA(string chanA)
           {
               try
               {
                   this.textLines = System.IO.File.ReadAllLines(chanA);
                   this.nbLines = System.IO.File.ReadAllLines(chanA).Count();
                   return true;
               }
               catch (Exception ex)
               {
                   Console.WriteLine("Erreur de lecture de fichier :" + ex);
                   return false;
               }
           }
   
   
   
           // permet de lire un fichier situé dans le channel B
           public bool ChannelB(string chanB)
           {
               try
               {
                   this.textLines = System.IO.File.ReadAllLines(chanB);
                   this.nbLines = System.IO.File.ReadAllLines(chanB).Count();
                   return true;
               }
               catch (Exception ex)
               {
                   Console.WriteLine("Erreur de lecture de fichier :" + ex);
                   return false;
               }
           }*/


        // permet d'envoyer un flux
        public bool Send()
        {
            try
            {
                // si flux permet d'écrire 
                if (this.stream.CanWrite)
                {

                    // pour toutes les lignes de textLines
                    for (int i = 0; i < this.nbLines; i++)
                    {

                        // tableau de byte nommé data dans lequel il y a textLines et son nb de lignes
                        byte[] data = System.Text.Encoding.ASCII.GetBytes(this.textLines[i] + "\n");

                        // compter elements de data
                        int count = data.Count();

                        // ecrit le flux avec data en partant de 0 et son nb d'elements
                        this.stream.Write(data, 0, count);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de reception de l'alimentation :" + ex);
                return false;
            }
            return true;
        }

        public int Recevoir()
        {
            int count = 0; // Nombre de d'octets réellement lus
            try
            {
                // permet de mettre un délais de 100ms pour attendre la reponse du serveur
                System.Threading.Thread.Sleep(1000);
                // si réponse disponnible
                if (this.stream.DataAvailable)
                {
                    // nouveau tableau en byte nommé buffer qui va recevoir la taille du buffer envoyé par le client
                    byte[] buffer = new byte[this.client.ReceiveBufferSize];
                    // stream.Read va remplir  le buffer, count va lire le buffer en commençant de 0 et dire y en a combien
                    count = stream.Read(buffer, 0, (int)client.ReceiveBufferSize);
                    // va retourner les données
                    this.returndata = System.Text.Encoding.UTF8.GetString(buffer, 0, count);
                }

                return count;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de reception :" + ex);
                return -1;
            }
        }


        public bool Exporter(string strRep)
        {
            string nom = "AlimData_";
            string date = DateTime.Now.ToString().Replace("/", "-").Replace(":", "'");
            string nomFichier = strRep + "\\" + date + nom + ".csv";
            string strSeperator = ",";


            try
            {
                string txt = Returndata;
                StringBuilder sbOutput = new StringBuilder();
                string[][] inaOutput = new string[][]
                {
                    new string[] { txt }
                };

                int ilength = inaOutput.GetLength(0);
                for (int i = 0; i < inaOutput.Length; i++)
                    sbOutput.AppendLine(string.Join(strSeperator, inaOutput[i]));
                // Create and write the csv file - version Margaux avec des , 
                //File.WriteAllText(nomFichier, sbOutput.ToString());

                // version Agnès avec ; 
                string Res = Returndata;
                Res = Res.Replace(',', ';');
                File.WriteAllText(nomFichier, Res);


                // exporter le script envoyé 
                nom = "AlimScript_";
                nomFichier = strRep + "\\" + date + nom + ".txt";

                string leScript = "";
                for (int i = 0; i < this.nbLines; i++)
                    leScript += textLines[i] + Environment.NewLine;
                File.WriteAllText(nomFichier, leScript);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur de lecture de fichier :" + ex);
                return false;
            }
        }

        public string GetLimitI()
        {
            try
            {
                string limiti = "Null";
                Regex rx = new Regex(@"=\ *(\d+(\.\d+)?)\ +-");
                foreach (string line in textLines)
                {
                    if (line.Contains("---------------------------------------------------------- SRo  - set SourceA un Courant limit"))
                    {
                        string chaine = line;
                        MatchCollection matches = rx.Matches(chaine);
                        foreach (Match match in matches)
                        {
                            GroupCollection groups = match.Groups;
                            limiti = groups[1].ToString();
                            Console.WriteLine(limiti);
                        }
                    }
                }
                return limiti;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible de Get LimitI :" + ex);
                return "Erreur";
            }
            // return 2.5;
        }

        public string GetLimitV()
        {
            try
            {
                string limitv = "Null";
                Regex rx = new Regex(@"=\ *(\d+(\.\d+)?)\ +-");
                foreach (string line in textLines)
                {
                    if (line.Contains("---------------------------------------------------------- SRo  - set SourceA une Tension limit"))
                    {
                        string chaine = line;
                        MatchCollection matches = rx.Matches(chaine);
                        foreach (Match match in matches)
                        {
                            GroupCollection groups = match.Groups;
                            limitv = groups[1].ToString();
                            Console.WriteLine(limitv);
                        }
                    }
                }
                return limitv;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible de Get LimitV :" + ex);
                return "Erreur";
            }
            // return 40;
        }

        public string GetLevelI()
        {
            try
            {
                string leveli = "Null";
                Regex rx = new Regex(@"=\ *(\d+(\.\d+)?)\ +-");
                foreach (string line in textLines)
                {
                    if (line.Contains("--------------------------------------------------------- SRo  - Source Courant A applique"))
                    {
                        string chaine = line;
                        MatchCollection matches = rx.Matches(chaine);
                        foreach (Match match in matches)
                        {
                            GroupCollection groups = match.Groups;
                            leveli = groups[1].ToString();
                            Console.WriteLine(leveli);
                        }
                    }
                }
                return leveli;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible de Get LevelI :" + ex);
                return "Erreur";
            }
            // return 0.35;
        }

        public string GetDelay()
        {
            try
            {
                string delay = "Null";
                Regex rx = new Regex(@"\((\d+[E]\D\d+)\)");
                foreach (string line in textLines)
                {
                    if (line.Contains("------------------    ---------------------------------------- SRo  - Applique un delai de"))
                    {
                        string chaine = line;
                        MatchCollection matches = rx.Matches(chaine);
                        foreach (Match match in matches)
                        {
                            GroupCollection groups = match.Groups;
                            delay = groups[1].ToString();
                            Console.WriteLine(delay);
                        }
                    }
                }
                return delay;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible de Get Delay :" + ex);
                return "Erreur";
            }
            //return "18E-3";
        }

        public bool SetLimitI(string limiti)
        {
            try
            {
                int i = 0;
                foreach (string line in textLines)
                {
                    if (line.Contains("---------------------------------------------------------- SRo  - set SourceA un Courant limitée à"))
                    {
                        this.textLines[i] = "    smu1.source.limiti = " + limiti +
                                            "   ---------------------------------------------------------- SRo  - set SourceA un Courant limitée à\n";
                        Console.WriteLine(this.textLines[i]);
                    }
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible de Set LimitI :" + ex);
                return false;
            }
        }

        public bool SetLimitV(string limitv)
        {
            try
            {
                int i = 0;
                foreach (string line in textLines)
                {
                    if (line.Contains("---------------------------------------------------------- SRo  - set SourceA une Tension limitée à"))
                    {
                        this.textLines[i] = "    smu1.source.limitv = " + limitv +
                                            "   ---------------------------------------------------------- SRo  - set SourceA une Tension limitée à\n";
                        Console.WriteLine(this.textLines[i]);
                    }
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible de Set LimitV :" + ex);
                return false;

            }
            }
            public bool SetLevelI(string leveli)
            {
                try
                {
                    int i = 0;
                    foreach (string line in textLines)
                    {
                        if (line.Contains("--------------------------------------------------------- SRo  - Source Courant A applique à"))
                        {
                            this.textLines[i] = "    smu1.source.leveli = " + leveli +
                                                "   --------------------------------------------------------- SRo  - Source Courant A applique à\n";
                            Console.WriteLine(this.textLines[i]);
                        }
                        i++;
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Impossible de Set LevelI :" + ex);
                    return false;
                }
            }

            public bool SetDelay(string delay)
            {
                try
                {
                    int i = 0;
                    foreach (string line in textLines)
                    {
                        if (line.Contains("------------------    ---------------------------------------- SRo  - Applique un delai de"))
                        {
                            this.textLines[i] = "    delay(" + delay +
                                                ")   ------------------    ---------------------------------------- SRo  - Applique un delai de\n";
                            Console.WriteLine(this.textLines[i]);
                        }
                        i++;
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Impossible de Set Delay :" + ex);
                    return false;
                }
            }


            /*public void DLLAlim()
            {
                //this.Disconnect(@"C:\Users\MASTER\Documents\deconnexion.txt");
                //Console.WriteLine("Déconnexion...");
                //Thread.Sleep(1500);
            }*/
        }
    }
