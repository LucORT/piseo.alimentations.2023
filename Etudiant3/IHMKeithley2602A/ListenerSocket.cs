﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using IHMKeithley.v2;
using testDLLAlim;



namespace IHMKeithley.v2
{

	public partial class Listener
	{

		// Incoming data from the client.
		public static string data = null;

		public static void StartListening(IHMKeithley Ecran)
		{
			// Data buffer for incoming data.
			byte[] bytes = new Byte[1024];

			// Establish the local endpoint for the socket.
			// Dns.GetHostName returns the name of the
			// host running the application.
			IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
			IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

			IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 5025);

			// Create a TCP/IP socket
			Socket listener = new Socket(ipAddress.AddressFamily,
				SocketType.Stream, ProtocolType.Tcp);

			// Bind the socket to the local endpoint and
			// listen for incoming connections.
			try
			{
				listener.Bind(localEndPoint);
				listener.Listen(10);

				// Start listening for connections.
				Socket handler;
				while (true)
				{
					//Console.WriteLine("Waiting for a connection...");
					// Program is suspended while waiting for an incoming connection.
					handler = listener.Accept();
					//Console.WriteLine("Connexion entrante acceptee");
					data = null;

					// An incoming connection needs to be processed.

					while (true)
					{
						int bytesRec = handler.Receive(bytes);
						data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
						//data += Encoding.GetEncoding(1252).GetString(bytes, 0, bytesRec);
						// Show the data on the console.
						//Console.WriteLine("Text received : {0}", data);
						if (data.Contains("endscript"))
							break;
					}

					//Echo the data back to the client.
					//byte[] msg = Encoding.ASCII.GetBytes(data);
					byte[] msg = Encoding.ASCII.GetBytes("Coucou");
					handler.Send(msg);
					handler.Shutdown(SocketShutdown.Both);
					handler.Close();
					Ecran.OnScriptReçu(data);

				}
				/*string[] lines = data.ReadLines;
					foreach (string r in lines)
				{
					Console.WriteLine("-- {0}", r);
				}*/

			}
			catch (Exception e)
			{
				//Console.WriteLine(e.ToString());
			}
		}
		//Console.WriteLine("\nPress ENTER to continue...");
		//Console.Read();

	}
	
}
