import re
import socket


class CKeithley2602A:
    CheminFichier = ""
    CheminExample = ""
    IpAddress = ""
    IpPort = 5025
    contents = []
    s = None
    # tu peux enlever ce code en commentaire je l'ai en double
    """def GetDelayAncient(self):
        # contents = np.loadtxt(self.CheminFichier, dtype=str)
        text_file = open(file=self.CheminFichier, mode="r", encoding="windows-1252")
        contents = text_file.readlines()
        text_file.close()
        delay = None
        # Parcourir les lignes de texte pour trouver la ligne qui contient l'information sur le delay
        for line in contents:
            # print(line)
            if ("------------------    ---------------------------------------- SRo  - Applique un delai de") in line:
                # C'est la ligne que l'on cherche
                # On utilise le regex pour trouver la valeur du delay
                pattern = "\((\d+[E]\D\d+)\)"
                nb = len(re.findall(pattern, line))
                print(nb)
                if nb == 1:
                    # Il y'a un seul résultat exactement
                    # On extrait la valeur du delay
                    result = re.search(pattern, line)
                    print(result.group(0))
                    # Notre code du delay est le premier resultat du regex : par exemple "delay(5E-5)"
                    codeDelay = result.group(0)
                    # extraire le delay de ce code
                    delay = codeDelay.replace("(", "")
                    delay = delay.replace(")", "")
                    # https://www.w3schools.com/python/ref_string_replace.asp

                else:
                    # Il n'y a aucun resultat ou plusieurs on ne peut pas connaitre le delay
                    print("Pas de résultat")
                    delay = None

        # Valeur de retour
        return delay"""
    #Patern du delay
    def GetDelay(self):
        information = self.GetInformation("delay\((\d+.\D\d+)\)",
                                          "------------------    ---------------------------------------- SRo  - Applique un delai de")
        # extraire l'info de ce code
        delay = information.replace("delay(", "")
        delay = delay.replace(")", "\n")
        return delay
    #Patern pour le LevelI
    def GetLevelI(self):
        information = self.GetInformation("=\ *(\d+(\.\d+)?)\ +-",
                                          "--------------------------------------------------------- SRo  - Source Courant A applique")
        # extraire l'info de ce code
        LevelI = information.replace("=", "")
        LevelI = LevelI.replace("-", "\n")
        return LevelI
    #Patern pour le LimitV
    def GetLimitV(self):
        information = self.GetInformation("=\ *(\d+(\.\d+)?)\ +-",
                                          "---------------------------------------------------------- SRo  - set SourceA une Tension limit")
        # extraire l'info de ce code
        if self.GetInformation("=\ *(\d+(\.\d+)?)\ +-",
                                          "---------------------------------------------------------- SRo  - set SourceA une Tension limit") != None:
            LimitV = information.replace("=", "")
            LimitV = LimitV.replace("-", "\n")
            return LimitV

    def GetLimitI(self):
        information = self.GetInformation("=\ *(\d+(\.\d+)?)\ +-",
                                          "---------------------------------------------------------- SRo  - set SourceA un Courant limit")
        LimitI = information
        # extraire l'info de ce code
        if self.GetInformation("=\ *(\d+(\.\d+)?)\ +-",
                                          "---------------------------------------------------------- SRo  - set SourceA un Courant limit") != None:
            LimitI = information.replace("=", "")
            LimitI = LimitI.replace("-", "\n")
        else:
            LimitI = "Information non trouvé "

        return LimitI

    def SetLimitV(self, limit):
        information = self.SetInfo("SetLimitV","   smu1.source.limitV = ", str(limit) + " ",
                                   " ---------------------------------------------------------- SRo  - set SourceA une Tension")
        SetLimitV = information

        return SetLimitV

    def SetLimitI(self, limit):
        information = self.SetInfo("Set LimitI","    smu1.source.limiti = ", str(limit) + " ",
                                    "   ---------------------------------------------------------- SRo  - set SourceA un Courant")
        SetLimitI = information
        return SetLimitI

    def SetLevelI(self, limit):
        information = self.SetInfo("Set LevelI","   smu1.source.levelI = ", str(limit) + " ",
                                    "--------------------------------------------------------- SRo  - Source Courant A")
        SetLevelI = information
        return SetLevelI

    def Setdelay(self, limit):
        information = self.SetInfo("Set delay","    delay(", str(limit) + ") ",
                                    "           ------------------    ---------------------------------------- SRo  - Applique un delai de 25ms pour le Pulse")
        Setdelay = information
        return Setdelay

    def SetInfo(self, set, smu1 , number, comme):
        info = None
        a = 0
        i = 0
        for line in self.contents:
            i = i + 1
            if comme in line:
                a = a + 1
                if a == 1:
                    list = line.split(comme)
                    line = smu1 + number + comme + list[1] + "\r"
                    self.contents[i-1] = line
                    info = line
                    "print(content[i])"
            elif a == 0 :
                info = ("Impossible de",set)
        return info

    # Lecture du fichier .tsp
    def fileRead(self):
        text_file = open(file=self.CheminFichier, mode="r", encoding="windows-1252")
        self.contents = text_file.readlines()
        text_file.close()

    # Ecriture dans le fichier example
    def FileWrite(self):
        ligne = 0
        with open(self.CheminExample, 'w', encoding="windows-1252") as f:
            for line in self.contents:
                f.write(self.contents[ligne])
                ligne += 1

    # Envoie de donnée
    def Send(self):
        lsend_encoded = None
        print(self.IpAddress)
        print(self.IpPort)
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.IpAddress, self.IpPort))
        """lsend_encoded = bytes(self.GetLimitI(),'cp1252') + bytes(self.GetDelay(),'cp1252') + bytes(self.GetLimitV(),'cp1252') + bytes(self.GetLevelI(),'cp1252')"""
        """s.send(lsend_encoded)"""
        for line in self.contents :
            lsend = line
            lsend_encoded = bytes(lsend, 'cp1252')
            self.s.send(lsend_encoded)


        self.s.send(b"Fin du scrip")
        return "lsend_encoded"
    """"Faire le Receive dans le meme socket """
    def Recevoir(self):
        reponse = ""
        while True :
            #toto = False
            LesBytes = self.s.recv(200)
            #if toto == True:
            if len(LesBytes) == 0 :
                break
            LeTexte = LesBytes.decode("utf-8")
            reponse = reponse + LeTexte
            if reponse.endswith('Off\n'):
               break
        return reponse

    def GetInformation(self, pattern, commentaire):

        info = None
        # Parcourir les lignes de texte pour trouver la ligne qui contient l'information
        for line in self.contents:
            # print(line)
            if commentaire in line:
                # C'est la ligne que l'on cherche
                # On utilise le regex pour trouver la valeur du delay
                # pattern = "\((\d+[E]\D\d+)\)"
                nb = len(re.findall(pattern, line))
                if nb == 1:
                    # Il y'a un seul résultat exactement
                    # On extrait la valeur du delay
                    result = re.search(pattern, line)
                    # Notre code du delay est le premier resultat du regex : par exemple "delay(5E-5)"
                    info = result.group(0)
                    # https://www.w3schools.com/python/ref_string_replace.asp

                else:
                    # Il n'y a aucun resultat ou plusieurs on ne peut pas connaitre le parametre
                    print("Pas de résultat")
                    info = None

        # Valeur de retour
        return info

"""
# Test de la classe


o = Pilotage_Keithley2602A()
#Fichier c'est le fichier du script de la keithley
o.CheminFichier = "C:\\Users\\User\\Documents\\Projet2emeAnnée\\Python\\Pulse 25ms_350mA_Trig_spectro APO.tsp"
#Example c'est pour les set mais j'ai pas finis
o.CheminExample = "C:\\Users\\User\\Documents\\Projet2emeAnnée\\Python\\example.txt"

o.fileRead()
o.Send()
o.FileWrite()



Test des differentes Fonctions
print(o.GetDelay())
print(o.GetLevelI())
print(o.GetLimitV())
print(o.GetLimitI())
print(o.SetLimitI(1))
print(o.SetLimitV(2))
print(o.SetLevelI(1))
print(o.Setdelay(2))"""

