﻿// Ci-dessous - une des versions du simulateur

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SynchronousSocketListener
{

	// Incoming data from the client.
	public static string data = null;

	public static void StartListening()
	{
		// Data buffer for incoming data.
		byte[] bytes = new Byte[1024];

		// Establish the local endpoint for the socket.
		// Dns.GetHostName returns the name of the
		// host running the application.
		IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
		IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

		IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 5025);

		// Create a TCP/IP socket
		Socket listener = new Socket(ipAddress.AddressFamily,
			SocketType.Stream, ProtocolType.Tcp);

		// Bind the socket to the local endpoint and
		// listen for incoming connections.
		try
		{
			listener.Bind(localEndPoint);
			listener.Listen(10);

			// Start listening for connections.
			Socket handler;
			while (true)
			{
				Console.WriteLine("Waiting for a connection...");
				// Program is suspended while waiting for an incoming connection.
				handler = listener.Accept();
				Console.WriteLine("Connexion entrante acceptee");
				data = null;

				// An incoming connection needs to be processed.

				while (true)
				{
					int bytesRec = handler.Receive(bytes);
					data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
					// Show the data on the console.
					Console.WriteLine("Text received : {0}", data);
					if (data.Contains(""))
						break;
				}

				// Echo the data back to the client.
				//byte[] msg = Encoding.ASCII.GetBytes(data);
				//handler.Send(msg);
				handler.Shutdown(SocketShutdown.Both);
				handler.Close();
			}

		}
		catch (Exception e)
		{
			Console.WriteLine(e.ToString());
		}

		Console.WriteLine("\nPress ENTER to continue...");
		Console.Read();

	}

	public static int Main(String[] args)
	{
		StartListening();
		return 0;
	}
}